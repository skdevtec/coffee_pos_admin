<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Journal extends Model
{
    protected $table = "journal";
    protected $primaryKey = "journal_id";
    public $timestamps = false;

    public function SelectJournal($store_id = 0, $branch_id = 0) {
    	if($store_id != 0) {
	    	return $journal = DB::table('journal')
	    		->leftjoin('transaction_detail', 'transaction_detail.transaction_detail_id', '=', 'journal.transaction_detail_id')
	    		->leftjoin('transaction', 'transaction.transaction_id', '=', 'transaction_detail.transaction_id')
	    		->leftjoin('other_transaction', 'other_transaction.other_transaction_id', '=', 'journal.other_transaction_id')
	    		->leftjoin('restock_detail', 'restock_detail.restock_detail_id', '=', 'journal.restock_detail_id')
	    		->leftjoin('restock', 'restock.restock_id', '=', 'restock_detail.restock_id')
	    		->leftjoin('transfer_detail', 'transfer_detail.transfer_detail_id', '=', 'journal.transfer_detail_id')
	    		->leftjoin('transfer', 'transfer.transfer_id', '=', 'transfer_detail.transfer_id')
	    		->join('branch', 'branch.branch_id', '=', 'journal.branch_id')
	    		->where('journal.store_id', '=', $store_id)
	    		// ->orderBy('journal.journal_date', 'DESC')
	    		->get();
    	} else {
	    	return $journal = DB::table('journal')
	    		->leftjoin('transaction_detail', 'transaction_detail.transaction_detail_id', '=', 'journal.transaction_detail_id')
	    		->leftjoin('transaction', 'transaction.transaction_id', '=', 'transaction_detail.transaction_id')
	    		->leftjoin('other_transaction', 'other_transaction.other_transaction_id', '=', 'journal.other_transaction_id')
	    		->leftjoin('restock_detail', 'restock_detail.restock_detail_id', '=', 'journal.restock_detail_id')
	    		->leftjoin('restock', 'restock.restock_id', '=', 'restock_detail.restock_id')
	    		->leftjoin('transfer_detail', 'transfer_detail.transfer_detail_id', '=', 'journal.transfer_detail_id')
	    		->leftjoin('transfer', 'transfer.transfer_id', '=', 'transfer_detail.transfer_id')
	    		->where('journal.branch_id', '=', $branch_id)
	    		// ->orderBy('journal.journal_date', 'DESC')
	    		->get();
    	}
	}

    public function ShowJournal($id, $column) {
    	return $journal = DB::table('journal')
    		->where($column, '=', $id)
    		->get();
    }

    public function InsertJournalRestock($restock_id, $restock_date) {
    	$restocks = DB::table('restock')
    		->join('restock_detail', 'restock_detail.restock_id', '=', 'restock.restock_id')
    		->where('restock.restock_id', '=', $restock_id)
    		->get();
    	$branches = Branch::find($restocks[0]->branch_id);

    	foreach($restocks as $row) {
	    	$journal = new Journal();
			$journal->journal_date = $restock_date;
			$journal->journal_debet = $row->restock_detail_price;
			$journal->journal_credit = 0;
			$journal->account_id = $row->account_id_debet;
			$journal->restock_detail_id = $row->restock_detail_id;
			$journal->branch_id = $row->branch_id;
			$journal->store_id = $branches->store_id;
			$journal->save();

			$journal = new Journal();
			$journal->journal_date = $restock_date;
			$journal->journal_debet = 0;
			$journal->journal_credit = $row->restock_detail_price;
			$journal->account_id = $row->account_id_credit;
			$journal->restock_detail_id = $row->restock_detail_id;
			$journal->branch_id = $row->branch_id;
			$journal->store_id = $branches->store_id;
			$journal->save();

			$account_debet = Account::find($row->account_id_debet);
			$account_debet->account_balance = $account_debet->account_balance + $row->restock_detail_price;
			$account_debet->save();

			$account_credit = Account::find($row->account_id_credit);
			$account_credit->account_balance = $account_credit->account_balance - $row->restock_detail_price;
			$account_credit->save();
    	}
    }

    public function InsertJournalRestockUnpaid($restock_id, $restock_date) {
    	$restocks = DB::table('restock')
    		->join('restock_detail', 'restock_detail.restock_id', '=', 'restock.restock_id')
    		->where('restock.restock_id', '=', $restock_id)
    		->get();
    	$branches = Branch::find($restocks[0]->branch_id);

    	$account_id_credit = DB::table('account_setting')
    		->where('account_setting_type', '=', 'Debet Kredit Hutang Dagang')
    		->where('branch_id', '=', $restocks[0]->branch_id)
    		->first();

    	foreach($restocks as $row) {
	    	$journal = new Journal();
			$journal->journal_date = $restock_date;
			$journal->journal_debet = $row->restock_detail_price;
			$journal->journal_credit = 0;
			$journal->account_id = $row->account_id_debet;
			$journal->restock_detail_id = $row->restock_detail_id;
			$journal->branch_id = $row->branch_id;
			$journal->store_id = $branches->store_id;
			$journal->save();

			$journal = new Journal();
			$journal->journal_date = $restock_date;
			$journal->journal_debet = 0;
			$journal->journal_credit = $row->restock_detail_price;
			$journal->account_id = $account_id_credit->account_id;
			$journal->restock_detail_id = $row->restock_detail_id;
			$journal->branch_id = $row->branch_id;
			$journal->store_id = $branches->store_id;
			$journal->save();

			$account_debet = Account::find($row->account_id_debet);
			$account_debet->account_balance = $account_debet->account_balance + $row->restock_detail_price;
			$account_debet->save();

			$account_credit = Account::find($account_id_credit->account_id);
			$account_credit->account_balance = $account_credit->account_balance - $row->restock_detail_price;
			$account_credit->save();
    	}
    }

    public function InsertJournalRestockPaid($restock_id, $restock_date) {
    	$restocks = DB::table('restock')
    		->join('restock_detail', 'restock_detail.restock_id', '=', 'restock.restock_id')
    		->where('restock.restock_id', '=', $restock_id)
    		->get();
    	$branches = Branch::find($restocks[0]->branch_id);

    	$account_id_debet = DB::table('account_setting')
    		->where('account_setting_type', '=', 'Debet Kredit Hutang Dagang')
    		->where('branch_id', '=', $restocks[0]->branch_id)
    		->first();

    	foreach($restocks as $row) {
	    	$journal = new Journal();
			$journal->journal_date = $restock_date;
			$journal->journal_debet = $row->restock_detail_price;
			$journal->journal_credit = 0;
			$journal->account_id = $account_id_debet->account_id;
			$journal->restock_detail_id = $row->restock_detail_id;
			$journal->branch_id = $row->branch_id;
			$journal->store_id = $branches->store_id;
			$journal->save();

			$journal = new Journal();
			$journal->journal_date = $restock_date;
			$journal->journal_debet = 0;
			$journal->journal_credit = $row->restock_detail_price;
			$journal->account_id = $row->account_id_credit;
			$journal->restock_detail_id = $row->restock_detail_id;
			$journal->branch_id = $row->branch_id;
			$journal->store_id = $branches->store_id;
			$journal->save();

			$account_debet = Account::find($account_id_debet->account_id);
			$account_debet->account_balance = $account_debet->account_balance + $row->restock_detail_price;
			$account_debet->save();

			$account_credit = Account::find($row->account_id_credit);
			$account_credit->account_balance = $account_credit->account_balance - $row->restock_detail_price;
			$account_credit->save();
    	}
    }

    public function InsertJournalTransfer($request, $transfer_detail_id, $transfer_date, $index) {
    	$branches = Branch::find($request->branch_id);
    	
    	$journal = new Journal();
		$journal->journal_date = $transfer_date;
		$journal->journal_debet = $request->transfer_detail_amount[$index];
		$journal->journal_credit = 0;
		$journal->account_id = $request->account_id_to;
		$journal->transfer_detail_id = $transfer_detail_id;
		$journal->branch_id = $request->branch_id;
		$journal->store_id = $branches->store_id;
		$journal->save();

		$journal = new Journal();
		$journal->journal_date = $transfer_date;
		$journal->journal_debet = 0;
		$journal->journal_credit = $request->transfer_detail_amount[$index];
		$journal->account_id = $request->account_id_from[$index];
		$journal->transfer_detail_id = $transfer_detail_id;
		$journal->branch_id = $request->branch_id;
		$journal->store_id = $branches->store_id;
		$journal->save();

		$account_debet = Account::find($request->account_id_to);
		$account_debet->account_balance = $account_debet->account_balance + $request->transfer_detail_amount[$index];
		$account_debet->save();

		$account_credit = Account::find($request->account_id_from[$index]);
		$account_credit->account_balance = $account_credit->account_balance - $request->transfer_detail_amount[$index];
		$account_credit->save();
    }

    public function InsertJournalOtherTransaction($request, $other_transaction_id) {
    	$journals = DB::table('journal')
    		->where('other_transaction_id', '=', $other_transaction_id)
    		->get();

    	// kalau edit, saldo jurnal dibalikin dulu
    	foreach($journals as $row) {
    		$accounts = Account::find($row->account_id);
    		if($row->journal_debet > 0) {
    			$accounts->account_balance = $accounts->account_balance - $row->journal_debet;
    		} else if($row->journal_credit > 0) {
    			$accounts->account_balance = $accounts->account_balance + $row->journal_credit;
    		}
    		$accounts->save();
    	}

    	$delete = DB::table('journal')
    		->where('other_transaction_id', '=', $other_transaction_id)
    		->delete();

    	$branches = Branch::find($request->branch_id);
    	
    	$journal = new Journal();
		$journal->journal_date = $request->other_transaction_date;
		$journal->journal_debet = $request->other_transaction_amount;
		$journal->journal_credit = 0;
		$journal->account_id = $request->account_id_debet;
		$journal->other_transaction_id = $other_transaction_id;
		$journal->branch_id = $request->branch_id;
		$journal->store_id = $branches->store_id;
		$journal->save();

		$journal = new Journal();
		$journal->journal_date = $request->other_transaction_date;
		$journal->journal_debet = 0;
		$journal->journal_credit = $request->other_transaction_amount;
		$journal->account_id = $request->account_id_credit;
		$journal->other_transaction_id = $other_transaction_id;
		$journal->branch_id = $request->branch_id;
		$journal->store_id = $branches->store_id;
		$journal->save();

		$account_debet = Account::find($request->account_id_debet);
		$account_debet->account_balance = $account_debet->account_balance + $request->other_transaction_amount;
		$account_debet->save();

		$account_credit = Account::find($request->account_id_credit);
		$account_credit->account_balance = $account_credit->account_balance - $request->other_transaction_amount;
		$account_credit->save();
    }
}

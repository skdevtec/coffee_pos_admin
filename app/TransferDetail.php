<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TransferDetail extends Model
{
    protected $table = "transfer_detail";
    protected $primaryKey = "transfer_detail_id";
    public $timestamps = false;

    public function SelectTransferDetail($transfer_id) {
        return $transfer_details = DB::table('transfer')
            ->join('transfer_detail', 'transfer_detail.transfer_id', '=', 'transfer.transfer_id')
            ->where('transfer_detail.transfer_id', '=', $transfer_id)
            ->get();
    }

    public function InsertTransferDetail($request, $transfer_id, $transfer_date) {
    	for($i = 0; $i < count($request->account_id_from); $i++) {
    		$transfer_detail = new TransferDetail();
    		$transfer_detail->transfer_detail_amount = $request->transfer_detail_amount[$i];
    		$transfer_detail->account_id_from = $request->account_id_from[$i];
    		$transfer_detail->account_id_to = $request->account_id_to;
    		$transfer_detail->transfer_id = $transfer_id;
    		$transfer_detail->save();

    		$transfer_detail_id = $transfer_detail->transfer_detail_id;

    		$journal = new Journal();
    		$journal->InsertJournalTransfer($request, $transfer_detail_id, $transfer_date, $i);
    	}
    }
}

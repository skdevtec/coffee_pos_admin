<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $tables = 'users';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'user_name', 'user_phone', 'email', 'password', 'user_role', 'user_delete', 'store_id',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function SelectUser($role = "Admin", $store_id = 0) {
        if($store_id == 0) {
            return $users = DB::table('users')
                ->leftjoin('branch', 'branch.branch_id', '=', 'users.branch_id')
                ->leftjoin('store', 'store.store_id', '=', 'users.store_id')
                ->where('user_role', '=', $role)
                ->where('user_delete', '=', 0)
                ->get();
        } else {
            return $users = DB::table('users')
                ->leftjoin('branch', 'branch.branch_id', '=', 'users.branch_id')
                ->leftjoin('store', 'store.store_id', '=', 'users.store_id')
                ->where('store.store_id', '=', $store_id)
                ->where('user_role', '=', $role)
                ->where('user_delete', '=', 0)
                ->get();
        }
    }

    public function ShowUser($id) {
        return $users = User::find($id);
    }

    public function InsertUser($request, $role) {
        $this->user_name = $request->user_name;
        $this->user_phone = $request->user_phone;
        $this->email = $request->email;
        $this->password = Hash::make($request->password);
        $this->user_role = $role;
        $this->user_point = 0;
        $this->user_delete = 0;
        if(isset($request->branch_id)) {
            $this->branch_id = $request->branch_id;

            $branches = Branch::find($request->branch_id);
            $this->store_id = $branches->store_id;
        } else {
            $this->store_id = $request->store_id;
        }
        $this->save();
    }

    public function UpdateUser($request, $id) {
        $user = User::find($id);
        $user->user_name = $request->user_name;
        $user->user_phone = $request->user_phone;
        $user->email = $request->email;
        if($request->password != "") {
            $user->password = Hash::make($request->password);
        }
        if(isset($request->branch_id)) {
            $user->branch_id = $request->branch_id;

            $branches = Branch::find($request->branch_id);
            $user->store_id = $branches->store_id;
        } else {
            $user->store_id = $request->store_id;
        }
        $user->save();
    }

    public function DeleteUser($id) {
        $user = User::find($id);
        $user->user_delete = 1;
        $user->save();
    }
}

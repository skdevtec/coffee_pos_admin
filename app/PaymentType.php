<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PaymentType extends Model
{
    protected $table = "payment_type";
    protected $primaryKey = "payment_type_id";
    public $timestamps = false;

    public function SelectPaymentType($store_id = 0, $branch_id = 0, $combobox = false) {
        if($store_id == 0 && $branch_id == 0) {
            return $payment_types = DB::table('payment_type')
                ->join('branch', 'branch.branch_id', '=', 'payment_type.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('payment_type_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
        } else if($store_id != 0 && $branch_id == 0) {
            $payment_types = DB::table('payment_type')
                ->join('branch', 'branch.branch_id', '=', 'payment_type.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('store.store_id', '=', $store_id)
                ->where('payment_type_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0);
            if($combobox == true) {
                $payment_types = $payment_types->groupBy('payment_type_name');
            }
            return $payment_types = $payment_types->get();
        } else {
            return $payment_types = DB::table('payment_type')
                ->join('branch', 'branch.branch_id', '=', 'payment_type.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch.branch_id', '=', $branch_id)
                ->where('payment_type_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
        }
    }

    public function ShowPaymentType($id) {
    	return $payment_types = DB::table('payment_type')
            ->join('branch', 'payment_type.branch_id', '=', 'branch.branch_id')
            ->where('payment_type.payment_type_id', $id)
            ->first();
    }

    public function InsertPaymentType($request) {
        if($request->branch_id == "all") {
            $branch = new Branch();
            $branches = $branch->SelectBranch($request->store_id);

            foreach($branches as $row) {
                $payment_type = new PaymentType();
                $payment_type->payment_type_name = $request->payment_type_name;
                $payment_type->payment_type_type = $request->payment_type_type;
                $payment_type->payment_type_delete = 0;
                $payment_type->branch_id = $row->branch_id;
                $payment_type->save();
            }
        } else {
            $this->payment_type_name = $request->payment_type_name;
            $this->payment_type_type = $request->payment_type_type;
            $this->payment_type_delete = 0;
            $this->branch_id = $request->branch_id;
            $this->save();
        }
    }

    public function UpdatePaymentType($request, $id) {
        if($request->branch_id == "all") {
            $payment_type_old = PaymentType::find($id);

            $payment_type_all = DB::table('payment_type')
                ->join('branch', 'payment_type.branch_id', '=', 'branch.branch_id')
                ->where('branch.store_id', $request->store_id)
                ->where('payment_type.payment_type_name', $payment_type_old->payment_type_name)
                ->where('payment_type.payment_type_delete', 0)
                ->get();

            foreach($payment_type_all as $row) {
                $payment_type = PaymentType::find($row->payment_type_id);
                $payment_type->payment_type_name = $request->payment_type_name;
                $payment_type->payment_type_type = $request->payment_type_type;
                $payment_type->save();
            }
        } else {
        	$payment_type = PaymentType::find($id);
        	$payment_type->payment_type_name = $request->payment_type_name;
            $payment_type->payment_type_type = $request->payment_type_type;
        	$payment_type->branch_id = $request->branch_id;
        	$payment_type->save();
        }
    }

    public function DeletePaymentType($id) {
    	$payment_type = PaymentType::find($id);
    	$payment_type->payment_type_delete = 1;
    	$payment_type->save();
    }
}

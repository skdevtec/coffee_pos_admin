<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class HistoryPoint extends Model
{
    protected $table = "history_point";
    protected $primaryKey = "history_point_id";
    public $timestamps = false;

    public function SelectHistoryPoint($id) {
    	return $history_points = DB::table('history_point')
    		->join('transaction', 'history_point.transaction_id', '=', 'transaction.transaction_id')
            ->where('history_point.user_id', '=', $id)
    		->get();
    }
}

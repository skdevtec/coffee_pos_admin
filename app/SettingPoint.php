<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SettingPoint extends Model
{
    protected $table = "setting_point";
    protected $primaryKey = "setting_point_id";
    public $timestamps = false;

    public function SelectSettingPoint($store_id = 0) {
    	if($store_id == 0) {
	    	return $setting_points = DB::table('setting_point')
	    		->join('store', 'store.store_id', '=', 'setting_point.store_id')
	    		->where('setting_point_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	} else {
	    	return $setting_points = DB::table('setting_point')
	    		->join('store', 'store.store_id', '=', 'setting_point.store_id')
	    		->where('store.store_id', '=', $store_id)
	    		->where('setting_point_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowSettingPoint($id) {
    	return $setting_points = SettingPoint::find($id);
    }

    public function InsertSettingPoint($request) {
    	DB::table('setting_point')
    		->where('store_id', '=', $request->store_id)
    		->update(['setting_point_delete' => 1]);

    	$this->setting_point_multiple = $request->setting_point_multiple;
    	$this->setting_point_value = $request->setting_point_value;
    	$this->setting_point_delete = 0;
    	$this->store_id = $request->store_id;
    	$this->save();
    }

    public function UpdateSettingPoint($request, $id) {
    	$setting_point = SettingPoint::find($id);
    	$setting_point->setting_point_multiple = $request->setting_point_multiple;
    	$setting_point->setting_point_value = $request->setting_point_value;
    	$setting_point->store_id = $request->store_id;
    	$setting_point->save();
    }

    public function DeleteSettingPoint($id) {
    	$setting_point = SettingPoint::find($id);
    	$setting_point->setting_point_delete = 1;
    	$setting_point->save();
    }
}

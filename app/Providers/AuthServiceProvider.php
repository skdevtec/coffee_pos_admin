<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('semua_dong', function($user){
            return $user->user_role=='Admin';
        });
        $this->registerPolicies();
        Gate::define('nota_aja', function($user){
            return $user->user_role=='Kasir';
        });
        Passport::routes();
        Passport::tokensExpireIn(now()->addDays(800));

        Passport::refreshTokensExpireIn(now()->addDays(800));

        Passport::personalAccessTokensExpireIn(now()->addMonths(24));
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Transfer extends Model
{
	protected $table = "transfer";
    protected $primaryKey = "transfer_id";
    public $timestamps = false;

    public function SelectTransfer($store_id = 0, $branch_id = 0) {
        if($store_id == 0 && $branch_id == 0) {
            return $transfers = DB::table('transfer')
	    		->join('users', 'users.user_id', '=', 'transfer.user_id')
                ->join('branch', 'branch.branch_id', '=', 'transfer.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch_delete', '=', 0)
                ->get();
        } else if($store_id != 0 && $branch_id == 0) {
            return $transfers = DB::table('transfer')
	    		->join('users', 'users.user_id', '=', 'transfer.user_id')
                ->join('branch', 'branch.branch_id', '=', 'transfer.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('store.store_id', '=', $store_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
        } else {
            return $transfers = DB::table('transfer')
	    		->join('users', 'users.user_id', '=', 'transfer.user_id')
                ->join('branch', 'branch.branch_id', '=', 'transfer.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch.branch_id', '=', $branch_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
        }
    }

    public function ShowTransfer($id) {
        return $transfers = DB::table('transfer')
            ->join('users', 'users.user_id', '=', 'transfer.user_id')
            ->join('branch', 'branch.branch_id', '=', 'transfer.branch_id')
            ->join('store', 'store.store_id', '=', 'branch.store_id')
            ->where('transfer.transfer_id', '=', $id)
            ->first();
    }

    public function InsertTransfer($request) {
    	$transfer_total_amount = 0;
    	for($i = 0; $i < count($request->transfer_detail_amount); $i++) {
    		$transfer_total_amount += $request->transfer_detail_amount[$i];
    	}

    	date_default_timezone_set('Asia/Jakarta');
    	$transfer_date = date('Y-m-d H:i:s');

        $transfers = DB::table('transfer')
            ->where('transfer_date', '>=', date('Y-m-d') . ' 00:00:00')
            ->where('transfer_date', '<=', date('Y-m-d') . ' 23:59:59')
            ->where('branch_id', '=', $request->branch_id)
            ->orderBy('transfer_invoice_number', 'DESC')
            ->first();

        $transfer_invoice_number = date('Ymd');
        if($transfers == null) {
            $transfer_invoice_number .= "0001";
        } else {
            $next = (substr($transfers->transfer_invoice_number, 8, 4) + 1);
            if(strlen($next) == 1) {
                $next = "000" . $next;
            } else if(strlen($next) == 2) {
                $next = "00" . $next;
            } else if(strlen($next) == 3) {
                $next = "0" . $next;
            }

            $transfer_invoice_number .= $next;
        }

        $this->transfer_invoice_number = $transfer_invoice_number;
    	$this->transfer_date = $transfer_date;
    	$this->transfer_message = $request->transfer_message;
    	$this->transfer_total_amount = $transfer_total_amount;
    	$this->user_id = Auth::user()->user_id;
    	$this->branch_id = $request->branch_id;
    	$this->save();

    	$transfer_id = $this->transfer_id;

    	$transfer_detail = new TransferDetail();
    	$transfer_detail->InsertTransferDetail($request, $transfer_id, $transfer_date);
    }
}

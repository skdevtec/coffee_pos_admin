<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class MenuIngredient extends Model
{
    protected $table = "menu_ingredient";
    public $timestamps = false;

    public function SelectMenuIngredient($menu_id = 0) {
    	if($menu_id == 0) {
	    	$menu_ingredients_temp = DB::table('menu_ingredient')
	    		->join('menu', 'menu.menu_id', '=', 'menu_ingredient.menu_id')
	    		->join('ingredient', 'ingredient.ingredient_id', '=', 'menu_ingredient.ingredient_id')
	    		->join('unit', 'unit.unit_id', '=', 'menu_ingredient.unit_id')
                ->where('ingredient_delete', '=', 0)
                ->where('unit_delete', '=', 0)
	    		->get();

	    	$menu_ingredients = [];
	    	foreach ($menu_ingredients_temp as $value) {
	    		$menu_ingredients[$value->menu_id][] = $value;
	    	}
	    	return $menu_ingredients;
    	} else {
	    	return $menu_ingredients = DB::table('menu_ingredient')
                ->join('menu', 'menu.menu_id', '=', 'menu_ingredient.menu_id')
                ->join('ingredient', 'ingredient.ingredient_id', '=', 'menu_ingredient.ingredient_id')
                ->join('unit', 'unit.unit_id', '=', 'menu_ingredient.unit_id')
	    		->where('menu_ingredient.menu_id', '=', $menu_id)
                ->where('ingredient_delete', '=', 0)
                ->where('unit_delete', '=', 0)
	    		->get();
    	}
    }

    public function InsertMenuIngredient($request) {
    	$ingredients = Ingredient::find($request->ingredient_id);

    	$this->menu_id = $request->menu_id;
    	$this->ingredient_id = $request->ingredient_id;
    	$this->menu_ingredient_count = $request->menu_ingredient_count;
    	$this->unit_id = $ingredients->unit_id;
    	$this->save();
    }

    public function DeleteMenuIngredient($menu_id) {
    	DB::table('menu_ingredient')->where('menu_id', '=', $menu_id)->delete();
    }
}

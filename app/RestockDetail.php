<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RestockDetail extends Model
{
    protected $table = "restock_detail";
    protected $primaryKey = "restock_detail_id";
    public $timestamps = false;

    public function SelectRestockDetail($restock_id) {
        return $restock_details = DB::table('restock')
            ->join('restock_detail', 'restock_detail.restock_id', '=', 'restock.restock_id')
            ->join('ingredient', 'ingredient.ingredient_id', '=', 'restock_detail.ingredient_id')
            ->join('unit', 'unit.unit_id', '=', 'restock_detail.unit_id')
            ->where('restock_detail.restock_id', '=', $restock_id)
            ->get();
    }

    public function InsertRestockDetail($request, $restock_id) {
    	for($i = 0; $i < count($request->ingredient_id); $i++) {
    		$ingredients = Ingredient::find($request->ingredient_id[$i]);

    		$restock_detail = new RestockDetail();
    		$restock_detail->restock_detail_price = $request->restock_detail_price[$i];
    		$restock_detail->restock_detail_count = $request->restock_detail_count[$i];
    		$restock_detail->ingredient_id = $request->ingredient_id[$i];
    		$restock_detail->restock_id = $restock_id;
    		$restock_detail->unit_id = $ingredients->unit_id;
    		$restock_detail->save();

    		$restock_detail_id = $restock_detail->restock_detail_id;

            $avg = 0;
            if($ingredients->ingredient_stock == 0) {
                $avg = $request->restock_detail_price[$i] / $request->restock_detail_count[$i];
            } else {
                $last = $ingredients->ingredient_buy_price * $ingredients->ingredient_stock;
                $now = $request->restock_detail_price[$i];
                $avg = ($last + $now) / ($ingredients->ingredient_stock + $request->restock_detail_count[$i]);
            }

            $ingredients->ingredient_buy_price = $avg;
            $ingredients->save();

    		DB::table('ingredient')
    			->where('ingredient_id', '=', $request->ingredient_id[$i])
    			->increment('ingredient_stock', $request->restock_detail_count[$i]);
    	}

        $restock = new Restock();
        $restocks = $restock->ShowRestock($restock_id);
        if($request->restock_payment_status == "Paid") {
            $journal = new Journal();
            $journal->InsertJournalRestock($restock_id, $restocks->restock_date);
        } else if($request->restock_payment_status == "Unpaid") {
            $journal = new Journal();
            $journal->InsertJournalRestockUnpaid($restock_id, $restocks->restock_date);
        }
    }
}

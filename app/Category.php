<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = "category";
    protected $primaryKey = "category_id";
    public $timestamps = false;

    public function SelectCategory($store_id = 0) {
    	if($store_id == 0) {
	    	return $categories = DB::table('category')
	    		->join('store', 'store.store_id', '=', 'category.store_id')
                ->where('store_delete', '=', 0)
	    		->where('category_delete', '=', 0)
	    		->get();
    	} else {
	    	return $categories = DB::table('category')
	    		->join('store', 'store.store_id', '=', 'category.store_id')
	    		->where('store.store_id', '=', $store_id)
	    		->where('category_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowCategory($id) {
    	return $categories = Category::find($id);
    }

    public function InsertCategory($request) {
    	$this->category_name = $request->category_name;
    	$this->category_delete = 0;
    	$this->store_id = $request->store_id;
    	$this->save();
    }

    public function UpdateCategory($request, $id) {
    	$category = Category::find($id);
    	$category->category_name = $request->category_name;
    	$category->store_id = $request->store_id;
    	$category->save();
    }

    public function DeleteCategory($id) {
    	$category = Category::find($id);
    	$category->category_delete = 1;
    	$category->save();
    }
}

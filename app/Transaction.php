<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Transaction extends Model
{
    protected $table = "transaction";
    protected $primaryKey = "transaction_id";
    public $timestamps = false;

    public function SelectTransaction($store_id = 0, $branch_id = 0) {
    	if($store_id == 0 && $branch_id == 0) {
	    	return $ingredients = DB::table('transaction')
	    		->join('users', 'users.user_id', '=', 'transaction.employee_id')
	    		->join('branch', 'branch.branch_id', '=', 'transaction.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('payment_type', 'payment_type.payment_type_id', '=', 'transaction.payment_type_id')
                ->join('transaction_type', 'transaction_type.transaction_type_id', '=', 'transaction.transaction_type_id')
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
        } else if($store_id != 0 && $branch_id == 0) {
            return $ingredients = DB::table('transaction')
	    		->join('users', 'users.user_id', '=', 'transaction.employee_id')
	    		->join('branch', 'branch.branch_id', '=', 'transaction.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('payment_type', 'payment_type.payment_type_id', '=', 'transaction.payment_type_id')
                ->join('transaction_type', 'transaction_type.transaction_type_id', '=', 'transaction.transaction_type_id')
                ->where('store.store_id', '=', $store_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	} else {
            return $ingredients = DB::table('transaction')
	    		->join('users', 'users.user_id', '=', 'transaction.employee_id')
	    		->join('branch', 'branch.branch_id', '=', 'transaction.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('payment_type', 'payment_type.payment_type_id', '=', 'transaction.payment_type_id')
                ->join('transaction_type', 'transaction_type.transaction_type_id', '=', 'transaction.transaction_type_id')
                ->where('branch.branch_id', '=', $branch_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowTransaction($id) {
        return $restocks = DB::table('transaction')
    		->join('users', 'users.user_id', '=', 'transaction.employee_id')
    		->join('branch', 'branch.branch_id', '=', 'transaction.branch_id')
    		->join('store', 'store.store_id', '=', 'branch.store_id')
            ->join('payment_type', 'payment_type.payment_type_id', '=', 'transaction.payment_type_id')
            ->join('transaction_type', 'transaction_type.transaction_type_id', '=', 'transaction.transaction_type_id')
            ->where('transaction.transaction_id', '=', $id)
            ->first();
    }

    public function DeleteTransaction($request, $id) {
        $transaction = Transaction::find($id);

        // select journal
        $journals = DB::table('journal')
            ->join('transaction_detail', 'transaction_detail.transaction_detail_id', '=', 'journal.transaction_detail_id')
            ->join('transaction', 'transaction.transaction_id', '=', 'transaction_detail.transaction_id')
            ->where('transaction.transaction_id', '=', $id)
            ->get();

        foreach($journals as $row) {
            // decrement account balance
            $account = Account::find($row->account_id);
            if($account->payment_type_id != "" && $row->journal_credit != "") {
                // if isset discount
                $account->account_balance = $account->account_balance + $row->journal_credit;
            } else if($row->journal_debet != "") {
                $account->account_balance = $account->account_balance - $row->journal_debet;
            } else if($row->journal_credit != "") {
                $account_setting = DB::table('account_setting')
                    ->where('account_setting_type', '=', 'Kredit Bahan Baku Penjualan')
                    ->where('branch_id', '=', $transaction->branch_id)
                    ->first();

                if($account_setting->account_id == $row->account_id) {
                    // if credit ingredient
                    $account->account_balance = $account->account_balance + $row->journal_credit;
                } else {
                    $account->account_balance = $account->account_balance - $row->journal_credit;
                }
            }
            $account->save();

            // delete journal
            $journal = Journal::find($row->journal_id);
            $journal->delete();
        }

        // select menu ingredient transaction
        $menu_ingredient_transactions = DB::table('menu_ingredient_transaction')
            ->join('transaction_detail', 'transaction_detail.transaction_detail_id', '=', 'menu_ingredient_transaction.transaction_detail_id')
            ->join('transaction', 'transaction.transaction_id', '=', 'transaction_detail.transaction_id')
            ->where('transaction_detail.transaction_id', '=', $id)
            ->select('menu_ingredient_transaction.*')
            ->get();

        foreach($menu_ingredient_transactions as $row) {
            // increment ingredient stock
            $ingredient = Ingredient::find($row->ingredient_id);
            $ingredient_buy_price = (($ingredient->ingredient_stock * $ingredient->ingredient_buy_price) + ($row->menu_ingredient_transaction_count * $row->menu_ingredient_transaction_buy_price)) / ($ingredient->ingredient_stock + $row->menu_ingredient_transaction_count);
            $ingredient->ingredient_buy_price = $ingredient_buy_price;
            $ingredient->ingredient_stock = $ingredient->ingredient_stock + $row->menu_ingredient_transaction_count;
            $ingredient->save();

            // delete menu ingredient
            // $delete_menu_ingredients = DB::table('menu_ingredient_transaction')
            //     ->where('menu_ingredient_transaction_id', '=', $row->menu_ingredient_transaction_id)
            //     ->delete();
        }

        // delete transaction
        $transaction->transaction_delete = 1;
        $transaction->transaction_delete_reason = $request->transaction_delete_reason;
        $transaction->save();
    }
}

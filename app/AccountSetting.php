<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AccountSetting extends Model
{
    protected $table = "account_setting";
    protected $primaryKey = "account_setting_id";
    public $timestamps = false;

    public function SelectAccountSetting($branch_id = 0) {
    	return $account_settings = DB::table('account_setting')
    		->where('branch_id', '=', $branch_id)
    		->get();
    }

    public function InsertAccountSetting($request) {
    	$account_settings = DB::table('account_setting')
    		->where('account_setting_type', '=', $request->account_setting_type)
    		->where('branch_id', '=', $request->branch_id)
    		->delete();

    	$this->account_setting_type = $request->account_setting_type;
    	$this->account_id = $request->account_id;
    	$this->branch_id = $request->branch_id;
    	$this->save();
    }
}

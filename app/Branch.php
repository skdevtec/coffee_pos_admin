<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Branch extends Model
{
    protected $table = "branch";
    protected $primaryKey = "branch_id";
    public $timestamps = false;

    public function SelectBranch($store_id = 0) {
    	if($store_id == 0) {
	    	return $branches = DB::table('branch')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
	    		->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	} else {
	    	return $branches = DB::table('branch')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
	    		->where('store.store_id', '=', $store_id)
	    		->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowBranch($id) {
    	return $branches = Branch::find($id);
    }

    public function InsertBranch($request) {
    	$this->branch_name = $request->branch_name;
    	$this->branch_address = $request->branch_address;
    	$this->branch_delete = 0;
    	$this->store_id = $request->store_id;
    	$this->save();
    }

    public function UpdateBranch($request, $id) {
    	$branch = Branch::find($id);
    	$branch->branch_name = $request->branch_name;
    	$branch->branch_address = $request->branch_address;
    	$branch->store_id = $request->store_id;
    	$branch->save();
    }

    public function DeleteBranch($id) {
    	$branch = Branch::find($id);
    	$branch->branch_delete = 1;
    	$branch->save();
    }
}

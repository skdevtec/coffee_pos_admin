<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TransactionType extends Model
{
    protected $table = "transaction_type";
    protected $primaryKey = "transaction_type_id";
    public $timestamps = false;

    public function SelectTransactionType($store_id = 0) {
    	if($store_id == 0) {
	    	return $transaction_types = DB::table('transaction_type')
	    		->join('store', 'store.store_id', '=', 'transaction_type.store_id')
	    		->where('transaction_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	} else {
	    	return $transaction_types = DB::table('transaction_type')
	    		->join('store', 'store.store_id', '=', 'transaction_type.store_id')
	    		->where('store.store_id', '=', $store_id)
	    		->where('transaction_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowTransactionType($id) {
    	return $transaction_types = TransactionType::find($id);
    }

    public function InsertTransactionType($request) {
    	$this->transaction_type_name = $request->transaction_type_name;
    	$this->transaction_type_charge_absolute = $request->transaction_type_charge_absolute;
    	$this->transaction_type_charge_percentage = $request->transaction_type_charge_percentage;
    	$this->transaction_type_delete = 0;
    	$this->store_id = $request->store_id;
    	$this->save();
    }

    public function UpdateTransactionType($request, $id) {
    	$transaction_type = TransactionType::find($id);
    	$transaction_type->transaction_type_name = $request->transaction_type_name;
    	$transaction_type->transaction_type_charge_absolute = $request->transaction_type_charge_absolute;
    	$transaction_type->transaction_type_charge_percentage = $request->transaction_type_charge_percentage;
    	$transaction_type->store_id = $request->store_id;
    	$transaction_type->save();
    }

    public function DeleteTransactionType($id) {
    	$transaction_type = TransactionType::find($id);
    	$transaction_type->transaction_type_delete = 1;
    	$transaction_type->save();
    }
}

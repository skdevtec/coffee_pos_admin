<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class AccountType extends Model
{
    protected $table = "account_type";
    protected $primaryKey = "account_type_id";
    public $timestamps = false;

    public function SelectAccountType($store_id = 0) {
    	if($store_id == 0) {
	    	return $account_types = DB::table('account_type')
	    		->join('store', 'store.store_id', '=', 'account_type.store_id')
                ->where('account_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	} else {
	    	return $account_types = DB::table('account_type')
	    		->join('store', 'store.store_id', '=', 'account_type.store_id')
	    		->where('store.store_id', '=', $store_id)
                ->where('account_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowAccountType($id) {
    	return $account_types = AccountType::find($id);
    }
    
    public function InsertAccountType($request) {
    	$this->account_type_name = $request->account_type_name;
    	$this->account_type_type = $request->account_type_type;
    	$this->account_type_delete = 0;
    	$this->store_id = $request->store_id;
    	$this->save();
    }

    public function UpdateAccountType($request, $id) {
    	$account_type = AccountType::find($id);
    	$account_type->account_type_name = $request->account_type_name;
    	$account_type->account_type_type = $request->account_type_type;
    	$account_type->store_id = $request->store_id;
    	$account_type->save();
    }

    public function DeleteAccountType($id) {
    	$account_type = AccountType::find($id);
    	$account_type->account_type_delete = 1;
    	$account_type->save();
    }
}

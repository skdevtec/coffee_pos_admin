<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Store extends Model
{
    protected $table = "store";
    protected $primaryKey = "store_id";
    public $timestamps = false;

    public function SelectStore() {
    	return $stores = DB::table('store')
    		->where('store_delete', '=', 0)
    		->get();
    }

    public function ShowStore($id) {
    	return $stores = Store::find($id);
    }

    public function InsertStore($request) {
        $photo = $request->file('store_photo');
        $file_ext = $photo->getClientOriginalExtension();
        $file_name = $request->store_name . '.' . $file_ext;
        $photo->move('images/store/', $file_name);

    	$this->store_name = $request->store_name;
        $this->store_photo = $file_name;
        $this->store_branch_max = $request->store_branch_max;
    	$this->store_delete = 0;
    	$this->save();
    }

    public function UpdateStore($request, $id) {
        $file_name = "";
        if($request->file('store_photo') != null && $request->file('store_photo') != "") {
            $photo = $request->file('store_photo');
            $file_ext = $photo->getClientOriginalExtension();
            $file_name = $request->store_name . '.' . $file_ext;
            $photo->move('images/store/', $file_name);
        }

    	$store = Store::find($id);
    	$store->store_name = $request->store_name;
        $store->store_branch_max = $request->store_branch_max;
        if($file_name != "") {
            $store->store_photo = $file_name;
        }
    	$store->save();
    }

    public function DeleteStore($id) {
    	$store = Store::find($id);
    	$store->store_delete = 1;
    	$store->save();
    }
}

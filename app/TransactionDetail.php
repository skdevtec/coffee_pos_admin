<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TransactionDetail extends Model
{
    protected $table = "transaction_detail";
    protected $primaryKey = "transaction_detail_id";
    public $timestamps = false;

    public function SelectTransactionDetail($transaction_id) {
        return $transaction_details = DB::table('transaction')
            ->join('transaction_detail', 'transaction_detail.transaction_id', '=', 'transaction.transaction_id')
            ->leftjoin('ingredient', 'ingredient.ingredient_id', '=', 'transaction_detail.ingredient_id')
            ->leftjoin('menu', 'menu.menu_id', '=', 'transaction_detail.menu_id')
            ->where('transaction_detail.transaction_id', '=', $transaction_id)
            ->get();
    }
}

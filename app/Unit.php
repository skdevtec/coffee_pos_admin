<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Unit extends Model
{
    protected $table = "unit";
    protected $primaryKey = "unit_id";
    public $timestamps = false;

    public function SelectUnit($store_id = 0) {
    	if($store_id == 0) {
	    	return $unites = DB::table('unit')
	    		->join('store', 'store.store_id', '=', 'unit.store_id')
                ->where('store_delete', '=', 0)
	    		->where('unit_delete', '=', 0)
	    		->get();
    	} else {
	    	return $unites = DB::table('unit')
	    		->join('store', 'store.store_id', '=', 'unit.store_id')
	    		->where('store.store_id', '=', $store_id)
	    		->where('unit_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowUnit($id) {
    	return $unites = Unit::find($id);
    }

    public function InsertUnit($request) {
    	$this->unit_name = $request->unit_name;
    	$this->unit_delete = 0;
    	$this->store_id = $request->store_id;
    	$this->save();
    }

    public function UpdateUnit($request, $id) {
    	$unit = Unit::find($id);
    	$unit->unit_name = $request->unit_name;
    	$unit->store_id = $request->store_id;
    	$unit->save();
    }

    public function DeleteUnit($id) {
    	$unit = Unit::find($id);
    	$unit->unit_delete = 1;
    	$unit->save();
    }
}

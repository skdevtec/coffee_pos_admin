<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Restock extends Model
{
    protected $table = "restock";
    protected $primaryKey = "restock_id";
    public $timestamps = false;

    public function SelectRestock($store_id = 0, $branch_id = 0) {
    	if($store_id == 0 && $branch_id == 0) {
	    	return $ingredients = DB::table('restock')
	    		->join('users', 'users.user_id', '=', 'restock.user_id')
	    		->join('branch', 'branch.branch_id', '=', 'restock.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
        } else if($store_id != 0 && $branch_id == 0) {
            return $ingredients = DB::table('restock')
	    		->join('users', 'users.user_id', '=', 'restock.user_id')
	    		->join('branch', 'branch.branch_id', '=', 'restock.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('store.store_id', '=', $store_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	} else {
            return $ingredients = DB::table('restock')
	    		->join('users', 'users.user_id', '=', 'restock.user_id')
	    		->join('branch', 'branch.branch_id', '=', 'restock.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch.branch_id', '=', $branch_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowRestock($id) {
        return $restocks = DB::table('restock')
            ->join('users', 'users.user_id', '=', 'restock.user_id')
            ->join('branch', 'branch.branch_id', '=', 'restock.branch_id')
            ->join('store', 'store.store_id', '=', 'branch.store_id')
            ->where('restock.restock_id', '=', $id)
            ->first();
    }

    public function InsertRestock($request) {
    	$restock_total_price = 0;
    	for($i = 0; $i < count($request->restock_detail_price); $i++) {
    		$restock_total_price += $request->restock_detail_price[$i];
    	}

    	date_default_timezone_set('Asia/Jakarta');
    	$restock_date = date('Y-m-d H:i:s');

        $restocks = DB::table('restock')
            ->where('restock_date', '>=', date('Y-m-d') . ' 00:00:00')
            ->where('restock_date', '<=', date('Y-m-d') . ' 23:59:59')
            ->where('branch_id', '=', $request->branch_id)
            ->orderBy('restock_invoice_number', 'DESC')
            ->first();

        $restock_invoice_number = date('Ymd');
        if($restocks == null) {
            $restock_invoice_number .= "0001";
        } else {
            $next = (substr($restocks->restock_invoice_number, 8, 4) + 1);
            if(strlen($next) == 1) {
                $next = "000" . $next;
            } else if(strlen($next) == 2) {
                $next = "00" . $next;
            } else if(strlen($next) == 3) {
                $next = "0" . $next;
            }

            $restock_invoice_number .= $next;
        }

        $this->restock_invoice_number = $restock_invoice_number;
    	$this->restock_date = $restock_date;
    	$this->restock_message = $request->restock_message;
        $this->restock_payment_status = $request->restock_payment_status;
    	$this->restock_total_price = $restock_total_price;
    	$this->user_id = Auth::user()->user_id;
    	$this->branch_id = $request->branch_id;
        $this->account_id_debet = $request->account_id_debet;
        $this->account_id_credit = $request->account_id_credit;
    	$this->save();

    	$restock_id = $this->restock_id;

    	$restock_detail = new RestockDetail();
    	$restock_detail->InsertRestockDetail($request, $restock_id);
    }

    public function UpdateRestock($request, $id) {
        $restock = Restock::find($id);
        $restock->restock_payment_status = "Paid";
        $restock->save();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    protected $table = "menu";
    protected $primaryKey = "menu_id";
    public $timestamps = false;

    public function SelectMenu($store_id = 0, $branch_id = 0) {
        if($store_id == 0 && $branch_id == 0) {
            return $menus = DB::table('menu')
                ->join('branch', 'branch.branch_id', '=', 'menu.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('category', 'category.category_id', '=', 'menu.category_id')
                ->where('menu_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->where('category_delete', '=', 0)
                ->get();
        } else if($store_id != 0 && $branch_id == 0) {
            return $menus = DB::table('menu')
                ->join('branch', 'branch.branch_id', '=', 'menu.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('category', 'category.category_id', '=', 'menu.category_id')
                ->where('store.store_id', '=', $store_id)
                ->where('menu_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->where('category_delete', '=', 0)
                ->get();
        } else {
            return $menus = DB::table('menu')
                ->join('branch', 'branch.branch_id', '=', 'menu.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('category', 'category.category_id', '=', 'menu.category_id')
                ->where('branch.branch_id', '=', $branch_id)
                ->where('menu_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->where('category_delete', '=', 0)
                ->get();
        }
    }

    public function ShowMenu($id) {
    	return $menus = DB::table('menu')
            ->join('branch', 'menu.branch_id', '=', 'branch.branch_id')
            ->where('menu.menu_id', $id)
            ->first();
    }

    public function InsertMenu($request) {
        if($request->branch_id == "all") {
            $branch = new Branch();
            $branches = $branch->SelectBranch($request->store_id);

                $first_photo = "";
            foreach($branches as $row) {
                $file_name = "";
                $file_ext = "";
                if($first_photo == "") {
                    $photo = $request->file('menu_photo');
                    $file_ext = $photo->getClientOriginalExtension();
                    $file_name = $request->menu_code . '_' . $row->branch_id . '.' . $file_ext;
                    $photo->move('images/menu/', $file_name);    

                    $first_photo = $file_name;
                } else {
                    $file_name = $request->menu_code . '_' . $row->branch_id . '.' . $file_ext;
                    \File::copy(base_path('public/images/menu/') . $first_photo, base_path('public/images/menu/') . $file_name);
                }

                $menu = new Menu();
                $menu->menu_code = $request->menu_code;
                $menu->menu_name = $request->menu_name;
                $menu->menu_sell_price = $request->menu_sell_price;
                $menu->menu_point_price = $request->menu_point_price;
                $menu->menu_discount = $request->menu_discount;
                $menu->menu_photo = $file_name;
                $menu->menu_delete = 0;
                $menu->category_id = $request->category_id;
                $menu->branch_id = $row->branch_id;
                $menu->save();
            }
        } else {
            $photo = $request->file('menu_photo');
            $file_ext = $photo->getClientOriginalExtension();
            $file_name = $request->menu_code . '_' . $request->branch_id . '.' . $file_ext;
            $photo->move('images/menu/', $file_name);

        	$this->menu_code = $request->menu_code;
        	$this->menu_name = $request->menu_name;
        	$this->menu_sell_price = $request->menu_sell_price;
            $this->menu_point_price = $request->menu_point_price;
            $this->menu_discount = $request->menu_discount;
            $this->menu_photo = $file_name;
        	$this->menu_delete = 0;
            $this->category_id = $request->category_id;
        	$this->branch_id = $request->branch_id;
        	$this->save();
        }
    }

    public function UpdateMenu($request, $id) {
        if($request->branch_id == "all") {
            $menu_old = Menu::find($id);

            $menu_all = DB::table('menu')
                ->join('branch', 'menu.branch_id', '=', 'branch.branch_id')
                ->where('branch.store_id', $request->store_id)
                ->where('menu.menu_code', $menu_old->menu_code)
                ->where('menu.menu_name', $menu_old->menu_name)
                ->where('menu.menu_delete', 0)
                ->where('menu.category_id', $menu_old->category_id)
                ->get();

            foreach($menu_all as $row) {
                $file_name = "";
                if($request->file('menu_photo') != null && $request->file('menu_photo') != "") {
                    $photo = $request->file('menu_photo');
                    $file_ext = $photo->getClientOriginalExtension();
                    $file_name = $request->menu_code . '_' . $row->branch_id . '.' . $file_ext;
                    $photo->move('images/menu/', $file_name);
                }

                $menu = Menu::find($id);
                $menu->menu_code = $request->menu_code;
                $menu->menu_name = $request->menu_name;
                $menu->menu_sell_price = $request->menu_sell_price;
                $menu->menu_point_price = $request->menu_point_price;
                $menu->menu_discount = $request->menu_discount;
                if($file_name != "") {
                    $menu->menu_photo = $file_name;
                }
                $menu->category_id = $request->category_id;
                $menu->save();
            }
        } else {
            $file_name = "";
            if($request->file('menu_photo') != null && $request->file('menu_photo') != "") {
                $photo = $request->file('menu_photo');
                $file_ext = $photo->getClientOriginalExtension();
                $file_name = $request->menu_code . '_' . $request->branch_id . '.' . $file_ext;
                $photo->move('images/menu/', $file_name);
            }

        	$menu = Menu::find($id);
        	$menu->menu_code = $request->menu_code;
        	$menu->menu_name = $request->menu_name;
        	$menu->menu_sell_price = $request->menu_sell_price;
            $menu->menu_point_price = $request->menu_point_price;
            $menu->menu_discount = $request->menu_discount;
            if($file_name != "") {
                $menu->menu_photo = $file_name;
            }
            $menu->category_id = $request->category_id;
        	$menu->branch_id = $request->branch_id;
        	$menu->save();
        }
    }

    public function DeleteMenu($id) {
    	$menu = Menu::find($id);
    	$menu->menu_delete = 1;
    	$menu->save();
    }
}

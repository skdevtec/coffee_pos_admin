<?php

namespace App\Http\Controllers;

use App\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin") {
            $store = new Store();
            $stores = $store->SelectStore();

            return view('store.index', compact('stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Store();
        $insert = $store->InsertStore($request);
        $stores = $store->SelectStore();

        $data['success'] = true;
        $data['data'] = $stores->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = new Store();
        $stores = $store->ShowStore($id);

        $data['success'] = true;
        $data['data'] = $stores->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = new Store();
        $update = $store->UpdateStore($request, $id);
        $stores = $store->SelectStore();

        $data['success'] = true;
        $data['data'] = $stores->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = new Store();
        $delete = $store->DeleteStore($id);
        $stores = $store->SelectStore();

        $data['success'] = true;
        $data['data'] = $stores->toArray();

        return Response::json($data);
    }
}

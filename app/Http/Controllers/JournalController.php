<?php

namespace App\Http\Controllers;

use App\Journal;
use App\Account;
use App\Branch;
use App\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class JournalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('journal.pre_index', compact('branches', 'stores'));
        } else if(Auth::user()->user_role == "Admin") {
            $journal = new Journal();
            $journals = $journal->SelectJournal(Auth::user()->branch_id);

            $branch = new Branch();
            $branches = $branch->ShowBranch(Auth::user()->branch_id);

            $store = new Store();
            $stores = $store->ShowStore($branches->store_id);
            
            $account = new Account();
            $accounts_temp = $account->SelectAccount(0, Auth::user()->branch_id);

            $accounts = [];
            foreach($accounts_temp as $row) {
                $accounts[$row->account_id] = $row;
            }

            return view('journal.index', compact('journals', 'branches', 'stores', 'accounts'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            if(strpos($id, 's') !== false) {
                $id = str_replace('s', '', $id);

                $journal = new Journal();
                $journals = $journal->SelectJournal($id, 0);

                $store = new Store();
                $stores = $store->ShowStore($id);
                    
                $account = new Account();
                $accounts_temp = $account->SelectAccount($id, 0);

                $accounts_library = [];
                $accounts_filter = [];
                foreach($accounts_temp as $row) {
                    $accounts_library[$row->account_id] = $row;

                    if($row->payment_type_name == "") {
                        $accounts_filter[$row->account_name] = $row;
                    } else {
                        $accounts_filter[$row->account_name . ' ' . $row->payment_type_name] = $row;
                    }
                }

                return view('journal.index', compact('journals', 'stores', 'accounts_library', 'accounts_filter'));
            } else {
                $journal = new Journal();
                $journals = $journal->SelectJournal(0, $id);

                $branch = new Branch();
                $branches = $branch->ShowBranch($id);

                $store = new Store();
                $stores = $store->ShowStore($branches->store_id);
                    
                $account = new Account();
                $accounts_temp = $account->SelectAccount(0, $id);

                $accounts_library = [];
                $accounts_filter = [];
                foreach($accounts_temp as $row) {
                    $accounts_library[$row->account_id] = $row;

                    if($row->payment_type_name == "") {
                        $accounts_filter[$row->account_name] = $row;
                    } else {
                        $accounts_filter[$row->account_name . ' ' . $row->payment_type_name] = $row;
                    }
                }

                return view('journal.index', compact('journals', 'branches', 'stores', 'accounts_library', 'accounts_filter'));
            }
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Account;
use App\Branch;
use App\Store;
use App\Transaction;
use App\TransactionDetail;
use App\Restock;
use App\RestockDetail;
use App\Transfer;
use App\TransferDetail;
use App\OtherTransaction;
use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{	
	// faktur penjualan
    public function invoice_transaction($transaction_id) {
    	$transaction = new Transaction();
    	$transactions = $transaction->ShowTransaction($transaction_id);

    	$transaction_detail = new TransactionDetail();
    	$transaction_details = $transaction_detail->SelectTransactionDetail($transaction_id);
    	
    	$branch = new Branch();
    	$branches = $branch->ShowBranch($transactions->branch_id);
    	
    	$store = new Store();
    	$stores = $store->ShowStore($branches->store_id);

    	$pdf = PDF::loadView('pdf.invoice_transaction', compact('transactions', 'transaction_details', 'branches', 'stores'));
    	return $pdf->download('Penjualan_' . $transactions->transaction_invoice_number . '.pdf');
    }

    // faktur penjualan
    public function invoice_restock($restock_id) {
    	$restock = new Restock();
    	$restocks = $restock->ShowRestock($restock_id);

    	$restock_detail = new RestockDetail();
    	$restock_details = $restock_detail->SelectRestockDetail($restock_id);
    	
    	$branch = new Branch();
    	$branches = $branch->ShowBranch($restocks->branch_id);
    	
    	$store = new Store();
    	$stores = $store->ShowStore($branches->store_id);

    	$pdf = PDF::loadView('pdf.invoice_restock', compact('restocks', 'restock_details', 'branches', 'stores'));
    	return $pdf->download('Pembelian_' . str_pad($restocks->restock_id, 5, 0, STR_PAD_LEFT) . '.pdf');
    }

    // faktur transfer saldo akun
    public function invoice_transfer($transfer_id) {
    	$transfer = new Transfer();
    	$transfers = $transfer->ShowTransfer($transfer_id);

    	$transfer_detail = new TransferDetail();
    	$transfer_details = $transfer_detail->SelectTransferDetail($transfer_id);

        $account = new Account();
        $accounts_temp = $account->SelectAccount(0, $transfers->branch_id);
        
        $accounts = [];
        foreach($accounts_temp as $row) {
            $accounts[$row->account_id] = $row;
        }
    	
    	$branch = new Branch();
    	$branches = $branch->ShowBranch($transfers->branch_id);
    	
    	$store = new Store();
    	$stores = $store->ShowStore($branches->store_id);

    	$pdf = PDF::loadView('pdf.invoice_transfer', compact('transfers', 'transfer_details', 'accounts', 'branches', 'stores'));
    	return $pdf->download('Transfer_' . str_pad($transfers->transfer_id, 5, 0, STR_PAD_LEFT) . '.pdf');
    }

    // faktur biaya lain
    public function invoice_other_transaction($other_transaction_id) {
        $other_transaction = new OtherTransaction();
        $other_transactions = $other_transaction->ShowOtherTransaction($other_transaction_id);
        
        $branch = new Branch();
        $branches = $branch->ShowBranch($other_transactions->branch_id);
        
        $store = new Store();
        $stores = $store->ShowStore($branches->store_id);

        $pdf = PDF::loadView('pdf.invoice_other_transaction', compact('other_transactions', 'branches', 'stores'));
        return $pdf->download('BiayaLain_' . $other_transactions->other_transaction_invoice_number . '.pdf');
    }
}

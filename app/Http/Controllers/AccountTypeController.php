<?php

namespace App\Http\Controllers;

use App\AccountType;
use App\Branch;
use App\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class AccountTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $account_type = new AccountType();
            $account_types = [];
            
            $store = new Store();
            $stores = [];
            if(Auth::user()->user_role == "Super Admin") {
                $account_types = $account_type->SelectAccountType();
                $stores = $store->SelectStore();
            } else {
                $account_types = $account_type->SelectAccountType(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('account_type.index', compact('account_types', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * account_type a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account_type = new AccountType();
        $insert = $account_type->InsertAccountType($request);
        $account_types = $account_type->SelectAccountType();

        $data['success'] = true;
        $data['data'] = $account_types->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\account_type  $account_type
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account_type = new AccountType();
        $account_types = $account_type->ShowAccountType($id);

        $data['success'] = true;
        $data['data'] = $account_types->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\account_type  $account_type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\account_type  $account_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account_type = new AccountType();
        $update = $account_type->UpdateAccountType($request, $id);
        $account_types = $account_type->SelectAccountType();

        $data['success'] = true;
        $data['data'] = $account_types->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\account_type  $account_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account_type = new AccountType();
        $delete = $account_type->DeleteAccountType($id);
        $account_types = $account_type->SelectAccountType();

        $data['success'] = true;
        $data['data'] = $account_types->toArray();

        return Response::json($data);
    }

    /**
     * unit a newly created resource in storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function get_account_type($id)
    {
        if($id[0] == "s") {
            $account_type = new AccountType();
            $account_types = $account_type->SelectAccountType(substr($id, 1));
        } else {
            $branch = new Branch();
            $branches = $branch->ShowBranch($id);

            $account_type = new AccountType();
            $account_types = $account_type->SelectAccountType($branches->store_id);
        }

        $data['success'] = true;
        $data['data'] = $account_types->toArray();

        return Response::json($data);
    }
}

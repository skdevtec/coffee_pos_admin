<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Branch;
use App\Store;
use App\Unit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class IngredientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
            $ingredient = new Ingredient();
            $ingredients = [];

            $store = new Store();
            $stores = [];

            $branch = new Branch();
            $branches = [];

            $unit = new Unit();
            $units = [];

            if(Auth::user()->user_role == "Super Admin") {
                $ingredients = $ingredient->SelectIngredient();
                $stores = $store->SelectStore();
                $branches = $branch->SelectBranch();
            } else if(Auth::user()->user_role == "Owner") {
                $ingredients = $ingredient->SelectIngredient(Auth::user()->store_id, 0);
                $units = $unit->SelectUnit(Auth::user()->store_id);
                $branches = $branch->SelectBranch(Auth::user()->store_id);
            } else {
                $ingredients = $ingredient->SelectIngredient(0, Auth::user()->branch_id);
                $units = $unit->SelectUnit(Auth::user()->store_id);
                $branches = $branch->ShowBranch(Auth::user()->branch_id);
            }

            return view('ingredient.index', compact('ingredients', 'stores', 'branches', 'units'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * ingredient a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ingredient = new Ingredient();
        $insert = $ingredient->InsertIngredient($request);
        $ingredients = $ingredient->SelectIngredient();

        $data['success'] = true;
        $data['data'] = $ingredients->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingredient = new Ingredient();
        $ingredients = $ingredient->ShowIngredient($id);

        $data['success'] = true;
        $data['data'] = $ingredients;

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ingredient = new Ingredient();
        $update = $ingredient->UpdateIngredient($request, $id);
        $ingredients = $ingredient->SelectIngredient();

        $data['success'] = true;
        $data['data'] = $ingredients->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingredient = new Ingredient();
        $delete = $ingredient->DeleteIngredient($id);
        $ingredients = $ingredient->SelectIngredient();

        $data['success'] = true;
        $data['data'] = $ingredients->toArray();

        return Response::json($data);
    }

    /**
     * ingredient a newly created resource in storage.
     *
     * @param  \App\ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function get_ingredient($id)
    {
        $ingredient = new Ingredient();
        $ingredients = $ingredient->SelectIngredient(0, $id);

        $data['success'] = true;
        $data['data'] = $ingredients->toArray();

        return Response::json($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Store;
use App\Transaction;
use App\TransactionDetail;
use Illuminate\Http\Request;
use Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('transaction.pre_index', compact('branches', 'stores'));
        } else if(Auth::user()->user_role == "Admin") {
            $transaction = new Transaction();
            $transactions = $transaction->SelectTransaction(0, Auth::user()->branch_id);

            $branch = new Branch();
            $branches = $branch->ShowBranch(Auth::user()->branch_id);

            $store = new Store();
            $stores = $store->ShowStore(Auth::user()->store_id);

            return view('transaction.index', compact('transactions', 'branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(strpos($id, 'dt') !== false) {
            $id = str_replace('dt', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
                $transaction = new Transaction();
                $transactions = $transaction->ShowTransaction($id);

                $transaction_detail = new TransactionDetail();
                $transaction_details = $transaction_detail->SelectTransactionDetail($id);

                return view('transaction.show', compact('transactions', 'transaction_details'));
            } else {
                return redirect()->back();
            }
        } else if(strpos($id, 's') !== false) {
            $id = str_replace('s', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $transaction = new Transaction();
                $transactions = $transaction->SelectTransaction($id, 0);

                $store = new Store();
                $stores = $store->ShowStore($id);

                return view('transaction.index', compact('transactions', 'stores'));
            } else {
                return redirect()->back();
            }
        } else {
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $transaction = new Transaction();
                $transactions = $transaction->SelectTransaction(0, $id);

                $branch = new Branch();
                $branches = $branch->ShowBranch($id);

                $store = new Store();
                $stores = $store->ShowStore($branches->store_id);

                return view('transaction.index', compact('transactions', 'branches', 'stores'));
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = new Transaction();
        $transactions = $transaction->DeleteTransaction($request, $id);

        return "OK";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

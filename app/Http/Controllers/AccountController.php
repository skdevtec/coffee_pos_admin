<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountType;
use App\Store;
use App\Branch;
use App\PaymentType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
            $account = new Account();
            $accounts = [];

            $account_type = new AccountType();
            $account_types = [];

            $payment_type = new PaymentType();
            $payment_types = [];

            $store = new Store();
            $stores = [];

            $branch = new Branch();
            $branches = [];

            if(Auth::user()->user_role == "Super Admin") {
                $accounts = $account->SelectAccount();
                $account_types = $account_type->SelectAccountType();
                $stores = $store->SelectStore();
                $branches = $branch->SelectBranch();
            } else if(Auth::user()->user_role == "Owner") {
                $accounts = $account->SelectAccount(Auth::user()->store_id, 0);
                $account_types = $account_type->SelectAccountType(Auth::user()->store_id);
                $branches = $branch->SelectBranch(Auth::user()->store_id);
            } else {
                $accounts = $account->SelectAccount(0, Auth::user()->branch_id);
                $account_types = $account_type->SelectAccountType(Auth::user()->store_id);
                $branches = $branch->ShowBranch(Auth::user()->branch_id);
                $payment_types = $payment_type->SelectPaymentType(0, Auth::user()->branch_id);
            }

            return view('account.index', compact('accounts', 'account_types', 'stores', 'branches', 'payment_types'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * account a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account = new Account();
        $insert = $account->InsertAccount($request);
        $accounts = $account->SelectAccount();

        $data['success'] = true;
        $data['data'] = $accounts;

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\account  $account
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = new Account();
        $accounts = $account->ShowAccount($id);

        $data['success'] = true;
        $data['data'] = $accounts;

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account = new Account();
        $update = $account->UpdateAccount($request, $id);
        $accounts = $account->SelectAccount();

        $data['success'] = true;
        $data['data'] = $accounts;

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = new Account();
        $delete = $account->DeleteAccount($id);
        $accounts = $account->SelectAccount();

        $data['success'] = true;
        $data['data'] = $accounts;

        return Response::json($data);
    }

    /**
     * account a newly created resource in storage.
     *
     * @param  \App\account  $account
     * @return \Illuminate\Http\Response
     */
    public function get_account($id)
    {
        $account = new Account();
        $accounts = $account->SelectAccount(0, $id);

        $data['success'] = true;
        $data['data'] = $accounts;

        return Response::json($data);
    }
}

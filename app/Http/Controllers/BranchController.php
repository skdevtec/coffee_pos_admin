<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Store;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class BranchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];
            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('branch.index', compact('branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * branch a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $branch = new Branch();
        $insert = $branch->InsertBranch($request);
        $branches = $branch->SelectBranch();

        $data['success'] = true;
        $data['data'] = $branches->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branch = new Branch();
        $branches = $branch->ShowBranch($id);

        $data['success'] = true;
        $data['data'] = $branches->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = new Branch();
        $update = $branch->UpdateBranch($request, $id);
        $branches = $branch->SelectBranch();

        $data['success'] = true;
        $data['data'] = $branches->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = new Branch();
        $delete = $branch->DeleteBranch($id);
        $branches = $branch->SelectBranch();

        $data['success'] = true;
        $data['data'] = $branches->toArray();

        return Response::json($data);
    }

    /**
     * unit a newly created resource in storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function get_branch($id)
    {
        $branch = new Branch();
        $branches = $branch->SelectBranch($id);

        $data['success'] = true;
        $data['data'] = $branches->toArray();

        return Response::json($data);
    }
}

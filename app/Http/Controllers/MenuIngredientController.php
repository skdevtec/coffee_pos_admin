<?php

namespace App\Http\Controllers;

use App\MenuIngredient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class MenuIngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu_ingredient = new MenuIngredient();
        $insert = $menu_ingredient->InsertMenuIngredient($request);

        $data['success'] = true;

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MenuIngredient  $menuIngredient
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu_ingredient = new MenuIngredient();
        $menu_ingredients = $menu_ingredient->SelectMenuIngredient($id);

        $data['success'] = true;
        $data['data'] = $menu_ingredients->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MenuIngredient  $menuIngredient
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MenuIngredient  $menuIngredient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MenuIngredient  $menuIngredient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu_ingredient = new MenuIngredient();
        $insert = $menu_ingredient->DeleteMenuIngredient($id);

        $data['success'] = true;

        return Response::json($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\OtherTransaction;
use App\Store;
use App\Branch;
use App\Account;
use App\Journal;
use Illuminate\Http\Request;
use Response;
use Auth;

class OtherTransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('other_transaction.pre_index', compact('branches', 'stores'));
        } else if(Auth::user()->user_role == "Admin") {
            $other_transaction = new OtherTransaction();
            $other_transactions = $other_transaction->SelectOtherTransaction(0, Auth::user()->branch_id);

            $branch = new Branch();
            $branches = $branch->ShowBranch(Auth::user()->branch_id);

            $store = new Store();
            $stores = $store->ShowStore($branches->store_id);

            $account = new Account();
            $accounts = $account->SelectAccount(0, Auth::user()->branch_id);

            return view('other_transaction.index', compact('other_transactions', 'stores', 'branches', 'accounts'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * other_transaction a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $other_transaction = new OtherTransaction();
        $insert = $other_transaction->InsertOtherTransaction($request);
        $other_transactions = $other_transaction->SelectOtherTransaction();

        $data['success'] = true;
        $data['data'] = $other_transactions->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\other_transaction  $other_transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(strpos($id, 'edit') !== false) {
            $id = str_replace('edit', '', $id);
            $other_transaction = new OtherTransaction();
            $other_transactions = $other_transaction->ShowOtherTransaction($id);

            $journal = new Journal();
            $journals = $journal->ShowJournal($id, 'other_transaction_id');

            $journals_new = [];
            foreach($journals as $row) {
                if($row->journal_debet > 0) {
                    $journals_new['account_id_debet'] = $row->account_id;
                } else if($row->journal_credit > 0) {
                    $journals_new['account_id_credit'] = $row->account_id;
                } 
            }

            $data['success'] = true;
            $data['data'] = $other_transactions->toArray();
            $data['journal'] = $journals_new;

            return Response::json($data);
        } else if(strpos($id, 's') !== false) {
            $id = str_replace('s', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $other_transaction = new OtherTransaction();
                $other_transactions = $other_transaction->SelectOtherTransaction($id, 0);

                $store = new Store();
                $stores = $store->ShowStore($id);

                return view('other_transaction.index', compact('other_transactions', 'stores'));
            } else {
                return redirect()->back();
            }
        } else {
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $other_transaction = new OtherTransaction();
                $other_transactions = $other_transaction->SelectOtherTransaction(0, $id);

                $branch = new Branch();
                $branches = $branch->ShowBranch($id);

                $store = new Store();
                $stores = $store->ShowStore($branches->store_id);

                $account = new Account();
                $accounts = $account->SelectAccount(0, $id);

                return view('other_transaction.index', compact('other_transactions', 'stores', 'branches', 'accounts'));
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\other_transaction  $other_transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\other_transaction  $other_transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $other_transaction = new OtherTransaction();
        $update = $other_transaction->UpdateOtherTransaction($request, $id);
        $other_transactions = $other_transaction->SelectOtherTransaction();

        $data['success'] = true;
        $data['data'] = $other_transactions->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\other_transaction  $other_transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $other_transaction = new OtherTransaction();
        $delete = $other_transaction->DeleteOtherTransaction($id);
        $other_transactions = $other_transaction->SelectOtherTransaction();

        $data['success'] = true;
        $data['data'] = $other_transactions->toArray();

        return Response::json($data);
    }
}

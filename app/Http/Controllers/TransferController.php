<?php

namespace App\Http\Controllers;

use App\Transfer;
use App\TransferDetail;
use App\Store;
use App\Branch;
use App\Account;
use Illuminate\Http\Request;
use Response;
use Auth;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('transfer.pre_index', compact('branches', 'stores'));
        } else if(Auth::user()->user_role == "Admin") {
            $transfer = new Transfer();
            $transfers = $transfer->SelectTransfer(0, Auth::user()->branch_id);

            $branch = new Branch();
            $branches = $branch->ShowBranch(Auth::user()->branch_id);

            $store = new Store();
            $stores = $store->ShowStore($branches->store_id);

            $account = new Account();
            $accounts = $account->SelectAccount(0, Auth::user()->branch_id);

            return view('transfer.index', compact('transfers', 'stores', 'branches', 'accounts'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transfer = new Transfer();
        $inserts = $transfer->InsertTransfer($request);
        $transfers = $transfer->SelectTransfer();

        $data['success'] = true;
        $data['data'] = $transfers->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(strpos($id, 'dt') !== false) {
            $id = str_replace('dt', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
                $transfer = new Transfer();
                $transfers = $transfer->ShowTransfer($id);

                $transfer_detail = new TransferDetail();
                $transfer_details = $transfer_detail->SelectTransferDetail($id);

                $account = new Account();
                $accounts_temp = $account->SelectAccount(0, Auth::user()->branch_id);
                
                $accounts = [];
                foreach($accounts_temp as $row) {
                    $accounts[$row->account_id] = $row;
                }

                return view('transfer.show', compact('transfers', 'transfer_details', 'accounts'));
            } else {
                return redirect()->back();
            }
        } else if(strpos($id, 's') !== false) {
            $id = str_replace('s', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $transfer = new Transfer();
                $transfers = $transfer->SelectTransfer($id, 0);

                $store = new Store();
                $stores = $store->ShowStore($id);

                return view('transfer.index', compact('transfers', 'stores'));
            } else {
                return redirect()->back();
            }
        } else {
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $transfer = new Transfer();
                $transfers = $transfer->SelectTransfer(0, $id);

                $branch = new Branch();
                $branches = $branch->ShowBranch($id);

                $store = new Store();
                $stores = $store->ShowStore($branches->store_id);

                $account = new Account();
                $accounts = $account->SelectAccount(0, $id);

                return view('transfer.index', compact('transfers', 'stores', 'branches', 'accounts'));
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

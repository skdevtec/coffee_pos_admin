<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Store;
use App\Menu;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function neraca_pre() {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('report.neraca_pre', compact('branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    public function neraca($store_id, $branch_id) {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = $branch->ShowBranch($branch_id);

            $store = new Store();
            $stores = $store->ShowStore($store_id);

            // AKTIVA
            $account_aktiva = DB::table('account_type')
                ->where('account_type_type', '=', 'Aktiva')
                ->where('store_id', '=', $store_id)
                ->get();

            $aktiva = [];
            foreach($account_aktiva as $row) {
                if($branch_id == 0) {
                    $aktiva[$row->account_type_id] = DB::table('account')
                        ->join('branch', 'branch.branch_id', '=', 'account.branch_id')
                        ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
                        ->where('account.account_type_id', '=', $row->account_type_id)
                        ->where('branch.store_id', '=', $store_id)
                        ->select('*', DB::raw('SUM(account.account_balance) as account_balance'))
                        ->groupBy('account.account_name', 'payment_type.payment_type_name')
                        ->get();
                } else {
                    $aktiva[$row->account_type_id] = DB::table('account')
                        ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
                        ->where('account.account_type_id', '=', $row->account_type_id)
                        ->where('account.branch_id', '=', $branch_id)
                        ->get();
                }
            }

            // PASIVA
            $account_pasiva = DB::table('account_type')
                ->where('account_type_type', '=', 'Pasiva')
                ->where('store_id', '=', $store_id)
                ->get();

            $pasiva = [];
            foreach($account_pasiva as $row) {
                if($branch_id == 0) {
                    $pasiva[$row->account_type_id] = DB::table('account')
                        ->join('branch', 'branch.branch_id', '=', 'account.branch_id')
                        ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
                        ->where('account.account_type_id', '=', $row->account_type_id)
                        ->where('branch.store_id', '=', $store_id)
                        ->select('*', DB::raw('SUM(account.account_balance) as account_balance'))
                        ->groupBy('account.account_name', 'payment_type.payment_type_name')
                        ->get();
                } else {
                    $pasiva[$row->account_type_id] = DB::table('account')
                        ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
                        ->where('account.account_type_id', '=', $row->account_type_id)
                        ->where('account.branch_id', '=', $branch_id)
                        ->get();
                }
            }

            return view('report.neraca', compact('branches', 'stores', 'account_aktiva', 'aktiva', 'account_pasiva', 'pasiva'));
        } else {
            return redirect()->back();
        }
    }

    public function laba_rugi_pre() {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('report.laba_rugi_pre', compact('branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    public function laba_rugi($store_id, $branch_id) {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = $branch->ShowBranch($branch_id);

            $store = new Store();
            $stores = $store->ShowStore($store_id);

            $account_type_pendapatan = 'Pendapatan';
            $account_type_beban = 'Beban';

            // PENDAPATAN
            $account_pendapatan = DB::table('account')
                ->join('branch', 'branch.branch_id', '=', 'account.branch_id')
                ->join('account_type', 'account_type.account_type_id', '=', 'account.account_type_id')
                ->where('account_type.account_type_name', '=', $account_type_pendapatan);
            if($branch_id == 0) {
                $account_pendapatan = $account_pendapatan->where('branch.store_id', '=', $store_id);
                $account_pendapatan = $account_pendapatan->groupBy('account.account_name');
            } else {
                $account_pendapatan = $account_pendapatan->where('branch.branch_id', '=', $branch_id);
            }
            $account_pendapatan = $account_pendapatan->get();

            $account_pendapatan_report = [];
            foreach($account_pendapatan as $row) {
                $journal = DB::table('journal')
                    ->join('account', 'journal.account_id', '=', 'account.account_id')
                    ->select(DB::raw('SUM(journal.journal_debet) as journal_debet'), DB::raw('SUM(journal.journal_credit) as journal_credit'))
                    ->where('account.account_name', '=', $row->account_name);

                if($branch_id != 0) {
                    $journal = $journal->where('journal.branch_id', '=', $branch_id)
                        ->groupBy('account.account_id');
                } else {
                    $journal = $journal->groupBy('account.account_name');
                }

                if(isset($_GET['min'])) {
                    $journal = $journal->where('journal.journal_date', '>=', $_GET['min'] . " 00:00:00")
                        ->where('journal.journal_date', '<=', $_GET['max'] . " 23:59:59");
                }

                $journal = $journal->first();

                $account_pendapatan_report[$row->account_name]['debet'] = 0;
                $account_pendapatan_report[$row->account_name]['credit'] = 0;

                if($journal != null) {
                    $account_pendapatan_report[$row->account_name]['debet'] = $journal->journal_debet;
                    $account_pendapatan_report[$row->account_name]['credit'] = $journal->journal_credit;
                }
            }

            // BEBAN
            $account_beban = DB::table('account')
                ->join('branch', 'branch.branch_id', '=', 'account.branch_id')
                ->join('account_type', 'account_type.account_type_id', '=', 'account.account_type_id')
                ->where('account_type.account_type_name', '=', $account_type_beban);
            if($branch_id == 0) {
                $account_beban = $account_beban->where('branch.store_id', '=', $store_id);
                $account_beban = $account_beban->groupBy('account.account_name');
            } else {
                $account_beban = $account_beban->where('branch.branch_id', '=', $branch_id);
            }
            $account_beban = $account_beban->get();

            $account_beban_report = [];
            foreach($account_beban as $row) {
                $journal = DB::table('journal')
                    ->join('account', 'journal.account_id', '=', 'account.account_id')
                    ->select(DB::raw('SUM(journal.journal_debet) as journal_debet'), DB::raw('SUM(journal.journal_credit) as journal_credit'))
                    ->where('account.account_name', '=', $row->account_name);

                if($branch_id != 0) {
                    $journal = $journal->where('journal.branch_id', '=', $branch_id)
                        ->groupBy('account.account_id');
                } else {
                    $journal = $journal->groupBy('account.account_name');
                }

                if(isset($_GET['min'])) {
                    $journal = $journal->where('journal.journal_date', '>=', $_GET['min'] . " 00:00:00")
                        ->where('journal.journal_date', '<=', $_GET['max'] . " 23:59:59");
                }

                $journal = $journal->first();

                $account_beban_report[$row->account_name]['debet'] = 0;
                $account_beban_report[$row->account_name]['credit'] = 0;

                if($journal != null) {
                    $account_beban_report[$row->account_name]['debet'] = $journal->journal_debet;
                    $account_beban_report[$row->account_name]['credit'] = $journal->journal_credit;
                }
            }

            return view('report.laba_rugi', compact('branches', 'stores', 'account_pendapatan_report', 'account_beban_report'));
        } else {
            return redirect()->back();
        }
    }

    public function arus_kas_pre() {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('report.arus_kas_pre', compact('branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    public function arus_kas($store_id, $branch_id) {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = $branch->ShowBranch($branch_id);

            $store = new Store();
            $stores = $store->ShowStore($store_id);

            return view('report.arus_kas', compact('branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Store;
use App\Branch;
use App\Menu;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->user_delete == 1) {
            Auth::logout();
            Session::flash('error', 'Anda tidak dapat menggunakan aplikasi.');
            return redirect('login');
        } else {
            $store_id_selected = 0;
            $branch_id_selected = 0;

            if(isset($_GET['sid'])) {
                $store_id_selected = $_GET['sid'];
            }
            if(isset($_GET['bid'])) {
                $branch_id_selected = $_GET['bid'];
            }

            $store = new Store();
            $stores = [];
            
            $branch = new Branch();
            $branches = [];

            if(Auth::user()->user_role == "Super Admin") {
                $stores = $store->SelectStore();
                $branches = $branch->SelectBranch();
            } else if(Auth::user()->user_role == "Owner") {
                $stores = $store->ShowStore(Auth::user()->store_id);
                $branches = $branch->SelectBranch(Auth::user()->store_id);

                $store_id_selected = Auth::user()->store_id;
            } else if(Auth::user()->user_role == "Admin") {
                $stores = $store->ShowStore(Auth::user()->store_id);
                $branches = $branch->ShowBranch(Auth::user()->branch_id);

                $store_id_selected = Auth::user()->store_id;
                $branch_id_selected = Auth::user()->branch_id;
            }

            $menus = [];
            $transaction_details = [];
            $menu_reports = [];

            if($store_id_selected != 0) {
                if($branch_id_selected == 0) {
                    $menu = new Menu();
                    $menus = $menu->SelectMenu($store_id_selected, 0);
                } else {
                    $menu = new Menu();
                    $menus = $menu->SelectMenu(0, $branch_id_selected);
                }

                foreach ($menus as $row) {
                    $transaction_details[$row->menu_id] = DB::table('transaction')
                        ->join('transaction_detail', 'transaction.transaction_id', 'transaction_detail.transaction_detail_id')
                        ->where('transaction_detail.menu_id', '=', $row->menu_id);

                    if(isset($_GET['min'])) {
                        $transaction_details[$row->menu_id] = $transaction_details[$row->menu_id]
                            ->where('transaction.transaction_date', '>=', $_GET['min'] . " 00:00:00")
                            ->where('transaction.transaction_date', '<=', $_GET['max'] . " 23:59:59");
                    }

                    $transaction_details[$row->menu_id] = $transaction_details[$row->menu_id]->get();

                    $menu_reports[$row->menu_id]['quantity'] = 0;
                    $menu_reports[$row->menu_id]['total_price_before_discount'] = 0;
                    $menu_reports[$row->menu_id]['total_discount'] = 0;
                    $menu_reports[$row->menu_id]['total_price_after_discount'] = 0;
                    $menu_reports[$row->menu_id]['capital_price'] = 0;
                    $menu_reports[$row->menu_id]['total_capital_price'] = 0;
                    $menu_reports[$row->menu_id]['profit'] = 0;

                    foreach ($transaction_details[$row->menu_id] as $row_td) {
                        $menu_reports[$row_td->menu_id]['quantity'] += $row_td->transaction_detail_count;
                        $menu_reports[$row_td->menu_id]['total_price_before_discount'] += $row_td->transaction_detail_subtotal;
                        $menu_reports[$row_td->menu_id]['total_discount'] += ($row_td->transaction_detail_subtotal - $row_td->transaction_detail_subtotal_after_discount);
                        $menu_reports[$row_td->menu_id]['total_price_after_discount'] += $row_td->transaction_detail_subtotal_after_discount;
                        $menu_reports[$row_td->menu_id]['capital_price'] = $row_td->transaction_detail_capital_price;
                        $menu_reports[$row_td->menu_id]['total_capital_price'] += ($row_td->transaction_detail_count * $row_td->transaction_detail_capital_price);
                    }

                    $menu_reports[$row->menu_id]['profit'] = $menu_reports[$row->menu_id]['total_price_after_discount'] - $menu_reports[$row->menu_id]['total_capital_price'];
                }

                // $total_transaction_discount = 0;
                // $total_transaction_charge = 0;

                // $transactions = DB::table('transaction');
                // if(isset($_GET['min'])) {
                //     $transactions = $transactions->where('transaction_date', '>=', $_GET['min'] . " 00:00:00")
                //         ->where('transaction_date', '<=', $_GET['max'] . " 23:59:59");
                // }
                // $transactions->get();

                // foreach($transactions as $row) {
                //     $total_transaction_discount
                // }
            }

            return view('home', compact('stores', 'branches', 'menus', 'menu_reports'));   
        }
    }
}

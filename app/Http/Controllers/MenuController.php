<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Store;
use App\Branch;
use App\Category;
use App\MenuIngredient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class MenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
            $menu = new Menu();
            $menus = [];

            $category = new Category();
            $categories = [];

            $menu_ingredient = new MenuIngredient();
            $menu_ingredients = $menu_ingredient->SelectMenuIngredient();

            $store = new Store();
            $stores = [];

            $branch = new Branch();
            $branches = [];

            if(Auth::user()->user_role == "Super Admin") {
                $menus = $menu->SelectMenu();
                $stores = $store->SelectStore();
                $branches = $branch->SelectBranch();
            } else if(Auth::user()->user_role == "Owner") {
                $menus = $menu->SelectMenu(Auth::user()->store_id, 0);
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $categories = $category->SelectCategory(Auth::user()->store_id);
            } else {
                $menus = $menu->SelectMenu(0, Auth::user()->branch_id);
                $branches = $branch->ShowBranch(Auth::user()->branch_id);
                $categories = $category->SelectCategory(Auth::user()->store_id);
            }

            return view('menu.index', compact('menus', 'stores', 'branches', 'categories', 'menu_ingredients'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * menu a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu();
        $insert = $menu->InsertMenu($request);
        $menus = $menu->SelectMenu();

        $data['success'] = true;
        $data['data'] = $menus->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = new Menu();
        $menus = $menu->ShowMenu($id);

        $data['success'] = true;
        $data['data'] = $menus;

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = new Menu();
        $update = $menu->UpdateMenu($request, $id);
        $menus = $menu->SelectMenu();

        $data['success'] = true;
        $data['data'] = $menus->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = new Menu();
        $delete = $menu->DeleteMenu($id);
        $menus = $menu->SelectMenu();

        $data['success'] = true;
        $data['data'] = $menus->toArray();

        return Response::json($data);
    }
}

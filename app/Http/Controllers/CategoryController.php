<?php

namespace App\Http\Controllers;

use App\Category;
use App\Store;
use App\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $category = new Category();
            $categories = [];
            
            $store = new Store();
            $stores = [];
            if(Auth::user()->user_role == "Super Admin") {
                $categories = $category->SelectCategory();
                $stores = $store->SelectStore();
            } else {
                $categories = $category->SelectCategory(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('category.index', compact('categories', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * category a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $insert = $category->InsertCategory($request);
        $categories = $category->SelectCategory();

        $data['success'] = true;
        $data['data'] = $categories->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = new Category();
        $categories = $category->ShowCategory($id);

        $data['success'] = true;
        $data['data'] = $categories->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = new Category();
        $update = $category->UpdateCategory($request, $id);
        $categories = $category->SelectCategory();

        $data['success'] = true;
        $data['data'] = $categories->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = new Category();
        $delete = $category->DeleteCategory($id);
        $categories = $category->SelectCategory();

        $data['success'] = true;
        $data['data'] = $categories->toArray();

        return Response::json($data);
    }

    /**
     * category a newly created resource in storage.
     *
     * @param  \App\category  $category
     * @return \Illuminate\Http\Response
     */
    public function get_Category($id)
    {
        if($id[0] == "s") {
            $category = new Category();
            $categories = $category->SelectCategory(substr($id, 1));
        } else {
            $branch = new Branch();
            $branches = $branch->ShowBranch($id);

            $category = new Category();
            $categories = $category->SelectCategory($branches->store_id);
        }

        $data['success'] = true;
        $data['data'] = $categories->toArray();

        return Response::json($data);
    }
}

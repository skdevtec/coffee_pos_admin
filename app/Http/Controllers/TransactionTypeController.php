<?php

namespace App\Http\Controllers;

use App\TransactionType;
use App\Store;
use Illuminate\Http\Request;
use Response;
use Auth;

class TransactionTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $transaction_type = new TransactionType();
            $transaction_types = [];

            $store = new Store();
            $stores = [];
            if(Auth::user()->user_role == "Super Admin") {
                $transaction_types = $transaction_type->SelectTransactionType();
                $stores = $store->SelectStore();
            } else {
                $transaction_types = $transaction_type->SelectTransactionType(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('transaction_type.index', compact('transaction_types', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * transaction_type a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction_type = new TransactionType();
        $insert = $transaction_type->InsertTransactionType($request);
        $transaction_types = $transaction_type->SelectTransactionType();

        $data['success'] = true;
        $data['data'] = $transaction_types->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TransactionType  $transaction_type
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction_type = new TransactionType();
        $transaction_types = $transaction_type->ShowTransactionType($id);

        $data['success'] = true;
        $data['data'] = $transaction_types->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TransactionType  $transaction_type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TransactionType  $transaction_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction_type = new TransactionType();
        $update = $transaction_type->UpdateTransactionType($request, $id);
        $transaction_types = $transaction_type->SelectTransactionType();

        $data['success'] = true;
        $data['data'] = $transaction_types->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TransactionType  $transaction_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction_type = new TransactionType();
        $delete = $transaction_type->DeleteTransactionType($id);
        $transaction_types = $transaction_type->SelectTransactionType();

        $data['success'] = true;
        $data['data'] = $transaction_types->toArray();

        return Response::json($data);
    }

    /**
     * unit a newly created resource in storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function get_TransactionType($id)
    {
        $transaction_type = new TransactionType();
        $transaction_types = $transaction_type->SelectTransactionType($id);

        $data['success'] = true;
        $data['data'] = $transaction_types->toArray();

        return Response::json($data);
    }
}

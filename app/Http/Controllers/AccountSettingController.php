<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountSetting;
use App\Branch;
use App\Store;
use Illuminate\Http\Request;
use Auth;
use Response;

class AccountSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('account_setting.pre_index', compact('branches', 'stores'));
        } else if(Auth::user()->user_role == "Admin") {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account_setting = new AccountSetting();
        $insert = $account_setting->InsertAccountSetting($request);
        $account_settings = $account_setting->SelectAccountSetting();

        $data['success'] = true;
        $data['data'] = $account_settings->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccountSetting  $accountSetting
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $account_setting = new AccountSetting();
            $account_settings_temp = $account_setting->SelectAccountSetting($id);

            $account_settings = [];
            foreach($account_settings_temp as $row) {
                $account_settings[$row->account_setting_type] = $row;
            }

            $account = new Account();
            $accounts = $account->SelectAccount(0, $id);

            $branch = new Branch();
            $branches = $branch->ShowBranch($id);

            $store = new Store();
            $stores = $store->ShowStore($branches->store_id);

            return view('account_setting.index', compact('account_settings', 'accounts', 'branches', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccountSetting  $accountSetting
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccountSetting  $accountSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountSetting  $accountSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\PaymentType;
use App\Store;
use App\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class PaymentTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
            $payment_type = new PaymentType();
            $payment_types = [];

            $store = new Store();
            $stores = [];

            $branch = new Branch();
            $branches = [];

            if(Auth::user()->user_role == "Super Admin") {
                $payment_types = $payment_type->SelectPaymentType();
                $stores = $store->SelectStore();
                $branches = $branch->SelectBranch();
            } else if(Auth::user()->user_role == "Owner") {
                $payment_types = $payment_type->SelectPaymentType(Auth::user()->store_id, 0);
                $branches = $branch->SelectBranch(Auth::user()->store_id);
            } else {
                $payment_types = $payment_type->SelectPaymentType(0, Auth::user()->branch_id);
                $branches = $branch->ShowBranch(Auth::user()->branch_id);
            }

            return view('payment_type.index', compact('payment_types', 'stores', 'branches'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * payment_type a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment_type = new PaymentType();
        $insert = $payment_type->InsertPaymentType($request);
        $payment_types = $payment_type->SelectPaymentType();

        $data['success'] = true;
        $data['data'] = $payment_types->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\payment_type  $payment_type
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment_type = new PaymentType();
        $payment_types = $payment_type->ShowPaymentType($id);

        $data['success'] = true;
        $data['data'] = $payment_types;

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\payment_type  $payment_type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\payment_type  $payment_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payment_type = new PaymentType();
        $update = $payment_type->UpdatePaymentType($request, $id);
        $payment_types = $payment_type->SelectPaymentType();

        $data['success'] = true;
        $data['data'] = $payment_types->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\payment_type  $payment_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment_type = new PaymentType();
        $delete = $payment_type->DeletePaymentType($id);
        $payment_types = $payment_type->SelectPaymentType();

        $data['success'] = true;
        $data['data'] = $payment_types->toArray();

        return Response::json($data);
    }

    /**
     * payment_type a newly created resource in storage.
     *
     * @param  \App\payment_type  $payment_type
     * @return \Illuminate\Http\Response
     */
    public function get_payment_type($id)
    {
        if($id[0] == "s") {
            $payment_type = new PaymentType();
            $payment_types = $payment_type->SelectPaymentType(substr($id, 1), 0, true);
        } else {
            $payment_type = new PaymentType();
            $payment_types = $payment_type->SelectPaymentType(0, $id);   
        }

        $data['success'] = true;
        $data['data'] = $payment_types->toArray();

        return Response::json($data);
    }
}

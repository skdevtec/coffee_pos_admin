<?php

namespace App\Http\Controllers;

use App\User;
use App\Store;
use App\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class CashierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $user = new User();
            $users = $user->SelectUser("Cashier");

            $store = new Store();
            $stores = [];

            $branch = new Branch();
            $branches = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
            } else {
                $stores = $store->ShowStore(Auth::user()->store_id);
                $branches = $branch->SelectBranch(Auth::user()->user_role);
            }

            return view('cashier.index', compact('users', 'stores', 'branches'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $insert = $user->InsertUser($request, "Cashier");
        $users = $user->SelectUser("Cashier");

        $data['success'] = true;
        $data['data'] = $users->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = new User();
        $users = $user->ShowUser($id);

        $data['success'] = true;
        $data['data'] = $users->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = new User();
        $update = $user->UpdateUser($request, $id);
        $users = $user->SelectUser("Cashier");

        $data['success'] = true;
        $data['data'] = $users->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = new User();
        $delete = $user->DeleteUser($id);
        $users = $user->SelectUser("Cashier");

        $data['success'] = true;
        $data['data'] = $users->toArray();

        return Response::json($data);
    }
}

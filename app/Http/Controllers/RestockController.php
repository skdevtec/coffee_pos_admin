<?php

namespace App\Http\Controllers;

use App\Restock;
use App\RestockDetail;
use App\Ingredient;
use App\Branch;
use App\Store;
use App\Unit;
use App\Account;
use App\Journal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Response;

class RestockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $branch = new Branch();
            $branches = [];

            $store = new Store();
            $stores = [];

            if(Auth::user()->user_role == "Super Admin") {
                $branches = $branch->SelectBranch();
                $stores = $store->SelectStore();

                $branches_temp = [];
                foreach ($branches as $row) {
                    $branches_temp[$row->store_id][] = $row;
                }
                $branches = $branches_temp;
            } else {
                $branches = $branch->SelectBranch(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('restock.pre_index', compact('branches', 'stores'));
        } else if(Auth::user()->user_role == "Admin") {
            $restock = new Restock();
            $restocks = $restock->SelectRestock(0, Auth::user()->branch_id);

            $ingredient = new Ingredient();
            $ingredients = $ingredient->SelectIngredient(0, Auth::user()->branch_id);

            $branch = new Branch();
            $branches = $branch->ShowBranch(Auth::user()->branch_id);

            $unit = new Unit();
            $units = $unit->SelectUnit(Auth::user()->store_id);

            $account = new Account();
            $accounts = $account->SelectAccount(0, Auth::user()->branch_id);

            return view('restock.index', compact('restocks', 'ingredients', 'branches', 'units', 'accounts'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restock = new Restock();
        $inserts = $restock->InsertRestock($request);
        $restocks = $restock->SelectRestock();

        $data['success'] = true;
        $data['data'] = $restocks->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(strpos($id, 'dt') !== false) {
            $id = str_replace('dt', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin") {
                $restock = new Restock();
                $restocks = $restock->ShowRestock($id);

                $restock_detail = new RestockDetail();
                $restock_details = $restock_detail->SelectRestockDetail($id);

                return view('restock.show', compact('restocks', 'restock_details'));
            } else {
                return redirect()->back();
            }
        } else if(strpos($id, 's') !== false) {
            $id = str_replace('s', '', $id);
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $restock = new Restock();
                $restocks = $restock->SelectRestock($id, 0);

                $store = new Store();
                $stores = $store->ShowStore($id);

                return view('restock.index', compact('restocks', 'stores'));
            } else {
                return redirect()->back();
            }
        } else {
            if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
                $restock = new Restock();
                $restocks = $restock->SelectRestock(0, $id);

                $ingredient = new Ingredient();
                $ingredients = $ingredient->SelectIngredient(0, $id);

                $branch = new Branch();
                $branches = $branch->ShowBranch($id);

                $store = new Store();
                $stores = $store->ShowStore($branches->store_id);

                $unit = new Unit();
                $units = $unit->SelectUnit($branches->store_id);

                $account = new Account();
                $accounts = $account->SelectAccount(0, $id);

                return view('restock.index', compact('restocks', 'ingredients', 'stores', 'branches', 'units', 'accounts'));
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $restock = new Restock();
        $restocks = $restock->UpdateRestock($request, $id);

        $date = date('Y-m-d H:i:s');
        $journal = new Journal();
        $journals = $journal->InsertJournalRestockPaid($id, $date);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\SettingPoint;
use App\Store;
use Illuminate\Http\Request;
use Response;
use Auth;

class SettingPointController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $setting_point = new SettingPoint();
            $setting_points = [];

            $store = new Store();
            $stores = [];
            if(Auth::user()->user_role == "Super Admin") {
                $setting_points = $setting_point->SelectSettingPoint();
                $stores = $store->SelectStore();
            } else {
                $setting_points = $setting_point->SelectSettingPoint(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('setting_point.index', compact('setting_points', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * setting_point a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $setting_point = new SettingPoint();
        $insert = $setting_point->InsertSettingPoint($request);
        $setting_points = $setting_point->SelectSettingPoint();

        $data['success'] = true;
        $data['data'] = $setting_points->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SettingPoint  $setting_point
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting_point = new SettingPoint();
        $setting_points = $setting_point->ShowSettingPoint($id);

        $data['success'] = true;
        $data['data'] = $setting_points->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SettingPoint  $setting_point
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SettingPoint  $setting_point
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting_point = new SettingPoint();
        $update = $setting_point->UpdateSettingPoint($request, $id);
        $setting_points = $setting_point->SelectSettingPoint();

        $data['success'] = true;
        $data['data'] = $setting_points->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SettingPoint  $setting_point
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting_point = new SettingPoint();
        $delete = $setting_point->DeleteSettingPoint($id);
        $setting_points = $setting_point->SelectSettingPoint();

        $data['success'] = true;
        $data['data'] = $setting_points->toArray();

        return Response::json($data);
    }

    /**
     * unit a newly created resource in storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function get_SettingPoint($id)
    {
        $setting_point = new SettingPoint();
        $setting_points = $setting_point->SelectSettingPoint($id);

        $data['success'] = true;
        $data['data'] = $setting_points->toArray();

        return Response::json($data);
    }
}

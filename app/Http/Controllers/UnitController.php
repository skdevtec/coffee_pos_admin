<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Store;
use App\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class UnitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner") {
            $unit = new Unit();
            $units = [];
            
            $store = new Store();
            $stores = [];
            if(Auth::user()->user_role == "Super Admin") {
                $units = $unit->SelectUnit();
                $stores = $store->SelectStore();
            } else {
                $units = $unit->SelectUnit(Auth::user()->store_id);
                $stores = $store->ShowStore(Auth::user()->store_id);
            }

            return view('unit.index', compact('units', 'stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * unit a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unit = new Unit();
        $insert = $unit->InsertUnit($request);
        $units = $unit->SelectUnit();

        $data['success'] = true;
        $data['data'] = $units->toArray();

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = new Unit();
        $units = $unit->ShowUnit($id);

        $data['success'] = true;
        $data['data'] = $units->toArray();

        return Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = new Unit();
        $update = $unit->UpdateUnit($request, $id);
        $units = $unit->SelectUnit();

        $data['success'] = true;
        $data['data'] = $units->toArray();

        return Response::json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = new Unit();
        $delete = $unit->DeleteUnit($id);
        $units = $unit->SelectUnit();

        $data['success'] = true;
        $data['data'] = $units->toArray();

        return Response::json($data);
    }

    /**
     * unit a newly created resource in storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function get_unit($id)
    {
        if($id[0] == "s") {
            $unit = new Unit();
            $units = $unit->SelectUnit(substr($id, 1));
        } else {
            $branch = new Branch();
            $branches = $branch->ShowBranch($id);

            $unit = new Unit();
            $units = $unit->SelectUnit($branches->store_id);
        }

        $data['success'] = true;
        $data['data'] = $units->toArray();

        return Response::json($data);
    }
}

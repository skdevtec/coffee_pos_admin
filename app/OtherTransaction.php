<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OtherTransaction extends Model
{
	protected $table = "other_transaction";
    protected $primaryKey = "other_transaction_id";
    public $timestamps = false;

    public function SelectOtherTransaction($store_id = 0, $branch_id = 0) {
        if($store_id == 0 && $branch_id == 0) {
            return $other_transactions = DB::table('other_transaction')
                ->join('branch', 'branch.branch_id', '=', 'other_transaction.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch_delete', '=', 0)
                ->get();
        } else if($store_id != 0 && $branch_id == 0) {
            return $other_transactions = DB::table('other_transaction')
                ->join('branch', 'branch.branch_id', '=', 'other_transaction.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('store.store_id', '=', $store_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
        } else {
            return $other_transactions = DB::table('other_transaction')
                ->join('branch', 'branch.branch_id', '=', 'other_transaction.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->where('branch.branch_id', '=', $branch_id)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
        }
    }

    public function ShowOtherTransaction($id) {
    	return $other_transactions = OtherTransaction::find($id);
    }

    public function InsertOtherTransaction($request) {
        $other_transaction_invoice_number = date('Ymd', strtotime($request->other_transaction_date));
        $other_transactions = DB::table('other_transaction')
            ->where('other_transaction_invoice_number', 'LIKE', "%$other_transaction_invoice_number%")
            ->where('branch_id', '=', $request->branch_id)
            ->orderBy('other_transaction_invoice_number', 'DESC')
            ->first();

        if($other_transactions == null) {
            $other_transaction_invoice_number .= "0001";
        } else {
            $next = (substr($other_transactions->other_transaction_invoice_number, 8, 4) + 1);
            if(strlen($next) == 1) {
                $next = "000" . $next;
            } else if(strlen($next) == 2) {
                $next = "00" . $next;
            } else if(strlen($next) == 3) {
                $next = "0" . $next;
            }

            $other_transaction_invoice_number .= $next;
        }

        $this->other_transaction_invoice_number = $other_transaction_invoice_number;
        $this->other_transaction_date = $request->other_transaction_date;
        $this->other_transaction_description = $request->other_transaction_description;
        $this->other_transaction_amount = $request->other_transaction_amount;
    	$this->branch_id = $request->branch_id;
    	$this->save();

		$journal = new Journal();
		$journal->InsertJournalOtherTransaction($request, $this->other_transaction_id);
    }

    public function UpdateOtherTransaction($request, $id) {
    	$other_transaction = OtherTransaction::find($id);
        $other_transaction->other_transaction_date = $request->other_transaction_date;
        $other_transaction->other_transaction_description = $request->other_transaction_description;
        $other_transaction->other_transaction_amount = $request->other_transaction_amount;
        $other_transaction->branch_id = $request->branch_id;
    	$other_transaction->save();

		$journal = new Journal();
		$journal->InsertJournalOtherTransaction($request, $id);
    }

    public function DeleteOtherTransaction($id) {
        // select journal
        $journals = DB::table('journal')
            ->where('other_transaction_id', '=', $id)
            ->get();

        foreach($journals as $row) {
            // decrement account balance
            $account_balance = 0;
            if($row->journal_debet != "") {
                $account_debet = Account::find($row->account_id);
                $account_debet->account_balance = $account_debet->account_balance - $row->journal_debet;
                $account_debet->save();
            } else if($row->journal_credit != "") {
                $account_credit = Account::find($row->account_id);
                $account_credit->account_balance = $account_credit->account_balance + $row->journal_credit;
                $account_credit->save();
            }

            // delete journal
            $journal = Journal::find($row->journal_id);
            $journal->delete();
        }

    	$other_transaction = OtherTransaction::find($id);
    	$other_transaction->delete();
    }
}

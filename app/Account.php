<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Account extends Model
{
    protected $table = "account";
    protected $primaryKey = "account_id";
    public $timestamps = false;

    public function SelectAccount($store_id = 0, $branch_id = 0) {
        if($store_id == 0 && $branch_id == 0) {
	    	$accounts = DB::table('account')
	    		->join('account_type', 'account_type.account_type_id', '=', 'account.account_type_id')
                ->join('store', 'store.store_id', '=', 'account.store_id')
	    		->leftjoin('branch', 'branch.branch_id', '=', 'account.branch_id')
                ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
	    		->where('account_delete', '=', 0)
                ->where('account_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->orderBy('account.account_name', 'ASC')
                ->orderBy('account.store_id', 'ASC')
                ->orderBy('account.branch_id', 'ASC')
	    		->get();
        } else if($store_id != 0 && $branch_id == 0) {
	    	$accounts = DB::table('account')
	    		->join('account_type', 'account_type.account_type_id', '=', 'account.account_type_id')
                ->join('store', 'store.store_id', '=', 'account.store_id')
	    		->leftjoin('branch', 'branch.branch_id', '=', 'account.branch_id')
                ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
	    		->where('store.store_id', '=', $store_id)
                ->where('account_delete', '=', 0)
                ->where('account_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->orderBy('account.account_name', 'ASC')
                ->orderBy('account.store_id', 'ASC')
                ->orderBy('account.branch_id', 'ASC')
	    		->get();
    	} else {
            $branch = new Branch();
            $branches = $branch->ShowBranch($branch_id);

            $accounts_store_general = DB::table('account')
                ->join('account_type', 'account_type.account_type_id', '=', 'account.account_type_id')
                ->join('store', 'store.store_id', '=', 'account.store_id')
                ->leftjoin('branch', 'branch.branch_id', '=', 'account.branch_id')
                ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
                ->where('store.store_id', '=', $branches->store_id)
                ->where('account_delete', '=', 0)
                ->where('account_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->whereNull('branch.branch_id')
                ->orderBy('account.account_name', 'ASC')
                ->orderBy('account.store_id', 'ASC')
                ->orderBy('account.branch_id', 'ASC')
                ->get()->toArray();

	    	$accounts_branch = DB::table('account')
	    		->join('account_type', 'account_type.account_type_id', '=', 'account.account_type_id')
                ->join('store', 'store.store_id', '=', 'account.store_id')
	    		->leftjoin('branch', 'branch.branch_id', '=', 'account.branch_id')
                ->leftjoin('payment_type', 'payment_type.payment_type_id', '=', 'account.payment_type_id')
	    		->where('branch.branch_id', '=', $branch_id)
                ->where('account_delete', '=', 0)
                ->where('account_type_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->orderBy('account.account_name', 'ASC')
                ->orderBy('account.store_id', 'ASC')
                ->orderBy('account.branch_id', 'ASC')
	    		->get()->toArray();

            $accounts = array_merge($accounts_store_general, $accounts_branch);
    	}

        $accounts_temp = [];
        foreach($accounts as $acc) {
            if($acc->branch_delete == "1" || $acc->payment_type_delete == "1") {

            } else {
                $accounts_temp[] = $acc;
            }
        }
        $accounts = $accounts_temp;

        return $accounts;
    }

    public function ShowAccount($id) {
    	return $accounts = DB::table('account')
            ->where('account.account_id', $id)
            ->first();
    }
    
    public function InsertAccount($request) {
        if($request->branch_id == "all") {
            $branch = new Branch();
            $branches = $branch->SelectBranch($request->store_id);

            foreach($branches as $row) {
                $account = new Account();
                $account->account_name = $request->account_name;
                $account->account_balance = $request->account_balance;
                $account->account_delete = 0;
                $account->account_type_id = $request->account_type_id;
                $account->branch_id = $row->branch_id;
                $account->store_id = $row->store_id;
                if($request->payment_type_id != "") {
                    $payment_type = PaymentType::find($request->payment_type_id);
                    $payment_type_branch = PaymentType::where('payment_type_name', '=', $payment_type->payment_type_name)->where('branch_id', '=', $row->branch_id)->first();
                    if($payment_type_branch != null) {
                        $account->payment_type_id = $payment_type_branch->payment_type_id;
                        $account->save();
                    }
                } else {
                    $account->payment_type_id = null;
                    $account->save();
                }
            }
        } else {
        	$this->account_name = $request->account_name;
            $this->account_balance = $request->account_balance;
        	$this->account_delete = 0;
        	if($request->payment_type_id != "") {
        		$this->payment_type_id = $request->payment_type_id;
        	}
        	$this->account_type_id = $request->account_type_id;
            if($request->branch_id != "") {
                $this->branch_id = $request->branch_id;
            }
            $this->store_id = $request->store_id;
        	$this->save();
        }
    }

    public function UpdateAccount($request, $id) {
        if($request->branch_id == "all") {
            $account_old = Account::find($id);

            $account_all = DB::table('account')
                ->join('branch', 'account.branch_id', '=', 'branch.branch_id')
                ->where('branch.store_id', $request->store_id)
                ->where('account.account_name', $account_old->account_name)
                ->where('account.account_delete', 0)
                ->where('account.account_type_id', $account_old->account_type_id)
                ->where('account.payment_type_id', $account_old->payment_type_id)
                ->get();

            foreach($account_all as $row) {
                $account = Account::find($row->account_id);
                $account->account_name = $request->account_name;
                $account->account_balance = $request->account_balance;
                $account->account_type_id = $request->account_type_id;
                if($request->payment_type_id != "") {
                    $payment_type = PaymentType::find($request->payment_type_id);
                    $payment_type_branch = PaymentType::where('payment_type_name', '=', $payment_type->payment_type_name)->where('branch_id', '=', $row->branch_id)->first();
                    if($payment_type_branch != null) {
                        $account->payment_type_id = $payment_type_branch->payment_type_id;
                        $account->save();
                    }
                } else {
                    $account->payment_type_id = null;
                    $account->save();
                }
            }
        } else {
        	$account = Account::find($id);
        	$account->account_name = $request->account_name;
            $account->account_balance = $request->account_balance;
        	if($request->payment_type_id != "") {
        		$account->payment_type_id = $request->payment_type_id;
        	} else {
                $account->payment_type_id = null;
            }
        	$account->account_type_id = $request->account_type_id;
            if($request->branch_id != "") {
                $account->branch_id = $request->branch_id;
            }
            $account->store_id = $request->store_id;
        	$account->save();
        }
    }

    public function DeleteAccount($id) {
    	$account = Account::find($id);
    	$account->account_delete = 1;
    	$account->save();
    }
}

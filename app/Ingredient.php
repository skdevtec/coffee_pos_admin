<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Ingredient extends Model
{
    protected $table = "ingredient";
    protected $primaryKey = "ingredient_id";
    public $timestamps = false;

    public function SelectIngredient($store_id = 0, $branch_id = 0) {
    	if($store_id == 0 && $branch_id == 0) {
	    	return $ingredients = DB::table('ingredient')
	    		->join('branch', 'branch.branch_id', '=', 'ingredient.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
	    		->join('unit', 'unit.unit_id', '=', 'ingredient.unit_id')
	    		->where('ingredient_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
        } else if($store_id != 0 && $branch_id == 0) {
            return $ingredients = DB::table('ingredient')
                ->join('branch', 'branch.branch_id', '=', 'ingredient.branch_id')
                ->join('store', 'store.store_id', '=', 'branch.store_id')
                ->join('unit', 'unit.unit_id', '=', 'ingredient.unit_id')
                ->where('store.store_id', '=', $store_id)
                ->where('ingredient_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
                ->get();
    	} else {
	    	return $ingredients = DB::table('ingredient')
	    		->join('branch', 'branch.branch_id', '=', 'ingredient.branch_id')
	    		->join('store', 'store.store_id', '=', 'branch.store_id')
	    		->join('unit', 'unit.unit_id', '=', 'ingredient.unit_id')
	    		->where('branch.branch_id', '=', $branch_id)
                ->where('ingredient_delete', '=', 0)
                ->where('branch_delete', '=', 0)
                ->where('store_delete', '=', 0)
	    		->get();
    	}
    }

    public function ShowIngredient($id) {
    	return $ingredients = DB::table('ingredient')
            ->join('branch', 'ingredient.branch_id', '=', 'branch.branch_id')
            ->where('ingredient.ingredient_id', $id)
            ->first();
    }

    public function InsertIngredient($request) {
        if($request->branch_id == "all") {
            $branch = new Branch();
            $branches = $branch->SelectBranch($request->store_id);

            foreach($branches as $row) {
                $ingredient = new Ingredient();
                $ingredient->ingredient_name = $request->ingredient_name;
                $ingredient->ingredient_buy_price = 0;
                $ingredient->ingredient_stock = 0;
                $ingredient->ingredient_delete = 0;
                $ingredient->unit_id = $request->unit_id;
                $ingredient->branch_id = $row->branch_id;
                $ingredient->save();
            }
        } else {
        	$this->ingredient_name = $request->ingredient_name;
        	$this->ingredient_buy_price = 0;
        	$this->ingredient_stock = 0;
        	$this->ingredient_delete = 0;
        	$this->unit_id = $request->unit_id;
        	$this->branch_id = $request->branch_id;
        	$this->save();
        }
    }

    public function UpdateIngredient($request, $id) {
        if($request->branch_id == "all") {
            $ingredient_old = Ingredient::find($id);

            $ingredient_all = DB::table('ingredient')
                ->join('branch', 'ingredient.branch_id', '=', 'branch.branch_id')
                ->where('branch.store_id', $request->store_id)
                ->where('ingredient.ingredient_name', $ingredient_old->ingredient_name)
                ->where('ingredient.ingredient_delete', 0)
                ->where('ingredient.unit_id', $ingredient_old->unit_id)
                ->get();

            foreach($ingredient_all as $row) {
                $ingredient = Ingredient::find($row->ingredient_id);
                $ingredient->ingredient_name = $request->ingredient_name;
                $ingredient->unit_id = $request->unit_id;
                $ingredient->save();
            }
        } else {
        	$ingredient = Ingredient::find($id);
        	$ingredient->ingredient_name = $request->ingredient_name;
        	$ingredient->unit_id = $request->unit_id;
        	$ingredient->branch_id = $request->branch_id;
        	$ingredient->save();
        }
    }

    public function DeleteIngredient($id) {
    	$ingredient = Ingredient::find($id);
    	$ingredient->ingredient_delete = 1;
    	$ingredient->save();
    }
}
<?php $__env->startSection('title','Brand'); ?>

<?php $__env->startSection('heading','Brand'); ?>

<?php $__env->startSection('content'); ?>
  <?php if(Session::has('error')): ?>
   <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('error')); ?></p>
  <?php endif; ?>

  <?php if($message = Session::get('success')): ?>
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
        <strong><?php echo e($message); ?></strong>
    </div>
  <?php endif; ?>

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Daftar Brand</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td><?php echo e($loop->iteration); ?></td>
                    <td><?php echo e($row->store_name); ?></td>
                    <td>
                      <a class="btn btn-primary edit" data-id="<?php echo e($row->store_id); ?>" href="javascript:void(0)"><?php echo e(__('Ubah')); ?></a>
                      <a class="btn btn-danger delete" data-id="<?php echo e($row->store_id); ?>" href="javascript:void(0)"><?php echo e(__('Hapus')); ?></a>
                    </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Brand
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="store_name" class="col-sm-3 col-form-label text-md-right">
              <?php echo e(__('Nama')); ?> <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="store_name" name="store_name" value="">
              <p style="color: red;" id="store_name_feedback">
                <strong>Nama toko wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group">
            <?php echo e(csrf_field()); ?>

            <input type="text" id="store_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data toko ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary"><?php echo e(__('Ya, Hapus')); ?></button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
   <script type="text/javascript">
    $('#store_name_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    $('#submit').click(function() {
      $('#store_name_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var store_name = $('#store_name').val();
        var _token = $('input[name=_token]').val();

        if(store_name == "") {
          $('#store_name_feedback').removeAttr('hidden');
        } else {
          $.ajax({
            url: "<?php echo e(route('toko.store')); ?>",
            method: "POST",  
            data: {
              store_name : store_name, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var store_id = $('#store_id').val();
        var store_name = $('#store_name').val();
        var _token = $('input[name=_token]').val();

        var url = "<?php echo e(route('toko.update', 'store_id')); ?>";
        url = url.replace('store_id', store_id);

        if(store_name == "") {
          $('#store_name_feedback').removeAttr('hidden');
        } else {
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              store_id : store_id, 
              store_name : store_name, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $('.edit').click(function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');

      var store_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "<?php echo e(route('toko.show', 'store_id')); ?>";
      url = url.replace('store_id', store_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          store_id : store_id, 
          _token : _token
        },
        success: function(result) {
          $('#store_id').val(store_id);
          $('#store_name').val(result.data.store_name);
        }
      });
    });

    var store_id = "";
    $('.delete').click(function() {
      store_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "<?php echo e(route('toko.destroy', 'store_id')); ?>";
        url = url.replace('store_id', store_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            store_id : store_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');

      $('#store_id').val('');
      $('#store_name').val('');
    });
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\MBS-PC\Desktop\PATRICK\coffee_pos_admin\resources\views/store/index.blade.php ENDPATH**/ ?>
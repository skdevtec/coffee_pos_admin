@extends('layouts.master')

@section('title','Konversi Poin')

@section('heading','Konversi Poin')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Konversi Poin
                @if(Auth::user()->user_role == "Owner")
                  - {{ $stores->store_name }}
                @endif
              </h6>
            </div>
            @if(Auth::user()->user_role == "Super Admin")
              <div class="col-lg-6 col-xs-6 col-md-6">
                <select class="form-control" id="filter_brand">
                  <option value="">-- Pilih Brand --</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_name }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
              </div>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kelipatan</th>
                  <th>Poin</th>
                  @if(Auth::user()->user_role == "Super Admin")
                    <th>Brand</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($setting_points as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ number_format($row->setting_point_multiple) }}</td>
                    <td>{{ number_format($row->setting_point_value) }}</td>
                    @if(Auth::user()->user_role == "Super Admin")
                      <td>{{ $row->store_name }}</td>
                    @endif
                    <td>
                      <a class="btn btn-primary edit" data-id="{{ $row->setting_point_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                      <a class="btn btn-danger delete" data-id="{{ $row->setting_point_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kelipatan</th>
                  <th>Poin</th>
                  @if(Auth::user()->user_role == "Super Admin")
                    <th>Brand</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Konversi Poin
            @if(Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
              - {{ $stores->store_name }}
            @endif
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <font style="color: red; text-align: center;">*Jika brand Anda telah memiliki 1 konversi poin dan Anda menambahkan konversi poin lagi, maka konversi poin sebelumnya akan terhapus!</font>
          </div>
          <div class="form-group row">
            <label for="setting_point_multiple" class="col-sm-3 col-form-label text-md-right">
              {{ __('Kelipatan') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="setting_point_multiple" name="setting_point_multiple" value="">
              <p style="color: red;" id="setting_point_multiple_feedback">
                <strong>Kelipatan konversi poin wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="setting_point_value" class="col-sm-3 col-form-label text-md-right">
              {{ __('Poin') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="setting_point_value" name="setting_point_value" value="">
              <p style="color: red;" id="setting_point_value_feedback">
                <strong>Poin konversi poin wajib diisi!</strong>
              </p>
            </div>
          </div>
          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="store_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Brand') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="store_id" name="store_id">
                  <option value="">---- Pilih Brand ----</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="store_id_feedback">
                  <strong>Brand wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
            <input type="text" id="store_id" value="{{ Auth::user()->store_id }}" hidden>
          @endif
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="setting_point_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data konversi poin ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#setting_point_multiple_feedback').attr('hidden', 'hidden');
    $('#setting_point_value_feedback').attr('hidden', 'hidden');
    $('#store_id_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');
    
    var table = $('#dataTable').DataTable();
    $('#filter_brand').change(function() {
      table.column(3).search($('#filter_brand').val());
      table.draw();
    });

    $('#submit').click(function() {
      $('#setting_point_multiple_feedback').attr('hidden', 'hidden');
      $('#setting_point_value_feedback').attr('hidden', 'hidden');
      $('#store_id_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var setting_point_multiple = $('#setting_point_multiple').val();
        var setting_point_value = $('#setting_point_value').val();
        var store_id = $('#store_id').val();
        var _token = $('input[name=_token]').val();
        
        if(setting_point_multiple == "") {
          $('#setting_point_multiple_feedback').removeAttr('hidden');
        } else if(setting_point_value == "") {
          $('#setting_point_value_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('konversi_poin.store') }}",
            method: "POST",  
            data: {
              setting_point_multiple : setting_point_multiple, 
              setting_point_value : setting_point_value, 
              store_id : store_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var setting_point_id = $('#setting_point_id').val();
        var setting_point_multiple = $('#setting_point_multiple').val();
        var setting_point_value = $('#setting_point_value').val();
        var store_id = $('#store_id').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('konversi_poin.update', 'setting_point_id') }}";
        url = url.replace('setting_point_id', setting_point_id);

        if(setting_point_multiple == "") {
          $('#setting_point_multiple_feedback').removeAttr('hidden');
        } else if(setting_point_value == "") {
          $('#setting_point_value_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              setting_point_id : setting_point_id, 
              setting_point_multiple : setting_point_multiple, 
              setting_point_value : setting_point_value, 
              store_id : store_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');

      var setting_point_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('konversi_poin.show', 'setting_point_id') }}";
      url = url.replace('setting_point_id', setting_point_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          setting_point_id : setting_point_id, 
          _token : _token
        },
        success: function(result) {
          $('#setting_point_id').val(setting_point_id);
          $('#setting_point_multiple').val(result.data.setting_point_multiple);
          $('#setting_point_value').val(result.data.setting_point_value);
          $('#store_id').val(result.data.store_id);
        }
      });
    });

    var setting_point_id = "";
    $(document).delegate('.delete', 'click', function() {
      setting_point_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('konversi_poin.destroy', 'setting_point_id') }}";
        url = url.replace('setting_point_id', setting_point_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            setting_point_id : setting_point_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');

      $('#setting_point_id').val('');
      $('#setting_point_multiple').val('');
      $('#setting_point_value').val('');
      $('#store_id').val('');
      if("{{ Auth::user()->user_role }}" != "Super Admin") {
        $('#store_id').val("{{ Auth::user()->store_id }}")
      }
    });

    $('#button_add').click();
    if("{{ Auth::user()->user_role }}" != "Super Admin") {
      $('#store_id').val("{{ Auth::user()->store_id }}")
    }
  </script>
@endsection
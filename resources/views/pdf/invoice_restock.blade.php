<center>
  <h2 style="margin-bottom: 0px;">{{ $stores->store_name }} - {{ $branches->branch_name }}</h2>
  <h4 style="margin-top: 5px;">{{ $branches->branch_address }}</h4>
</center>

<br>

<table style="text-align: left;">
  <tr>
    <td style="font-weight: 500;">Nomor Nota</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $restocks->restock_invoice_number }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Tanggal</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ date('d F Y H:i:s', strtotime($restocks->restock_date)) }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Pelaksana</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $restocks->user_name }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Keterangan</td>
    <td>&emsp;:&nbsp;</td>
    <td>
      @if($restocks->restock_message == "")
        -
      @else
        {{ $restocks->restock_message }}
      @endif
    </td>
  </tr>
</table>

<br>

<table border="1" style="width: 100%;" align="center">
  <thead>
    <tr>
      <th>No</th>
      <th>Bahan Baku</th>
      <th>Jumlah</th>
      <th>Harga</th>
    </tr>
  </thead>
  <tbody id="table_data">
    @foreach($restock_details as $row)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $row->ingredient_name }}</td>
        <td>{{ $row->restock_detail_count }} {{ $row->unit_name }}</td>
        <td>IDR {{ number_format($row->restock_detail_price, 0, ',', '.') }}</td>
      </tr>
    @endforeach
    <tr>
      <td style="font-weight: 500; text-align: right;" colspan="3">Grand Total &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($restocks->restock_total_price, 0, ',', '.') }}</td>
    </tr>
  </tbody>
</table>
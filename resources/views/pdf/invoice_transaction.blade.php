<center>
  <h2 style="margin-bottom: 0px;">{{ $stores->store_name }} - {{ $branches->branch_name }}</h2>
  <h4 style="margin-top: 5px;">{{ $branches->branch_address }}</h4>
</center>

<br>

<table style="text-align: left;">
  <tr>
    <td style="font-weight: 500;">Nomor Nota</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $transactions->transaction_invoice_number }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Tanggal</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ date('d F Y H:i:s', strtotime($transactions->transaction_date)) }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Kasir</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $transactions->user_name }}</td>
  </tr>
</table>

<br>

<table border="1" style="width: 100%;" align="center">
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Jumlah</th>
      <th>Harga</th>
      <th>Subtotal</th>
    </tr>
  </thead>
  <tbody id="table_data">
    @foreach($transaction_details as $row)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>
          @if($row->menu_name != "")
            {{ $row->menu_name }}
          @elseif($row->ingredient_name != "")
            {{ $row->ingredient_name }}
          @endif
        </td>
        <td>{{ $row->transaction_detail_count }}</td>
        <td>
          @if($row->transaction_detail_price == $row->transaction_detail_price_after_discount)
            IDR {{ number_format($row->transaction_detail_price, 0, ',', '.') }}
          @else
            <strike>IDR {{ number_format($row->transaction_detail_price, 0, ',', '.') }}</strike><br> IDR {{ number_format($row->transaction_detail_price_after_discount, 0, ',', '.') }}
          @endif
        </td>
        <td>
          @if($row->transaction_detail_subtotal == $row->transaction_detail_subtotal_after_discount)
            IDR {{ number_format($row->transaction_detail_subtotal, 0, ',', '.') }}
          @else
            <strike>IDR {{ number_format($row->transaction_detail_subtotal, 0, ',', '.') }}</strike><br> IDR {{ number_format($row->transaction_detail_subtotal_after_discount, 0, ',', '.') }}
          @endif
        </td>
      </tr>
    @endforeach
    <tr>
      <td style="font-weight: 500; text-align: right;" colspan="4">Total &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($transactions->transaction_total_price, 0, ',', '.') }}</td>
    </tr>
    <tr>
      <td style="font-weight: 500; text-align: right;" colspan="4">Diskon Nota &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($transactions->transaction_discount_absolute, 0, ',', '.') }}</td>
    </tr>
    <tr>
      <td style="font-weight: 500; text-align: right;" colspan="4">Diskon Menu &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($transactions->transaction_total_discount - $transactions->transaction_discount_absolute, 0, ',', '.') }}</td>
    </tr>
    <tr>
      <td style="font-weight: 500; text-align: right;" colspan="4">Grand Total &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($transactions->transaction_total_price - $transactions->transaction_total_discount, 0, ',', '.') }}</td>
    </tr>
  </tbody>
</table>
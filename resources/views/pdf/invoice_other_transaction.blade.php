<center>
  <h2 style="margin-bottom: 0px;">{{ $stores->store_name }} - {{ $branches->branch_name }}</h2>
  <h4 style="margin-top: 5px;">{{ $branches->branch_address }}</h4>
</center>

<br>

<table style="text-align: left;">
  <tr>
    <td style="font-weight: 500;">Nomor Nota</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $other_transactions->other_transaction_invoice_number }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Tanggal</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ date('d F Y H:i:s', strtotime($other_transactions->other_transaction_date)) }}</td>
  </tr>
</table>

<br>

<table border="1" style="width: 100%;" align="center">
  <thead>
    <tr>
      <th>Keterangan</th>
      <th>Harga</th>
    </tr>
  </thead>
  <tbody id="table_data">
    <tr>
      <td>{{ $other_transactions->other_transaction_description }}</td></td>
      <td>IDR {{ number_format($other_transactions->other_transaction_amount, 0, ',', '.') }}</td>
    </tr>
    <tr>
      <td style="font-weight: 500; text-align: right;">Grand Total &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($other_transactions->other_transaction_amount, 0, ',', '.') }}</td>
    </tr>
  </tbody>
</table>
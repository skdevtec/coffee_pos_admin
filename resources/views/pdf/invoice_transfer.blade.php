<center>
  <h2 style="margin-bottom: 0px;">{{ $stores->store_name }} - {{ $branches->branch_name }}</h2>
  <h4 style="margin-top: 5px;">{{ $branches->branch_address }}</h4>
</center>

<br>

<table style="text-align: left;">
  <tr>
    <td style="font-weight: 500;">Nomor Nota</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $transfers->transfer_invoice_number }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Tanggal</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ date('d F Y H:i:s', strtotime($transfers->transfer_date)) }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Pelaksana</td>
    <td>&emsp;:&nbsp;</td>
    <td>{{ $transfers->user_name }}</td>
  </tr>
  <tr>
    <td style="font-weight: 500;">Keterangan</td>
    <td>&emsp;:&nbsp;</td>
    <td>
      @if($transfers->transfer_message == "")
        -
      @else
        {{ $transfers->transfer_message }}
      @endif
    </td>
  </tr>
</table>

<br>

<table border="1" style="width: 100%;" align="center">
  <thead>
    <tr>
      <th>No</th>
      <th>Asal</th>
      <th>Tujuan</th>
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody id="table_data">
    @foreach($transfer_details as $row)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $accounts[$row->account_id_from]->account_name }} {{ $accounts[$row->account_id_from]->payment_type_name }}</td>
        <td>{{ $accounts[$row->account_id_to]->account_name }} {{ $accounts[$row->account_id_to]->payment_type_name }}</td>
        <td>IDR {{ number_format($row->transfer_detail_amount, 0, ',', '.') }}</td>
      </tr>
    @endforeach
    <tr>
      <td style="font-weight: 500; text-align: right;" colspan="3">Grand Total &nbsp;&nbsp;</td>
      <td>IDR {{ number_format($transfers->transfer_total_amount, 0, ',', '.') }}</td>
    </tr>
  </tbody>
</table>
@extends('layouts.master')

@section('title','Pembelian')

@section('heading','Pembelian')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Detail Pembelian {{ $restocks->restock_invoice_number }}</h6>
            </div>
          </div>
        </div>
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
              <center>
                <table>
                  <tr>
                    <th>Tanggal</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ date('d F Y H:i:s', strtotime($restocks->restock_date)) }}</td>
                  </tr>
                  <tr>
                    <th>Pelaksana</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ $restocks->user_name }}</td>
                  </tr>
                  <tr>
                    <th>Total</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>IDR {{ number_format($restocks->restock_total_price, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <th>Keterangan</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>
                      @if($restocks->restock_message == "")
                        -
                      @else
                        {{ $restocks->restock_message }}
                      @endif
                    </td>
                  </tr>
                </table>
              </center>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Bahan Baku</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($restock_details as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->ingredient_name }}</td>
                    <td>{{ $row->restock_detail_count }} {{ $row->unit_name }}</td>
                    <td>IDR {{ number_format($row->restock_detail_price, 0, ',', '.') }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
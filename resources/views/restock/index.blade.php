@extends('layouts.master')

@section('title','Pembelian')

@section('heading','Pembelian')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Pembelian -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="" method="GET">
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">&nbsp;</label>
                          <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                          @if(isset($branches))
                            &nbsp;<a class="btn btn-danger" href="{{ url('/pembelian/' . $branches->branch_id) }}">Hapus Filter</a>
                          @else
                            &nbsp;<a class="btn btn-danger" href="{{ url('/pembelian/s' . $stores->store_id) }}">Hapus Filter</a>
                          @endif
                      </div>
                  </div>
              </div>
          </form>

          <div class="table-responsive">
            <input type="hidden" value="{{ $total_price = 0 }}">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th style="width: 150px;">Total</th>
                  <th>Keterangan</th>
                  <th>Pelaksana</th>
                  <th>Status</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($restocks as $row)
                  @if(isset($_GET['min']))
                    @if(strtotime($_GET['min'] . " 00:00:00") <= strtotime($row->restock_date) && strtotime($_GET['max'] . " 23:59:59") >= strtotime($row->restock_date))
                      <input type="hidden" value="{{ $total_price += $row->restock_total_price }}">
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ date('d/m/Y H:i:s', strtotime($row->restock_date)) }}</td>
                        <td>{{ $row->restock_invoice_number }}</td>
                        <td>IDR {{ number_format($row->restock_total_price, 0, ',', '.') }}</td>
                        <td>
                          @if($row->restock_message == "")
                            -
                          @else
                            {{ $row->restock_message }}
                          @endif
                        </td>
                        <td>{{ $row->user_name }}</td>
                        <td>
                          @if($row->restock_payment_status == "Paid")
                            <span style="color: green;">Lunas</span>
                          @elseif($row->restock_payment_status == "Unpaid")
                            <span style="color: red;">Belum Lunas</span>
                            <a class="btn btn-warning payment_update" data-id="{{ $row->restock_id }}" href="javascript:void(0)">{{ __('Lunas') }}</a>
                          @endif
                        </td>
                        @if(!isset($branches))
                          <td>{{ $row->branch_name }}</td>
                        @endif
                        <td>
                          <a class="btn btn-primary" href="{{ url('invoice_download/pembelian/' . $row->restock_id) }}">{{ __('Unduh') }}</a>
                          <a class="btn btn-success" href="{{ route('pembelian.show', 'dt' . $row->restock_id) }}">Lihat</a>
                        </td>
                      </tr>
                    @endif
                  @else
                    <input type="hidden" value="{{ $total_price += $row->restock_total_price }}">
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ date('d/m/Y H:i:s', strtotime($row->restock_date)) }}</td>
                      <td>{{ $row->restock_invoice_number }}</td>
                      <td>IDR {{ number_format($row->restock_total_price, 0, ',', '.') }}</td>
                      <td>
                        @if($row->restock_message == "")
                          -
                        @else
                          {{ $row->restock_message }}
                        @endif
                      </td>
                      <td>{{ $row->user_name }}</td>
                      <td>
                        @if($row->restock_payment_status == "Paid")
                          <span style="color: green;">Lunas</span>
                        @elseif($row->restock_payment_status == "Unpaid")
                          <span style="color: red;">Belum Lunas</span>
                          <a class="btn btn-warning payment_update" data-id="{{ $row->restock_id }}" href="javascript:void(0)">{{ __('Lunas') }}</a>
                        @endif
                      </td>
                      @if(!isset($branches))
                        <td>{{ $row->branch_name }}</td>
                      @endif
                      <td>
                        <a class="btn btn-primary" href="{{ url('invoice_download/pembelian/' . $row->restock_id) }}">{{ __('Unduh') }}</a>
                        <a class="btn btn-success" href="{{ route('pembelian.show', 'dt' . $row->restock_id) }}">Lihat</a>
                      </td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th>Pelaksana</th>
                  <th>Status</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <h4>&nbsp;</h4>
          <table>
            <tr>
              <td>Total Pembelian</td>
              <td>&emsp;:&nbsp;</td>
              <td>{{ number_format($total_price, 0, ',', '.') }}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>

    @if(isset($branches))
      <div class="col-lg-12">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
              Tambah Pembelian -
              @if(Auth::user()->user_role == "Super Admin")
                {{ $stores->store_name }}
              @endif
              {{ $branches->branch_name }}
              <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
            </h6>
          </div>
          <div class="card-body">
            <input type="text" id="branch_id" value="{{ $branches->branch_id }}" hidden>
            <div class="form-group row">
              <label for="account_id_debet" class="col-sm-4 col-form-label text-md-right">
                {{ __('Debet') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-6">
                <select class="form-control" id="account_id_debet" name="account_id_debet">
                  <option value="">---- Pilih Akun ----</option>
                  @foreach($accounts as $row)
                    <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="account_id_debet_feedback">
                  <strong>Akun debet wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="account_id_credit" class="col-sm-4 col-form-label text-md-right">
                {{ __('Kredit') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-6">
                <select class="form-control" id="account_id_credit" name="account_id_credit">
                  <option value="">---- Pilih Akun ----</option>
                  @foreach($accounts as $row)
                    <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="account_id_credit_feedback">
                  <strong>Akun kredit wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="restock_payment_status" class="col-sm-4 col-form-label text-md-right">
                {{ __('Status') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-6">
                <input type="radio" name="restock_payment_status" value="Paid"> Lunas &emsp;
                <input type="radio" name="restock_payment_status" value="Unpaid"> Belum Lunas
                <p style="color: red;" id="restock_payment_status_feedback">
                  <strong>Status pembayaran wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="restock_message" class="col-sm-4 col-form-label text-md-right">
                {{ __('Keterangan') }}
              </label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="restock_message" name="restock_message">
              </div>
            </div>
            <div id="body_restock_detail">
              <div class="form-group row">
                <div class="col-sm-1">
                </div>
                <div class="col-sm-3">
                  <select class="form-control ingredient_id" data-count="0" id="ingredient_id0" name="ingredient_id">
                    <option value="">-- Pilih Bahan Baku --</option>
                    @foreach($ingredients as $row)
                      <option value="{{ $row->ingredient_id }}">{{ $row->ingredient_name }}</option>
                    @endforeach
                  </select>
                  <p style="color: red;" id="ingredient_id0_feedback">
                    <strong>Bahan baku wajib dipilih!</strong>
                  </p>
                </div>
                <div class="col-sm-2">
                  <input type="number" class="form-control" id="restock_detail_count0" name="restock_detail_count" placeholder="Quantity">
                  <p style="color: red;" id="restock_detail_count0_feedback">
                    <strong>Jumlah wajib diisi!</strong>
                  </p>
                </div>
                <div class="col-sm-1">
                  <span id="ingredient_name0">**</span>
                </div>
                <div class="col-sm-3">
                  <input type="number" class="form-control" id="restock_detail_price0" name="restock_detail_price" placeholder="Price">
                  <p style="color: red;" id="restock_detail_price0_feedback">
                    <strong>Harga wajib diisi!</strong>
                  </p>
                </div>
                <div class="col-sm-1">
                  <button class="btn btn-success" id="add_restock_detail">+</button>
                </div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" id="menu_id_restock_detail" value="" hidden>
              <center>
                <button type="button" class="btn btn-primary" id="submit_restock_detail">Simpan</button>
              </center>
            </div>
          </div>
        </div>
      </div>
    @endif
  </div>

  <div class="modal fade" id="modal_payment_update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pelunasan</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin melunasi pembelian ini?
        </div>
        <div class="modal-footer">
          <button id="button_payment_update" class="btn btn-secondary">{{ __('Ya, Lunas') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    var table = $('#dataTable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4 ]
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4 ]
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
      ],
    }); 

    $('#account_id_debet_feedback').attr('hidden', 'hidden');
    $('#account_id_credit_feedback').attr('hidden', 'hidden');
    $('#ingredient_id0_feedback').attr('hidden', 'hidden');
    $('#restock_detail_count0_feedback').attr('hidden', 'hidden');
    $('#restock_detail_price0_feedback').attr('hidden', 'hidden');
    $('#restock_payment_status_feedback').attr('hidden', 'hidden');

    var array_restock_detail_count = new Array();
    array_restock_detail_count.push(0);
    var branch_id = $('#branch_id').val();
    var count = 0;

    var array_ingredient = new Array();
    var url = "{{ url('/get_ingredient/branch_id') }}";
    url = url.replace('branch_id', branch_id);

    $.ajax({
      url: url,
      method: "GET", 
      data: {

      },
      success: function(result) {
        array_ingredient = result.data;
      },
      error: function(xhr) {
        console.log(xhr);
      }
    });

    $('#add_restock_detail').click(function() {   
      count++;
      array_restock_detail_count.push(count);
      $('#body_restock_detail').append(`
        <div class="form-group row" id="list_restock_detail` + count + `">
          <div class="col-sm-1">
          </div>
          <div class="col-sm-3">
            <select class="form-control ingredient_id" data-count="` + count + `" id="ingredient_id` + count + `" name="ingredient_id">
              <option value="">-- Pilih Bahan Baku --</option>
            </select>
            <p style="color: red;" id="ingredient_id` + count + `_feedback">
              <strong>Bahan baku wajib dipilih!</strong>
            </p>
          </div>
          <div class="col-sm-2">
            <input type="number" class="form-control" id="restock_detail_count` + count + `" name="restock_detail_count" placeholder="Quantity">
            <p style="color: red;" id="restock_detail_count` + count + `_feedback">
              <strong>Jumlah wajib diisi!</strong>
            </p>
          </div>
          <div class="col-sm-1">
            <span id="ingredient_name` + count + `">**</span>
          </div>
          <div class="col-sm-3">
            <input type="number" class="form-control" id="restock_detail_price` + count + `" name="restock_detail_price" placeholder="Price">
            <p style="color: red;" id="restock_detail_price` + count + `_feedback">
              <strong>Harga wajib diisi!</strong>
            </p>
          </div>
          <div class="col-sm-1">
            <button class="btn btn-danger" onclick="remove_restock_detail('` + count + `');">-</button>
          </div>
        </div>
      `);

      $('#ingredient_id' + count + '_feedback').attr('hidden', 'hidden');
      $('#restock_detail_count' + count + '_feedback').attr('hidden', 'hidden');
      $('#restock_detail_price' + count + '_feedback').attr('hidden', 'hidden');
      
      $('#ingredient_id' + count).html(``);
      $('#ingredient_id' + count).append(`<option value="">-- Pilih Bahan Baku --</option>`);

      $.each(array_ingredient, function(i,index){
        $('#ingredient_id' + count).append(`
            <option value="` + array_ingredient[i]['ingredient_id'] + `">` + array_ingredient[i]['ingredient_name'] + `</option>
          `);
      });

      $('.ingredient_id').change(function() {
        var count = $(this).data('count');

        var ingredient_id = $('#ingredient_id' + count).val();
        $.each(array_ingredient, function(i,index){
          if(array_ingredient[i]['ingredient_id'] == ingredient_id) {
            $('#ingredient_name' + count).html(array_ingredient[i]['unit_name']);
          }
        });
      });
    });

    $('.ingredient_id').change(function() {
      var count = $(this).data('count');

      var ingredient_id = $('#ingredient_id' + count).val();
      $.each(array_ingredient, function(i,index){
        if(array_ingredient[i]['ingredient_id'] == ingredient_id) {
          $('#ingredient_name' + count).html(array_ingredient[i]['unit_name']);
        }
      });
    });

    function remove_restock_detail(id) {
      var array_restock_detail_count_temp = new Array();
      for(var i = 0; i < array_restock_detail_count.length; i++) {
        if(array_restock_detail_count[i] != id) {
          array_restock_detail_count_temp.push(array_restock_detail_count[i]);
        }
      }
      array_restock_detail_count = array_restock_detail_count_temp;

      $('#list_restock_detail' + id).remove();
    }

    $('#submit_restock_detail').click(function() {
      $('#account_id_debet_feedback').attr('hidden', 'hidden');
      $('#account_id_credit_feedback').attr('hidden', 'hidden');
      $('#restock_payment_status_feedback').attr('hidden', 'hidden');
      for(i = 0; i <= count; i++) {
        $('#ingredient_id' + i + '_feedback').attr('hidden', 'hidden');
        $('#restock_detail_count' + i + '_feedback').attr('hidden', 'hidden');
        $('#restock_detail_price' + i + '_feedback').attr('hidden', 'hidden');
      }

      var account_id_debet = $('#account_id_debet').val();
      var account_id_credit = $('#account_id_credit').val();
      var restock_payment_status = $('input[type=radio][name=restock_payment_status]:checked').val();
      var restock_message = $('#restock_message').val();

      var _token = $('input[name=_token]').val();

      if(account_id_debet == "") {
        $('#account_id_debet_feedback').removeAttr('hidden');
      } else if(account_id_credit == "") {
        $('#account_id_credit_feedback').removeAttr('hidden');
      } else if(restock_payment_status == "" || restock_payment_status == null || restock_payment_status == undefined) {
        $('#restock_payment_status_feedback').removeAttr('hidden');
      } else {
        var status = true;
        var array_ingredient_restock = new Array();
        var array_restock_detail_count_restock = new Array();
        var array_restock_detail_price_restock = new Array();
        for(i = 0; i <= count; i++) {
          var ingredient_id = $('#ingredient_id' + i).val();
          array_ingredient_restock.push(ingredient_id);
          var restock_detail_count = $('#restock_detail_count' + i).val();
          array_restock_detail_count_restock.push(restock_detail_count);
          var restock_detail_price = $('#restock_detail_price' + i).val();
          array_restock_detail_price_restock.push(restock_detail_price);

          if(ingredient_id !== undefined) {
            if(ingredient_id == "") {
              $('#ingredient_id' + i + '_feedback').removeAttr('hidden');
              status = false;
            }
            if(restock_detail_count == 0 || restock_detail_count == "") {
              $('#restock_detail_count' + i + '_feedback').removeAttr('hidden');
              status = false;
            }
            if(restock_detail_price == 0 || restock_detail_price == "") {
              $('#restock_detail_price' + i + '_feedback').removeAttr('hidden');
              status = false;
            }
          }
        }

        if(status === true) {  
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('pembelian.store') }}",
            method: "POST",  
            data: {
              branch_id : branch_id, 
              account_id_debet : account_id_debet, 
              account_id_credit : account_id_credit, 
              restock_payment_status : restock_payment_status,
              restock_message : restock_message,
              ingredient_id : array_ingredient_restock, 
              restock_detail_count : array_restock_detail_count_restock, 
              restock_detail_price : array_restock_detail_price_restock, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            },
            error: function(xhr) {
              console.log(xhr);
            }
          });
        }
      }
    });

    var restock_id = "";
    $(document).delegate('.payment_update', 'click', function() {
      restock_id = $(this).data('id');

      $('#modal_payment_update').modal("show");
      $('#button_payment_update').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('pembelian.update', 'restock_id') }}";
        url = url.replace('restock_id', restock_id);

        $.ajax({
          url: url,
          method: "PUT",  
          data: {
            restock_id : restock_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#account_id_debet').val('');
    $('#account_id_credit').val('');
    $('#restock_message').val('');
    $('#ingredient_id0').val('');
    $('#restock_detail_count0').val('');
    $('#restock_detail_price0').val('');
  </script>
@endsection
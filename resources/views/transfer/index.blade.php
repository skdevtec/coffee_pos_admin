@extends('layouts.master')

@section('title','Transfer')

@section('heading','Transfer')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Transfer -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="" method="GET">
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">&nbsp;</label>
                          <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                          @if(isset($branches))
                            &nbsp;<a class="btn btn-danger" href="{{ url('/transfer/' . $branches->branch_id) }}">Hapus Filter</a>
                          @else
                            &nbsp;<a class="btn btn-danger" href="{{ url('/transfer/s' . $stores->store_id) }}">Hapus Filter</a>
                          @endif
                      </div>
                  </div>
              </div>
          </form>
          
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th style="width: 150px;">Nominal</th>
                  <th>Keterangan</th>
                  <th>Pelaksana</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($transfers as $row)
                  @if(isset($_GET['min']))
                    @if(strtotime($_GET['min'] . " 00:00:00") <= strtotime($row->transfer_date) && strtotime($_GET['max'] . " 23:59:59") >= strtotime($row->transfer_date))
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ date('d/m/Y H:i:s', strtotime($row->transfer_date)) }}</td>
                        <td>{{ $row->transfer_invoice_number }}</td>
                        <td>IDR {{ number_format($row->transfer_total_amount, 0, ',', '.') }}</td>
                        <td>
                          @if($row->transfer_message == "")
                            -
                          @else
                            {{ $row->transfer_message }}
                          @endif
                        </td>
                        <td>{{ $row->user_name }}</td>
                        @if(!isset($branches))
                          <td>{{ $row->branch_name }}</td>
                        @endif
                        <td>
                          <a class="btn btn-primary" href="{{ url('invoice_download/transfer/' . $row->transfer_id) }}">{{ __('Unduh') }}</a>
                          <a class="btn btn-success" href="{{ route('transfer.show', 'dt' . $row->transfer_id) }}">Lihat</a>
                        </td>
                      </tr>
                    @endif
                  @else
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ date('d/m/Y H:i:s', strtotime($row->transfer_date)) }}</td>
                      <td>{{ $row->transfer_invoice_number }}</td>
                      <td>IDR {{ number_format($row->transfer_total_amount, 0, ',', '.') }}</td>
                      <td>
                        @if($row->transfer_message == "")
                          -
                        @else
                          {{ $row->transfer_message }}
                        @endif
                      </td>
                      <td>{{ $row->user_name }}</td>
                      @if(!isset($branches))
                        <td>{{ $row->branch_name }}</td>
                      @endif
                      <td>
                        <a class="btn btn-primary" href="{{ url('invoice_download/transfer/' . $row->transfer_id) }}">{{ __('Unduh') }}</a>
                        <a class="btn btn-success" href="{{ route('transfer.show', 'dt' . $row->transfer_id) }}">Lihat</a>
                      </td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th>Nominal</th>
                  <th>Keterangan</th>
                  <th>Pelaksana</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    @if(isset($branches))
      <div class="col-lg-12">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
              Tambah Transfer -
              @if(Auth::user()->user_role == "Super Admin")
                {{ $stores->store_name }}
              @endif
              {{ $branches->branch_name }}
              <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
            </h6>
          </div>
          <div class="card-body">
            <input type="text" id="branch_id" value="{{ $branches->branch_id }}" hidden>
            <div class="form-group row">
              <label for="transfer_message" class="col-sm-4 col-form-label text-md-right">
                {{ __('Keterangan') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="transfer_message" name="transfer_message">
                <p style="color: red;" id="transfer_message_feedback">
                  <strong>Keterangan wajib diisi!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="account_id_to" class="col-sm-4 col-form-label text-md-right">
                {{ __('Akun Tujuan') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-6">
                <select class="form-control" id="account_id_to" name="account_id_to">
                  <option value="">---- Pilih Akun ----</option>
                  @foreach($accounts as $row)
                    <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="account_id_to_feedback">
                  <strong>Akun tujuan wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div id="body_transfer_detail">
              <div class="form-group row">
                <label for="debet" class="col-sm-12 col-form-label text-md-center">
                  {{ __('Akun Asal') }} <abbr style="color: red;">*</abbr>
                </label>
              </div>
              <div class="form-group row">
                <div class="col-sm-1">
                </div>
                <div class="col-sm-3">
                  <select class="form-control account_id_from" data-index="0" id="account_id_from0" name="account_id_from">
                    <option value="">---- Pilih Akun ----</option>
                    @foreach($accounts as $row)
                      <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                    @endforeach
                  </select>
                  <p style="color: red;" id="account_id_from0_feedback">
                    <strong>Akun asal wajib dipilih!</strong>
                  </p>
                </div>
                <div class="col-sm-3">
                  <input type="number" class="form-control" id="account_balance0" name="account_balance" placeholder="Jumlah Tersedia" readonly>
                </div>
                <div class="col-sm-3">
                  <input type="number" class="form-control" id="transfer_detail_amount0" name="transfer_detail_amount" placeholder="Jumlah Transfer">
                  <p style="color: red;" id="transfer_detail_amount0_feedback">
                    <strong>Jumlah wajib diisi!</strong>
                  </p>
                </div>
                <div class="col-sm-1">
                  <button class="btn btn-success" id="add_transfer_detail">+</button>
                </div>
              </div>
            </div>
            <div class="form-group">
              <center>
                <button type="button" class="btn btn-primary" id="submit_transfer_detail">Simpan</button>
              </center>
            </div>
          </div>
        </div>
      </div>
    @endif
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    var table = $('#dataTable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4 ]
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4 ]
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
      ],
    }); 

    $('#transfer_message_feedback').attr('hidden', 'hidden');
    $('#account_id_to_feedback').attr('hidden', 'hidden');
    $('#account_id_from0_feedback').attr('hidden', 'hidden');
    $('#transfer_detail_amount0_feedback').attr('hidden', 'hidden');

    var array_account = new Array();
    var url = "{{ url('/get_account/branch_id') }}";
    url = url.replace('branch_id', branch_id);

    $.ajax({
      url: url,
      method: "GET", 
      data: {

      },
      success: function(result) {
        array_account = result.data;
      },
      error: function(xhr) {
        console.log(xhr);
      }
    });

    var array_transfer_detail_count = new Array();
    array_transfer_detail_count.push(0);
    var branch_id = $('#branch_id').val();
    var count = 0;
    $('#add_transfer_detail').click(function() {   
      count++;
      array_transfer_detail_count.push(count);
      $('#body_transfer_detail').append(`
        <div class="form-group row" id="list_transfer_detail` + count + `">
          <div class="col-sm-1">
          </div>
          <div class="col-sm-3">
            <select class="form-control account_id_from" data-index="` + count + `" id="account_id_from` + count + `" name="account_id_from">
              
            </select>
            <p style="color: red;" id="account_id_from` + count + `_feedback">
              <strong>Akun asal wajib dipilih!</strong>
            </p>
          </div>
          <div class="col-sm-3">
            <input type="number" class="form-control" id="account_balance` + count + `" name="account_balance" placeholder="Jumlah Tersedia" readonly>
          </div>
          <div class="col-sm-3">
            <input type="number" class="form-control" id="transfer_detail_amount` + count + `" name="transfer_detail_amount" placeholder="Jumlah Transfer">
            <p style="color: red;" id="transfer_detail_amount` + count + `_feedback">
              <strong>Jumlah wajib diisi!</strong>
            </p>
          </div>
          <div class="col-sm-1">
            <button class="btn btn-danger" onclick="remove_transfer_detail('` + count + `');">-</button>
          </div>
        </div>
      `);

      $('#account_id_from' + count + '_feedback').attr('hidden', 'hidden');
      $('#account_balance' + count + '_feedback').attr('hidden', 'hidden');
      $('#transfer_detail_amount' + count + '_feedback').attr('hidden', 'hidden');
      
      $('#account_id_from' + count).html(``);
      $('#account_id_from' + count).append(`<option value="">---- Pilih Akun ----</option>`);

      $.each(array_account, function(i,index){
        var account_name = array_account[i]['account_name'];
        if(array_account[i]['payment_type_name'] != null) {
          account_name += " " + array_account[i]['payment_type_name'];
        }
        $('#account_id_from' + count).append(`
            <option value="` + array_account[i]['account_id'] + `">` + account_name + `</option>
          `);
      });

      $('.account_id_from').change(function() {
        var account_id = $(this).val();
        var index = $(this).data('index');

        var url = "{{ route('akun.show', 'account_id') }}";
        url = url.replace('account_id', account_id);

        $.ajax({
          url: url,
          method: "GET", 
          data: {

          },
          success: function(result) {
            $('#account_balance' + index).val(result.data.account_balance);
          },
          error: function(xhr) {
            console.log(xhr);
          }
        });
      });
    });

    function remove_transfer_detail(id) {
      var array_transfer_detail_count_temp = new Array();
      for(var i = 0; i < array_transfer_detail_count.length; i++) {
        if(array_transfer_detail_count[i] != id) {
          array_transfer_detail_count_temp.push(array_transfer_detail_count[i]);
        }
      }
      array_transfer_detail_count = array_transfer_detail_count_temp;

      $('#list_transfer_detail' + id).remove();
    }

    $('.account_id_from').change(function() {
      var account_id = $(this).val();
      var index = $(this).data('index');

      var url = "{{ route('akun.show', 'account_id') }}";
      url = url.replace('account_id', account_id);

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#account_balance' + index).val(result.data.account_balance);
        },
        error: function(xhr) {
          console.log(xhr);
        }
      });
    });

    $('#submit_transfer_detail').click(function() {
      $('#transfer_message_feedback').attr('hidden', 'hidden');
      $('#account_id_to_feedback').attr('hidden', 'hidden');
      for(i = 0; i <= count; i++) {
        $('#account_id_from' + i + '_feedback').attr('hidden', 'hidden');
        $('#transfer_detail_amount' + i + '_feedback').attr('hidden', 'hidden');
      }

      var transfer_message = $('#transfer_message').val();
      var account_id_to = $('#account_id_to').val();

      var _token = $('input[name=_token]').val();

      if(transfer_message == "") {
        $('#transfer_message_feedback').removeAttr('hidden');
      } else if(account_id_to == "") {
        $('#account_id_to_feedback').removeAttr('hidden');
      } else {
        var status = true;
        var array_account_id_from = new Array();
        var array_transfer_detail_amount = new Array();
        for(i = 0; i <= count; i++) {
          var account_id_from = $('#account_id_from' + i).val();
          array_account_id_from.push(account_id_from);
          var transfer_detail_amount = $('#transfer_detail_amount' + i).val();
          array_transfer_detail_amount.push(transfer_detail_amount);

          if(account_id_from !== undefined) {
            if(account_id_from == "") {
              $('#account_id_from' + i + '_feedback').removeAttr('hidden');
              status = false;
            }
            if(transfer_detail_amount == 0 || transfer_detail_amount == "") {
              $('#transfer_detail_amount' + i + '_feedback').removeAttr('hidden');
              status = false;
            }
          }
        }

        if(status === true) {  
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('transfer.store') }}",
            method: "POST",  
            data: {
              branch_id : branch_id, 
              transfer_message : transfer_message,
              account_id_to : account_id_to, 
              account_id_from : array_account_id_from, 
              transfer_detail_amount : array_transfer_detail_amount, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            },
            error: function(xhr) {
              console.log(xhr);
            }
          });
        }
      }
    });

    $('#transfer_message').val('');
    $('#account_id_to').val('');
    $('#account_id_from0').val('');
    $('#transfer_detail_amount0').val('');
  </script>
@endsection
@extends('layouts.master')

@section('title','Transfer')

@section('heading','Transfer')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Pilih Cabang
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @if(Auth::user()->user_role == "Super Admin")
                  @foreach($stores as $row)
                    <tr>
                      <td>{{ $row->store_name }}</td>
                      <td>
                        <a class="btn btn-info" href="{{ url('/transfer/s' . $row->store_id) }}">{{ __('Semua') }}</a>
                        @if(isset($branches[$row->store_id]))
                          @foreach($branches[$row->store_id] as $row_branch)
                            <a class="btn btn-primary" href="{{ url('/transfer/' . $row_branch->branch_id) }}">{{ $row_branch->branch_name }}</a>
                          @endforeach
                        @endif
                      </td>
                    </tr>
                  @endforeach
                @elseif(Auth::user()->user_role == "Owner")
                  <tr>
                    <td># Semua</td>
                    <td>
                      <a class="btn btn-primary" href="{{ url('/transfer/s' . Auth::user()->store_id) }}">{{ __('Lihat Semua') }}</a>
                    </td>
                  </tr>
                  @foreach($branches as $row)
                    <tr>
                      <td>{{ $row->branch_name }}</td>
                      <td>
                        <a class="btn btn-primary" href="{{ url('/transfer/' . $row->branch_id) }}">{{ __('Lihat Transfer') }}</a>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
              <tfoot>
                <tr>
                  <th>Nama</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    
  </script>
@endsection
@extends('layouts.master')

@section('title','Transfer')

@section('heading','Transfer')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Detail Transfer {{ $transfers->transfer_invoice_number }}</h6>
            </div>
          </div>
        </div>
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
              <center>
                <table>
                  <tr>
                    <th>Tanggal</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ date('d F Y H:i:s', strtotime($transfers->transfer_date)) }}</td>
                  </tr>
                  <tr>
                    <th>Pelaksana</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ $transfers->user_name }}</td>
                  </tr>
                  <tr>
                    <th>Total</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>IDR {{ number_format($transfers->transfer_total_amount, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <th>Keterangan</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>
                      @if($transfers->transfer_message == "")
                        -
                      @else
                        {{ $transfers->transfer_message }}
                      @endif
                    </td>
                  </tr>
                </table>
              </center>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Asal</th>
                  <th>Tujuan</th>
                  <th>Jumlah</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($transfer_details as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $accounts[$row->account_id_from]->account_name }} {{ $accounts[$row->account_id_from]->payment_type_name }}</td>
                    <td>{{ $accounts[$row->account_id_to]->account_name }} {{ $accounts[$row->account_id_to]->payment_type_name }}</td>
                    <td>IDR {{ number_format($row->transfer_detail_amount, 0, ',', '.') }}</td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Asal</th>
                  <th>Tujuan</th>
                  <th>Jumlah</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
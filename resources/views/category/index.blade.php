@extends('layouts.master')

@section('title','Kategori')

@section('heading','Kategori')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Kategori
                @if(Auth::user()->user_role == "Owner")
                  - {{ $stores->store_name }}
                @endif
              </h6>
            </div>
            @if(Auth::user()->user_role == "Super Admin")
              <div class="col-lg-6 col-xs-6 col-md-6">
                <select class="form-control" id="filter_brand">
                  <option value="">-- Pilih Brand --</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_name }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
              </div>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  @if(Auth::user()->user_role == "Super Admin")
                    <th>Brand</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($categories as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->category_name }}</td>
                    @if(Auth::user()->user_role == "Super Admin")
                      <td>{{ $row->store_name }}</td>
                    @endif
                    <td>
                      <a class="btn btn-primary edit" data-id="{{ $row->category_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                      <a class="btn btn-danger delete" data-id="{{ $row->category_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  @if(Auth::user()->user_role == "Super Admin")
                    <th>Brand</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Kategori
            @if(Auth::user()->user_role == "Owner")
              - {{ $stores->store_name }}
            @endif
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="category_name" class="col-sm-3 col-form-label text-md-right">
              {{ __('Nama') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="category_name" name="category_name" value="">
              <p style="color: red;" id="category_name_feedback">
                <strong>Nama Kategori wajib diisi!</strong>
              </p>
            </div>
          </div>
          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="store_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Brand') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="store_id" name="store_id">
                  <option value="">---- Pilih Brand ----</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="store_id_feedback">
                  <strong>Brand wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
            <input type="text" id="store_id" value="{{ Auth::user()->store_id }}" hidden>
          @endif
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="category_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data kategori ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#category_name_feedback').attr('hidden', 'hidden');
    $('#store_id_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    var table = $('#dataTable').DataTable();
    $('#filter_brand').change(function() {
      table.column(2).search($('#filter_brand').val());
      table.draw();
    });

    $('#submit').click(function() {
      $('#category_name_feedback').attr('hidden', 'hidden');
      $('#store_id_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var category_name = $('#category_name').val();
        var store_id = $('#store_id').val();
        var _token = $('input[name=_token]').val();
        
        if(category_name == "") {
          $('#category_name_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('kategori.store') }}",
            method: "POST",  
            data: {
              category_name : category_name, 
              store_id : store_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var category_id = $('#category_id').val();
        var category_name = $('#category_name').val();
        var store_id = $('#store_id').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('kategori.update', 'category_id') }}";
        url = url.replace('category_id', category_id);

        if(category_name == "") {
          $('#category_name_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              category_id : category_id, 
              category_name : category_name, 
              store_id : store_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');

      var category_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('kategori.show', 'category_id') }}";
      url = url.replace('category_id', category_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          category_id : category_id, 
          _token : _token
        },
        success: function(result) {
          $('#category_id').val(category_id);
          $('#category_name').val(result.data.category_name);
          $('#store_id').val(result.data.store_id);
        }
      });
    });

    var category_id = "";
    $(document).delegate('.delete', 'click', function() {
      category_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('kategori.destroy', 'category_id') }}";
        url = url.replace('category_id', category_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            category_id : category_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');

      $('#category_id').val('');
      $('#category_name').val('');
      $('#store_id').val('');
      if("{{ Auth::user()->user_role }}" != "Super Admin") {
        $('#store_id').val("{{ Auth::user()->store_id }}")
      }
    });

    $('#button_add').click();
    if("{{ Auth::user()->user_role }}" != "Super Admin") {
      $('#store_id').val("{{ Auth::user()->store_id }}")
    }
  </script>
@endsection
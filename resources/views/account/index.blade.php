@extends('layouts.master')

@section('title','Akun')

@section('heading','Akun')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Akun
                @if(Auth::user()->user_role == "Admin")
                  - {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
            @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
              <div class="col-lg-6 col-xs-6 col-md-6">
                <select class="form-control" id="filter_branch">
                  <option value="">-- Pilih Cabang --</option>
                  @foreach($branches as $row)
                    @if(Auth::user()->user_role == "Super Admin")
                      <option value="{{ $row->store_name }} - {{ $row->branch_name }}">{{ $row->store_name }} - {{ $row->branch_name }}</option>
                    @else
                      <option value="{{ $row->branch_name }}">{{ $row->branch_name }}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th style="width: 150px;">Saldo</th>
                  <th>Jenis Akun</th>
                  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($accounts as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->account_name }} {{ $row->payment_type_name }}</td>
                    <td>IDR {{ number_format($row->account_balance, 0, ',', '.') }}</td>
                    <td>{{ $row->account_type_name }}</td>
                    @if(Auth::user()->user_role == "Super Admin")
                      <td>{{ $row->store_name }}
                        @if($row->branch_name == "")
                          
                        @else
                          - {{ $row->branch_name }}
                        @endif
                      </td>
                    @elseif(Auth::user()->user_role == "Owner")
                      <td>
                        @if($row->branch_name == "")
                          -
                        @else
                          {{ $row->branch_name }}
                        @endif
                      </td>
                    @endif
                    <td>
                      <a class="btn btn-primary edit" data-id="{{ $row->account_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                      <a class="btn btn-danger delete" data-id="{{ $row->account_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Saldo</th>
                  <th>Jenis Akun</th>
                  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Akun
            @if(Auth::user()->user_role == "Admin")
              - {{ $branches->branch_name }}
            @endif
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="account_name" class="col-sm-3 col-form-label text-md-right">
              {{ __('Nama') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="account_name" name="account_name" value="">
              <p style="color: red;" id="account_name_feedback">
                <strong>Nama akun wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="account_balance" class="col-sm-3 col-form-label text-md-right">
              {{ __('Saldo') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="number" class="form-control" id="account_balance" name="account_balance" value="0">
              <p style="color: red;" id="account_balance_feedback">
                <strong>Saldo akun wajib diisi!</strong>
              </p>
            </div>
          </div>

          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="store_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Brand') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="store_id" name="store_id">
                  <option value="">---- Pilih Brand ----</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="store_id_feedback">
                  <strong>Brand wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @else
            <input type="text" id="store_id" value="{{ Auth::user()->store_id }}" hidden>
          @endif

          <div class="form-group row">
            <label for="account_type_id" class="col-sm-3 col-form-label text-md-right">
              {{ __('Jenis Akun') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <select class="form-control" id="account_type_id" name="account_type_id">
                @if(Auth::user()->user_role == "Super Admin")
                  <option value="">---- Pilih Brand ----</option>
                @else
                  <option value="">---- Pilih Jenis Akun ----</option>
                  @foreach($account_types as $row)
                    <option value="{{ $row->account_type_id }}">{{ $row->account_type_name }}</option>
                  @endforeach
                @endif
              </select>
              <p style="color: red;" id="account_type_id_feedback">
                <strong>Jenis akun wajib dipilih!</strong>
              </p>
            </div>
          </div>

          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="branch_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Cabang') }}
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="branch_id" name="branch_id">
                  <option value="">---- Pilih Brand ----</option>
                </select>
                <p style="color: red;" id="branch_id_feedback">
                  <strong>Cabang wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Owner")
            <div class="form-group row">
              <label for="branch_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Cabang') }}
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="branch_id" name="branch_id">
                  <option value="">---- Pilih Cabang ----</option>
                  <option value="all">Semua</option>
                  @foreach($branches as $row)
                    <option value="{{ $row->branch_id }}">{{ $row->branch_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="branch_id_feedback">
                  <strong>Cabang wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Admin")
            <input type="text" id="branch_id" value="{{ Auth::user()->branch_id }}" hidden>
          @endif

          <div class="form-group row">
            <label for="payment_type_id" class="col-sm-3 col-form-label text-md-right">
              {{ __('Jenis Pembayaran') }}
            </label>
            <div class="col-sm-7">
              <select class="form-control" id="payment_type_id" name="payment_type_id">
                @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                  <option value="">---- Pilih Cabang ----</option>
                @else
                  <option value="">---- Pilih Jenis Pembayaran ----</option>
                  @foreach($payment_types as $row)
                    <option value="{{ $row->payment_type_id }}">{{ $row->payment_type_name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="account_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data akun ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#account_name_feedback').attr('hidden', 'hidden');
    $('#account_balance_feedback').attr('hidden', 'hidden');
    $('#account_type_id_feedback').attr('hidden', 'hidden');
    $('#store_id_feedback').attr('hidden', 'hidden');
    $('#branch_id_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    var table = $('#dataTable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4 ]
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4 ]
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
      ]
    }); 
    $('#filter_branch').change(function() {
      table.column(4).search($('#filter_branch').val());
      table.draw();
    });

    $('#store_id').change(function() {      
      var url = "{{ url('/get_branch/store_id') }}";
      url = url.replace('store_id', $('#store_id').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#branch_id').html(``);
          $('#branch_id').append(`<option value="">---- Pilih Cabang ----</option>`);
          $('#branch_id').append(`<option value="all">Semua</option>`);

          $.each(result.data, function(i,index){
            $('#branch_id').append(`
                <option value="` + result.data[i]['branch_id'] + `">` + result.data[i]['branch_name'] + `</option>
              `);
          });
        }
      });

      var url = "{{ url('/get_account_type/sstore_id') }}";
      url = url.replace('store_id', $('#store_id').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#account_type_id').html(``);
          $('#account_type_id').append(`<option value="">---- Pilih Jenis Akun ----</option>`);

          $.each(result.data, function(i,index){
            $('#account_type_id').append(`
                <option value="` + result.data[i]['account_type_id'] + `">` + result.data[i]['account_type_name'] + `</option>
              `);
          });
        }
      });
    });

    $('#branch_id').change(function() {
      var url = "";
      if($('#branch_id').val() == "all") {
        url = "{{ url('/get_payment_type/sstore_id') }}";
        url = url.replace('store_id', $('#store_id').val());
      } else {
        url = "{{ url('/get_payment_type/branch_id') }}";
        url = url.replace('branch_id', $('#branch_id').val());
      }

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#payment_type_id').html(``);
          $('#payment_type_id').append(`<option value="">---- Pilih Jenis Pembayaran ----</option>`);

          $.each(result.data, function(i,index){
            $('#payment_type_id').append(`
                <option value="` + result.data[i]['payment_type_id'] + `">` + result.data[i]['payment_type_name'] + `</option>
              `);
          });
        }
      });
    });

    $('#submit').click(function() {
      $('#account_name_feedback').attr('hidden', 'hidden');
      $('#account_balance_feedback').attr('hidden', 'hidden');
      $('#account_type_id_feedback').attr('hidden', 'hidden');
      $('#store_id_feedback').attr('hidden', 'hidden');
      $('#branch_id_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var account_name = $('#account_name').val();
        var account_balance = $('#account_balance').val();
        var store_id = $('#store_id').val();
        var branch_id = $('#branch_id').val();
        var account_type_id = $('#account_type_id').val();
        var payment_type_id = $('#payment_type_id').val();
        var _token = $('input[name=_token]').val();
        
        if(account_name == "") {
          $('#account_name_feedback').removeAttr('hidden');
        } else if(account_balance == "") {
          $('#account_balance_feedback').removeAttr('hidden');
        } else if(account_type_id == "") {
          $('#account_type_id_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        // } else if(branch_id == "") {
        //   $('#branch_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('akun.store') }}",
            method: "POST",  
            data: {
              account_name : account_name,  
              account_balance : account_balance,
              store_id : store_id, 
              branch_id : branch_id, 
              account_type_id : account_type_id,
              payment_type_id : payment_type_id,
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var account_id = $('#account_id').val();
        var account_name = $('#account_name').val();
        var store_id = $('#store_id').val();
        var account_balance = $('#account_balance').val();
        var branch_id = $('#branch_id').val();
        var account_type_id = $('#account_type_id').val();
        var payment_type_id = $('#payment_type_id').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('akun.update', 'account_id') }}";
        url = url.replace('account_id', account_id);

        if(account_name == "") {
          $('#account_name_feedback').removeAttr('hidden');
        } else if(account_balance == "") {
          $('#account_balance_feedback').removeAttr('hidden');
        } else if(account_type_id == "") {
          $('#account_type_id_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        // } else if(branch_id == "") {
        //   $('#branch_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              account_id : account_id, 
              account_name : account_name, 
              account_balance : account_balance,
              store_id : store_id, 
              branch_id : branch_id, 
              account_type_id : account_type_id,
              payment_type_id : payment_type_id,
              _token : _token
            },
            success: function(result) {
              location.reload();
            },
            error: function(xhr) {
              console.log(xhr);
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');

      var account_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('akun.show', 'account_id') }}";
      url = url.replace('account_id', account_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          account_id : account_id, 
          _token : _token
        },
        success: function(result) {
          $('#account_id').val(account_id);
          $('#account_name').val(result.data.account_name);
          $('#account_balance').val(result.data.account_balance);

          $('#store_id').val(result.data.store_id);
          $('#store_id').change();
          setTimeout(function() {
            $('#account_type_id').val(result.data.account_type_id);
            $('#branch_id').val(result.data.branch_id);
            $('#branch_id').change();
            setTimeout(function() {
              $('#payment_type_id').val(result.data.payment_type_id);
            }, 1000);
          }, 1000);
        }
      });
    });

    var account_id = "";
    $(document).delegate('.delete', 'click', function() {
      account_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('akun.destroy', 'account_id') }}";
        url = url.replace('account_id', account_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            account_id : account_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');

      $('#account_id').val('');
      $('#account_name').val('');
      $('#account_balance').val('0');
      $('#store_id').val('');
      $('#branch_id').val('');
      $('#account_type_id').val('');
      $('#payment_type_id').val('');
      if("{{ Auth::user()->user_role }}" != "Super Admin" && "{{ Auth::user()->user_role }}" != "Owner") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
        $('#branch_id').val("{{ Auth::user()->branch_id }}");
      } else if("{{ Auth::user()->user_role }}" == "Owner") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
      }
    });

    $('#button_add').click();
    if("{{ Auth::user()->user_role }}" != "Super Admin" && "{{ Auth::user()->user_role }}" != "Owner") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
      $('#branch_id').val("{{ Auth::user()->branch_id }}")
    } else if("{{ Auth::user()->user_role }}" == "Owner") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
    }
  </script>
@endsection
@extends('layouts.master')

@section('title','Beranda')

@section('heading','Beranda')

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">SK Technology</h6>
        </div>
        <div class="card-body" style="text-align: center;">
        	<h2>Selamat Datang di</h2>
        	<h3>Coffee POS!</h2>
        </div>
      </div>
    </div>
  </div>

  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
    <div class="row">
      <div class="col-lg-12">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-lg-6 col-xs-6 col-md-6">
                <h6 class="m-0 font-weight-bold text-primary">Laporan Penjualan
                </h6>
              </div>
              <div class="col-lg-6 col-xs-6 col-md-6">
                @if(Auth::user()->user_role == "Super Admin")
                  <div class="row">
                    <div class="col-lg-6 col-xs-6 col-md-6">
                      <select class="form-control" id="filter_store">
                        <option value="">-- Pilih Brand --</option>
                        @foreach($stores as $row)
                          @if(isset($_GET['sid']))
                            <option value="{{ $row->store_id }}" @if($_GET['sid'] == $row->store_id) selected @endif>{{ $row->store_name }}</option>
                          @else
                            <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-6 col-xs-6 col-md-6">
                      <select class="form-control" id="filter_branch">
                        @if(isset($_GET['bid']))
                          <option value="">-- Pilih Cabang --</option>
                          <option value="0" @if($_GET['bid'] == 0) selected @endif>Semua</option>
                          @foreach($branches as $row)
                            @if($_GET['sid'] == $row->store_id)
                              <option value="{{ $row->branch_id }}" @if($_GET['bid'] == $row->branch_id) selected @endif>{{ $row->branch_name }}</option>
                            @endif
                          @endforeach
                        @else
                          <option value="">-- Pilih Brand --</option>
                        @endif
                      </select>
                    </div>
                  </div>
                @elseif(Auth::user()->user_role == "Owner")
                  <div class="row">
                    <input type="hidden" id="filter_store" value="{{ Auth::user()->store_id }}">
                    <div class="col-lg-12 col-xs-12 col-md-12">
                      <select class="form-control" id="filter_branch">
                        <option value="">-- Pilih Cabang --</option>
                        <option value="0" @if(isset($_GET['bid'])) @if($_GET['bid'] == 0) selected @endif @else selected @endif>Semua</option>
                        @foreach($branches as $row)
                          @if(isset($_GET['bid']))
                            <option value="{{ $row->branch_id }}" @if($_GET['bid'] == $row->branch_id) selected @endif>{{ $row->branch_name }}</option>
                          @else
                            <option value="{{ $row->branch_id }}">{{ $row->branch_name }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                @endif
              </div>
            </div>
          </div>
          <div class="card-body">
            <form action="" method="GET">
                <div class="row">
                    @if(isset($_GET['sid']) && isset($_GET['bid']))
                        <input type="hidden" name="sid" value="@php echo $_GET['sid']; @endphp">
                        <input type="hidden" name="bid" value="@php echo $_GET['bid']; @endphp">
                    @endif
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">&nbsp;</label>
                            <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                            @if(isset($_GET['sid']) && isset($_GET['bid']))
                              &nbsp;<a class="btn btn-danger" href="{{ url('/home?sid=' . $_GET['sid'] . '&bid=' . $_GET['bid']) }}">Hapus Filter</a>
                            @else
                              &nbsp;<a class="btn btn-danger" href="{{ url('/home/') }}">Hapus Filter</a>
                            @endif
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Menu</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                    <th>Diskon</th>
                    <th>Grand Total</th>
                    <th>Biaya<br>Bahan Baku</th>
                    <th>Total Biaya<br>Bahan Baku</th>
                    <th>Keuntungan</th>
                    @if(isset($_GET['bid']))
                      @if($_GET['bid'] == 0)
                        <th>Cabang</th>
                      @endif
                    @endif
                  </tr>
                </thead>
                <tbody id="table_data">
                  @foreach($menus as $row)
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ $row->menu_name }}</td>
                      <td>{{ number_format($menu_reports[$row->menu_id]['quantity']) }}</td>
                      <td>IDR {{ number_format($menu_reports[$row->menu_id]['total_price_before_discount']) }}</td>
                      <td>IDR {{ number_format($menu_reports[$row->menu_id]['total_discount']) }}</td>
                      <td>IDR {{ number_format($menu_reports[$row->menu_id]['total_price_after_discount']) }}</td>
                      <td>IDR {{ number_format($menu_reports[$row->menu_id]['capital_price']) }}</td>
                      <td>IDR {{ number_format($menu_reports[$row->menu_id]['total_capital_price']) }}</td>
                      <td>IDR {{ number_format($menu_reports[$row->menu_id]['profit']) }}</td>
                      @if(isset($_GET['bid']))
                        @if($_GET['bid'] == 0)
                          <th>{{ $row->branch_name }}</th>
                        @endif
                      @endif
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Menu</th>
                    <th>Jumlah</th>
                    <th>Total</th>
                    <th>Diskon</th>
                    <th>Grand Total</th>
                    <th>Biaya<br>Bahan Baku</th>
                    <th>Total Biaya<br>Bahan Baku</th>
                    <th>Keuntungan</th>
                    @if(isset($_GET['bid']))
                      @if($_GET['bid'] == 0)
                        <th>Cabang</th>
                      @endif
                    @endif
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection

@section('script')
  <script type="text/javascript">
    $('#filter_store').change(function() {      
      var url = "{{ url('/get_branch/store_id') }}";
      url = url.replace('store_id', $('#filter_store').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#filter_branch').html(``);
          $('#filter_branch').append(`<option value="">-- Pilih Cabang --</option>`);
          $('#filter_branch').append(`<option value="all">Semua</option>`);

          $.each(result.data, function(i,index){
            $('#filter_branch').append(`
                <option value="` + result.data[i]['branch_id'] + `">` + result.data[i]['branch_name'] + `</option>
              `);
          });
        }
      });
    });

    $('#filter_branch').change(function() {
      if($('#filter_branch').val() != "") {
        var url = "{{ url('/home') }}";
        url += "?sid=" + $('#filter_store').val() + "&bid=" + $('#filter_branch').val();

        if($('#min').val() != "" && $('#max').val() != "") {
          url += "&min=" + $('#min').val() + "&max=" + $('#max').val() + "&submit=Filter";
        }

        window.location.href = url;
      }
    });
  </script>
@endsection
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Coffee POS - @yield('title')</title>

  <!-- Custom fonts for this template-->
  <link href="{{url('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{url('css/sb-admin-2.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('home')}}">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fa fa-beer"></i>
        </div>
        <!--<img src="{{ asset('images/icon/aaa.jpg') }}" width="50" height="40" title="" alt="" />-->
        <div class="sidebar-brand-text mx-3">Coffee<br>POS</div>
      </a>

      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('/home') }}">
          <i class="fas fa-fw fa-home"></i>
          <span>Beranda</span>
        </a>
      </li>

      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Pengaturan Pengguna
      </div>

      @if(Auth::user()->user_role == "Super Admin")
        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/owner') }}">
            <i class="fas fa-fw fa-user-secret"></i>
            <span>Owner</span>
          </a>
        </li>
      @endif

      @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/admin') }}">
            <i class="fas fa-fw fa-user"></i>
            <span>Admin</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/kasir') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Kasir</span>
          </a>
        </li>
      @endif

      <li class="nav-item">
        <a class="nav-link collapsed" href="{{ url('/member') }}">
          <i class="fas fa-fw fa-user-plus"></i>
          <span>Member</span>
        </a>
      </li>

      @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
          Pengaturan Brand
        </div>

        @if(Auth::user()->user_role == "Super Admin")
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ url('/brand') }}">
              <i class="fas fa-fw fa-university"></i>
              <span>Brand</span>
            </a>
          </li>
        @endif

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/cabang') }}">
            <i class="fas fa-fw fa-warehouse"></i>
            <span>Cabang</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/kategori') }}">
            <i class="fas fa-fw fa-bars"></i>
            <span>Kategori</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/satuan') }}">
            <i class="fas fa-fw fa-anchor"></i>
            <span>Satuan</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/jenis_penjualan') }}">
            <i class="fas fa-fw fa-road"></i>
            <span>Jenis Penjualan</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/konversi_poin') }}">
            <i class="fas fa-fw fa-life-ring"></i>
            <span>Konversi Poin</span>
          </a>
        </li>
      @endif

      @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
          Pengaturan Cabang
        </div>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/jenis_pembayaran') }}">
            <i class="fas fa-fw fa-credit-card"></i>
            <span>Jenis Pembayaran</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/bahan_baku') }}">
            <i class="fas fa-fw fa-cubes"></i>
            <span>Bahan Baku</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/menu') }}">
            <i class="fas fa-fw fa-book"></i>
            <span>Menu</span>
          </a>
        </li>
      @endif

      @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
          Daftar Transaksi
        </div>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/penjualan') }}">
            <i class="fas fa-fw fa-list-ul"></i>
            <span>Penjualan</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/pembelian') }}">
            <i class="fas fa-fw fa-list-ul"></i>
            <span>Pembelian</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/biaya_lain') }}">
            <i class="fas fa-fw fa-list-ul"></i>
            <span>Biaya Lain</span>
          </a>
        </li>

        <hr class="sidebar-divider">

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/transfer') }}">
            <i class="fas fa-fw fa-bookmark"></i>
            <span>Transfer</span>
          </a>
        </li>
      @endif

      @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
          Pengaturan Akuntansi
        </div>

        @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ url('/jenis_akun') }}">
              <i class="fas fa-fw fa-tags"></i>
              <span>Jenis Akun</span>
            </a>
          </li>
        @endif

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/akun') }}">
            <i class="fas fa-fw fa-tasks"></i>
            <span>Akun</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link collapsed" href="{{ url('/jurnal') }}">
            <i class="fas fa-fw fa-list-alt"></i>
            <span>Jurnal</span>
          </a>
        </li>

        @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
          <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagesItem" aria-expanded="true" aria-controls="collapsePages">
              <i class="fas fa-fw fa-book"></i>
              <span>Laporan</span>
            </a>
            <div id="collapsePagesItem" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Silahkan Pilih :</h6>
                <a class="collapse-item" href="{{ url('/laporan/neraca') }}">Neraca</a>
                <a class="collapse-item" href="{{ url('/laporan/laba_rugi') }}">Laba Rugi</a>
                <a class="collapse-item" href="{{ url('/laporan/arus_kas') }}">Arus Kas</a>
              </div>
            </div>
          </li>
          
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ url('/atur_akun') }}">
              <i class="fas fa-fw fa-wrench"></i>
              <span>Setting Akun</span>
            </a>
          </li>
        @endif
      @endif
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          @yield('heading')

          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->user_name }}</span>
                <!-- <img class="img-profile rounded-circle" > -->
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                {{-- <a class="dropdown-item" href="/account">
                  <i class="fas fa-user-plus fa-sm fa-fw mr-2 text-gray-400"></i>
                  Change password
                </a>
                <div class="dropdown-divider"></div> --}}
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          @yield('content')

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; SK Technology <script type='text/javascript'>var creditsyear = new Date();document.write(creditsyear.getFullYear());</script>. All Right Reserved.</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('logout') }}" 
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}</a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>


        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{url('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{url('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{url('js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
  {{-- <script src="{{url('vendor/chart.js/Chart.min.js')}}"></script> --}}

  <!-- Page level custom scripts -->
  {{-- <script src="{{url('js/demo/chart-area-demo.js')}}"></script> --}}
  {{-- <script src="{{url('js/demo/chart-pie-demo.js')}}"></script> --}}

  <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.flash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="{{url('js/demo/datatables-demo.js')}}"></script>

  @yield('script')

</body>

</html>

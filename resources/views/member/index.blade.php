@extends('layouts.master')

@section('title','Member')

@section('heading','Member')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Daftar Member</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Kontak</th>
                  <th>Poin</th>
                  @if(Auth::user()->user_role == "Super Admin")
                    <th>Brand</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($users as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->user_name }}</td>
                    <td>
                      <a href="tel:{{ $row->user_phone }}" target="_blank"><i class="fa fa-phone"></i> {{ $row->user_phone }}</a><br>
                      <a href="https://wa.me/{{ $row->user_phone }}" target="_blank"><i class="fa fa-whatsapp"></i> {{ $row->user_phone }}</a><br>
                      <a href="mailto:{{ $row->email }}"><i class="fa fa-envelope"></i> {{ $row->email }}</a>
                    </td>
                    <td>{{ number_format($row->user_point) }} <a href="javascript:void" class="point_detail" data-id="{{ $row->user_id }}"><i class="fa fa-history"></i></a></td>
                    @if(Auth::user()->user_role == "Super Admin")
                      <td>{{ $row->store_name }}</td>
                    @endif
                    <td>
                      @if(Auth::user()->user_role == "Cashier")
                        -
                      @else
                        <a class="btn btn-primary edit" data-id="{{ $row->user_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                        <a class="btn btn-danger delete" data-id="{{ $row->user_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Kontak</th>
                  <th>Poin</th>
                  @if(Auth::user()->user_role == "Super Admin")
                    <th>Brand</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Member
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="user_name" class="col-sm-3 col-form-label text-md-right">
              {{ __('Nama') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="user_name" name="user_name" value="">
              <p style="color: red;" id="user_name_feedback">
                <strong>Nama member wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="user_phone" class="col-sm-3 col-form-label text-md-right">
              {{ __('Telepon') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control col-4" id="user_phone_start" name="user_phone_start" value="+62" readonly style="float: left;">
              <input type="text" class="form-control col-8" id="user_phone" name="user_phone" value="" style="float: left;">
              <p style="color: red;" id="user_phone_feedback">
                <strong>Telepon member wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="email" class="col-sm-3 col-form-label text-md-right">
              {{ __('Email') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="email" class="form-control" id="email" name="email" value="">
              <p style="color: red;" id="email_feedback">
                <strong>Email member wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="password" class="col-sm-3 col-form-label text-md-right">
              {{ __('Password') }} <abbr style="color: red;" id="password_required">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="password" class="form-control" id="password" name="password" value="">
              <p style="color: red;" id="password_feedback">
                <strong>Password member wajib diisi!</strong>
              </p>
            </div>
          </div>
          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="store_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Brand') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="store_id" name="store_id">
                  <option value="">---- Pilih Brand ----</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="store_id_feedback">
                  <strong>Brand wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @else
            <input type="hidden" id="store_id" name="store_id" value="{{ Auth::user()->store_id }}">
          @endif
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="user_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data member ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_point_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Riwayat Poin</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable_point_detail" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Poin</th>
                  <th>Status</th>
                  <th>Transaksi</th>
                </tr>
              </thead>
              <tbody id="table_data_point_detail">
                
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Poin</th>
                  <th>Status</th>
                  <th>Transaksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="button" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#user_name_feedback').attr('hidden', 'hidden');
    $('#user_phone_feedback').attr('hidden', 'hidden');
    $('#email_feedback').attr('hidden', 'hidden');
    $('#password_feedback').attr('hidden', 'hidden');
    $('#store_id_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    $('#submit').click(function() {
      $('#user_name_feedback').attr('hidden', 'hidden');
      $('#user_phone_feedback').attr('hidden', 'hidden');
      $('#email_feedback').attr('hidden', 'hidden');
      $('#password_feedback').attr('hidden', 'hidden');
      $('#store_id_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var user_name = $('#user_name').val();
        var user_phone = $('#user_phone').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var store_id = $('#store_id').val();
        var _token = $('input[name=_token]').val();
        
        if(user_name == "") {
          $('#user_name_feedback').removeAttr('hidden');
        } else if(user_phone == "") {
          $('#user_phone_feedback').removeAttr('hidden');
        } else if(email == "") {
          $('#email_feedback').removeAttr('hidden');
        } else if(password == "") {
          $('#password_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('member.store') }}",
            method: "POST",
            data: {
              user_name : user_name, 
              user_phone : $('#user_phone_start').val().toString() + user_phone.toString(), 
              email : email, 
              password : password, 
              store_id : store_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            },
            error: function(xhr) {
              console.log(xhr);
            }
          });
        }
      } else if(type == "edit") {
        var user_id = $('#user_id').val();
        var user_name = $('#user_name').val();
        var user_phone = $('#user_phone').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var store_id = $('#store_id').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('member.update', 'user_id') }}";
        url = url.replace('user_id', user_id);

        if(user_name == "") {
          $('#user_name_feedback').removeAttr('hidden');
        } else if(user_phone == "") {
          $('#user_phone_feedback').removeAttr('hidden');
        } else if(email == "") {
          $('#email_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              user_id : user_id, 
              user_name : user_name, 
              user_phone : $('#user_phone_start').val().toString() + user_phone.toString(), 
              email : email, 
              password : password, 
              store_id : store_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            },
            error: function(xhr) {
              console.log(xhr);
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');
      $('#password_required').attr('hidden', 'hidden');

      var user_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('member.show', 'user_id') }}";
      url = url.replace('user_id', user_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          user_id : user_id, 
          _token : _token
        },
        success: function(result) {
          $('#user_id').val(user_id);
          $('#user_name').val(result.data.user_name);
          $('#user_phone').val(result.data.user_phone.substring(3));
          $('#email').val(result.data.email);
          $('#store_id').val(result.data.store_id);
        }
      });
    });

    var user_id = "";
    $(document).delegate('.delete', 'click', function() {
      user_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('member.destroy', 'user_id') }}";
        url = url.replace('user_id', user_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            user_id : user_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $(document).delegate('.point_detail', 'click', function() {
      user_id = $(this).data('id');
        
      var url = "{{ route('riwayat_poin.show', 'user_id') }}";
      url = url.replace('user_id', user_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {

        },
        success: function(result) {
          $('#table_data_point_detail').html('');
          $.each(result['data'], function(index, value) {
            var history_point_status = "";
            var color = "";
            if(value['history_point_status'] == "In") {
              history_point_status = "Masuk";
              color = "green";
            } else if(value['history_point_status'] == "Out") {
              history_point_status = "Keluar";
              color = "red";
            }
            $('#table_data_point_detail').append(`
                <tr>
                  <td>` + (index + 1) + `</td>
                  <td>` + value['history_point_value'] + `</td>
                  <td style="color: ` + color + `;">` + history_point_status + `</td>
                  <td>` + value['transaction_invoice_number'] + `</td>
                </tr>
              `);
          });

          if(result['data'].length == 0) {
            $('#table_data_point_detail').append(`
                <tr>
                  <td colspan="4">Tidak ada data riwayat poin pada member ini.</td>
                </tr>
              `);
          }

          $('#modal_point_detail').modal("show");
        }
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');
      $('#password_required').removeAttr('hidden');

      $('#user_id').val('');
      $('#user_name').val('');
      $('#user_phone').val('');
      $('#email').val('');
      $('#password').val('');
      $('#store_id').val('');
      if("{{ Auth::user()->user_role }}" != "Super Admin") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
      }
    });

    $('#button_add').click();
    if("{{ Auth::user()->user_role }}" != "Super Admin") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
    }
  </script>
@endsection
@extends('layouts.master')

@section('title','Laporan Neraca')

@section('heading','Laporan Neraca')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Laporan Neraca -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                
              </thead>
              <tbody>
                <tr>
                  <th colspan="3">AKTIVA</th>
                </tr>
                @foreach($account_aktiva as $row_account_aktiva)
                  <tr>
                    <th>{{ $row_account_aktiva->account_type_name }}</th>
                    <th style="width: 150px;">Debet</th>
                    <th style="width: 150px;">Credit</th>
                  </tr>
                  <input type="hidden" value="{{ $total = 0 }}">
                  @foreach($aktiva[$row_account_aktiva->account_type_id] as $row_aktiva)
                    <tr>
                      <td>{{ $row_aktiva->account_name }} {{ $row_aktiva->payment_type_name }}</td>
                      <td>IDR {{ number_format($row_aktiva->account_balance, 0, ',', '.') }}</td>
                      <td></td>
                    </tr>
                    <input type="hidden" value="{{ $total += $row_aktiva->account_balance }}">
                  @endforeach
                  <tr>
                    <th>Total {{ $row_account_aktiva->account_type_name }}</th>
                    <th colspan="2">IDR {{ number_format($total, 0, ',', '.') }}</th>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                @endforeach
                <tr>
                  <th colspan="3">PASIVA</th>
                </tr>
                @foreach($account_pasiva as $row_account_pasiva)
                  <tr>
                    <th>{{ $row_account_pasiva->account_type_name }}</th>
                    <th>Debet</th>
                    <th>Credit</th>
                  </tr>
                  <input type="hidden" value="{{ $total = 0 }}">
                  @foreach($pasiva[$row_account_pasiva->account_type_id] as $row_pasiva)
                    <tr>
                      <td>{{ $row_pasiva->account_name }}</td>
                      <td></td>
                      <td>IDR {{ number_format($row_pasiva->account_balance, 0, ',', '.') }}</td>
                    </tr>
                    <input type="hidden" value="{{ $total += $row_pasiva->account_balance }}">
                  @endforeach
                  <tr>
                    <th>Total {{ $row_account_pasiva->account_type_name }}</th>
                    <th colspan="2">IDR {{ number_format($total, 0, ',', '.') }}</th>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    
  </script>
@endsection
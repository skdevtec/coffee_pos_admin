@extends('layouts.master')

@section('title','Laporan Laba Rugi')

@section('heading','Laporan Laba Rugi')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Laporan Laba Rugi -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="" method="GET">
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">&nbsp;</label>
                          <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                          @if(isset($branches))
                            &nbsp;<a class="btn btn-danger" href="{{ url('/laporan/laba_rugi/' . $stores->store_id . '/' . $branches->branch_id) }}">Hapus Filter</a>
                          @else
                            &nbsp;<a class="btn btn-danger" href="{{ url('/laporan/laba_rugi/' . $stores->store_id . '/0') }}">Hapus Filter</a>
                          @endif
                      </div>
                  </div>
              </div>
          </form>

          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th colspan="3">PENDAPATAN</th>
                </tr>
                <tr>
                  <th>Akun</th>
                  <th>Debet</th>
                  <th>Credit</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @php
                  $total_credit = 0;
                @endphp
                @foreach($account_pendapatan_report as $account => $row)
                  <tr>
                    <td>{{ $account }}</td>
                    <td></td>
                    <td>
                      @php
                        $credit = $row['credit'] - $row['debet'];
                        $total_credit += $credit;
                      @endphp
                      @if($credit < 0)
                        <span style="color: red;">(IDR {{ number_format($credit * -1) }})</span>
                      @else
                        IDR {{ number_format($credit) }}
                      @endif
                    </td>
                  </tr>
                @endforeach
                <tr>
                  <th>Total</th>
                  <th></th>
                  <th>IDR {{ number_format($total_credit) }}</th>
                </tr>
              </tbody>
              <thead>
                <tr>
                  <th colspan="3">&nbsp;</th>
                </tr>
                <tr>
                  <th colspan="3">BEBAN</th>
                </tr>
                <tr>
                  <th>Akun</th>
                  <th>Debet</th>
                  <th>Credit</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @php
                  $total_debet = 0;
                @endphp
                @foreach($account_beban_report as $account => $row)
                  <tr>
                    <td>{{ $account }}</td>
                    <td>
                      @php
                        $debet = $row['debet'] - $row['credit'];
                        $total_debet += $debet;
                      @endphp
                      @if($debet < 0)
                        <span style="color: red;">(IDR {{ number_format($debet * -1) }})</span>
                      @else
                        IDR {{ number_format($debet) }}
                      @endif
                    </td>
                    <td></td>
                  </tr>
                @endforeach
                <tr>
                  <th>Total</th>
                  <th>IDR {{ number_format($total_debet) }}</th>
                  <th></th>
                </tr>
              </tbody>
              <thead>
                <tr>
                  <th colspan="3">&nbsp;</th>
                </tr>
                <tr>
                  <th>PENDAPATAN - BEBAN</th>
                  <th colspan="2" style="text-align: center;">
                    @php
                      $profit = $total_credit - $total_debet;
                    @endphp
                    @if($profit < 0)
                      <span style="color: red;">(IDR {{ number_format($profit * -1) }})</span>
                    @elseif($profit == 0)
                      <span>IDR {{ number_format($profit) }}</span>
                    @else 
                      <span style="color: green;">IDR {{ number_format($profit) }}</span>
                    @endif
                  </th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    
  </script>
@endsection
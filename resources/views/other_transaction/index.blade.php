@extends('layouts.master')

@section('title','Biaya Lain')

@section('heading','Biaya Lain')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Biaya Lain -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="" method="GET">
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">&nbsp;</label>
                          <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                          @if(isset($branches))
                            &nbsp;<a class="btn btn-danger" href="{{ url('/biaya_lain/' . $branches->branch_id) }}">Hapus Filter</a>
                          @else
                            &nbsp;<a class="btn btn-danger" href="{{ url('/biaya_lain/s' . $stores->store_id) }}">Hapus Filter</a>
                          @endif
                      </div>
                  </div>
              </div>
          </form>

          <div class="table-responsive">
            @php
              $other_transaction_count = 0;
              $other_transaction_amount_total = 0;
            @endphp
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th>Keterangan</th>
                  <th style="width: 150px;">Total</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($other_transactions as $row)
                  @if(isset($_GET['min']))
                    @if(strtotime($_GET['min']) <= strtotime($row->other_transaction_date) && strtotime($_GET['max']) >= strtotime($row->other_transaction_date))
                      @php
                        $other_transaction_count++;
                        $other_transaction_amount_total += $row->other_transaction_amount;
                      @endphp
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ date('d/m/Y', strtotime($row->other_transaction_date)) }}</td>
                        <td>{{ $row->other_transaction_invoice_number }}</td>
                        <td>{{ $row->other_transaction_description }}</td>
                        <td>IDR {{ number_format($row->other_transaction_amount, 0, ',', '.') }}</td>
                        @if(!isset($branches))
                          <td>{{ $row->branch_name }}</td>
                        @endif
                        <td>
                        <a class="btn btn-info" href="{{ url('invoice_download/biaya_lain/' . $row->other_transaction_id) }}">{{ __('Unduh') }}</a>
                          @if(isset($branches))
                            <a class="btn btn-primary edit" data-id="{{ $row->other_transaction_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                          @endif
                          <a class="btn btn-danger delete" data-id="{{ $row->other_transaction_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                        </td>
                      </tr>
                    @endif
                  @else
                    @php
                      $other_transaction_count++;
                      $other_transaction_amount_total += $row->other_transaction_amount;
                    @endphp
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ date('d/m/Y', strtotime($row->other_transaction_date)) }}</td>
                      <td>{{ $row->other_transaction_invoice_number }}</td>
                      <td>{{ $row->other_transaction_description }}</td>
                      <td>IDR {{ number_format($row->other_transaction_amount, 0, ',', '.') }}</td>
                      @if(!isset($branches))
                        <td>{{ $row->branch_name }}</td>
                      @endif
                      <td>
                        <a class="btn btn-info" href="{{ url('invoice_download/biaya_lain/' . $row->other_transaction_id) }}">{{ __('Unduh') }}</a>
                        @if(isset($branches))
                          <a class="btn btn-primary edit" data-id="{{ $row->other_transaction_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                        @endif
                        <a class="btn btn-danger delete" data-id="{{ $row->other_transaction_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                      </td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th>Keterangan</th>
                  <th>Total</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            Rekap Biaya Lain
          </h6>
        </div>
        <div class="card-body">
          <div class="block">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-xs-4">
                <table>
                  <tr>
                    <td>Jumlah Biaya Lain</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ number_format($other_transaction_count, 0, ',', '.') }}</td>
                  </tr>
                </table>
              </div>
              <div class="col-lg-4 col-md-4 col-xs-4">
                <table>
                  <tr>
                    <td>Total Biaya Lain</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ number_format($other_transaction_amount_total, 0, ',', '.') }}</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @if(isset($branches))
      <div class="col-lg-12">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
              <span id="header_action">Tambah</span> Biaya Lain -
              @if(Auth::user()->user_role == "Super Admin")
                {{ $stores->store_name }}
              @endif
              {{ $branches->branch_name }}
              <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
              <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
            </h6>
          </div>
          <div class="card-body">
            <div class="form-group row">
              <label for="other_transaction_date" class="col-sm-3 col-form-label text-md-right">
                {{ __('Tanggal') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <input type="date" class="form-control" id="other_transaction_date" name="other_transaction_date" value="">
                <p style="color: red;" id="other_transaction_date_feedback">
                  <strong>Tanggal wajib diisi!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="other_transaction_description" class="col-sm-3 col-form-label text-md-right">
                {{ __('Keterangan') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="other_transaction_description" name="other_transaction_description" value="">
                <p style="color: red;" id="other_transaction_description_feedback">
                  <strong>Keterangan wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="other_transaction_amount" class="col-sm-3 col-form-label text-md-right">
                {{ __('Jumlah') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <input type="number" class="form-control" id="other_transaction_amount" name="other_transaction_amount" value="">
                <p style="color: red;" id="other_transaction_amount_feedback">
                  <strong>Jumlah wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <input type="text" id="branch_id" value="{{ $branches->branch_id }}" hidden>
            <div class="form-group row">
              <label for="account_id_debet" class="col-sm-3 col-form-label text-md-right">
                {{ __('Debet') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="account_id_debet" name="account_id_debet">
                  <option value="">---- Pilih Akun ----</option>
                  @foreach($accounts as $row)
                    <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="account_id_debet_feedback">
                  <strong>Akun debet wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label for="account_id_credit" class="col-sm-3 col-form-label text-md-right">
                {{ __('Kredit') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="account_id_credit" name="account_id_credit">
                  <option value="">---- Pilih Akun ----</option>
                  @foreach($accounts as $row)
                    <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="account_id_credit_feedback">
                  <strong>Akun kredit wajib dipilih!</strong>
                </p>
              </div>
            </div>
            <div class="form-group">
              {{ csrf_field() }}
              <input type="text" id="other_transaction_id" value="" hidden>
              <input type="text" id="type" value="create" hidden>
              <center>
                <button type="button" class="btn btn-primary" id="submit">Simpan</button>
              </center>
            </div>
          </div>
        </div>
      </div>
    @endif
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data biaya lain ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#dataTable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3 ]
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
      ]
    }); 

    $('#other_transaction_date_feedback').attr('hidden', 'hidden');
    $('#other_transaction_description_feedback').attr('hidden', 'hidden');
    $('#other_transaction_amount_feedback').attr('hidden', 'hidden');
    $('#branch_id_feedback').attr('hidden', 'hidden');
    $('#account_id_debet_feedback').attr('hidden', 'hidden');
    $('#account_id_credit_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    $('#submit').click(function() {
      $('#other_transaction_date_feedback').attr('hidden', 'hidden');
      $('#other_transaction_description_feedback').attr('hidden', 'hidden');
      $('#other_transaction_amount_feedback').attr('hidden', 'hidden');
      $('#account_id_debet_feedback').attr('hidden', 'hidden');
      $('#account_id_credit_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var other_transaction_date = $('#other_transaction_date').val();
        var other_transaction_description = $('#other_transaction_description').val();
        var other_transaction_amount = $('#other_transaction_amount').val();
        var branch_id = $('#branch_id').val();
        var account_id_debet = $('#account_id_debet').val();
        var account_id_credit = $('#account_id_credit').val();
        var _token = $('input[name=_token]').val();
        
        if(other_transaction_date == "") {
          $('#other_transaction_date_feedback').removeAttr('hidden');
        } else if(other_transaction_description == "") {
          $('#other_transaction_description_feedback').removeAttr('hidden');
        } else if(other_transaction_amount == "") {
          $('#other_transaction_amount_feedback').removeAttr('hidden');
        } else if(account_id_debet == "") {
          $('#account_id_debet_feedback').removeAttr('hidden');
        } else if(account_id_credit == "") {
          $('#account_id_credit_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('biaya_lain.store') }}",
            method: "POST",  
            data: {
              other_transaction_date : other_transaction_date,  
              other_transaction_description : other_transaction_description,
              other_transaction_amount : other_transaction_amount,
              branch_id : branch_id, 
              account_id_debet : account_id_debet, 
              account_id_credit : account_id_credit, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var other_transaction_id = $('#other_transaction_id').val();
        var other_transaction_date = $('#other_transaction_date').val();
        var other_transaction_description = $('#other_transaction_description').val();
        var other_transaction_amount = $('#other_transaction_amount').val();
        var branch_id = $('#branch_id').val();
        var account_id_debet = $('#account_id_debet').val();
        var account_id_credit = $('#account_id_credit').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('biaya_lain.update', 'other_transaction_id') }}";
        url = url.replace('other_transaction_id', other_transaction_id);

        if(other_transaction_date == "") {
          $('#other_transaction_date_feedback').removeAttr('hidden');
        } else if(other_transaction_description == "") {
          $('#other_transaction_description_feedback').removeAttr('hidden');
        } else if(other_transaction_amount == "") {
          $('#other_transaction_amount_feedback').removeAttr('hidden');
        } else if(account_id_debet == "") {
          $('#account_id_debet_feedback').removeAttr('hidden');
        } else if(account_id_credit == "") {
          $('#account_id_credit_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              other_transaction_id : other_transaction_id, 
              other_transaction_date : other_transaction_date, 
              other_transaction_description : other_transaction_description,
              other_transaction_amount : other_transaction_amount,
              branch_id : branch_id, 
              account_id_debet : account_id_debet, 
              account_id_credit : account_id_credit, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');

      var other_transaction_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('biaya_lain.show', 'editother_transaction_id') }}";
      url = url.replace('other_transaction_id', other_transaction_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          other_transaction_id : other_transaction_id, 
          _token : _token
        },
        success: function(result) {
          $('#other_transaction_id').val(other_transaction_id);
          $('#other_transaction_date').val(result.data.other_transaction_date.substring(0, 10));
          $('#other_transaction_description').val(result.data.other_transaction_description);
          $('#other_transaction_amount').val(result.data.other_transaction_amount);
          $('#account_id_debet').val(result.journal.account_id_debet);
          $('#account_id_credit').val(result.journal.account_id_credit);
        }
      });
    });

    var other_transaction_id = "";
    $(document).delegate('.delete', 'click', function() {
      other_transaction_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('biaya_lain.destroy', 'other_transaction_id') }}";
        url = url.replace('other_transaction_id', other_transaction_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            other_transaction_id : other_transaction_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');

      $('#other_transaction_id').val('');
      $('#other_transaction_date').val('');
      $('#other_transaction_description').val('');
      $('#other_transaction_amount').val('');
      $('#account_id_debet').val('');
      $('#account_id_credit').val('');
    });

    $('#button_add').click();
  </script>
@endsection
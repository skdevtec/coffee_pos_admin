@extends('layouts.master')

@section('title','Setting Akun')

@section('heading','Setting Akun')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Setting Akun - 
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                {{ $branches->branch_name }}
              </h6>
            </div>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <input type="hidden" id="branch_id" value="{{ $branches->branch_id }}">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Tipe</th>
                  <th>Akun</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                <tr>
                  <td>Debet Potongan Penjualan</td>
                  <td>
                    <input type="hidden" id="account-setting-type-debet-potongan-penjualan" value="Debet Potongan Penjualan">
                    <select id="account-id-debet-potongan-penjualan" class="form-control">
                      <option value="">-- Pilih Akun --</option>
                      @foreach($accounts as $row)
                        @if(isset($account_settings['Debet Potongan Penjualan']))
                          @if($account_settings['Debet Potongan Penjualan']->account_id == $row->account_id)
                            <option value="{{ $row->account_id }}" selected>{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @else
                            <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @endif
                        @else
                          <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                        @endif
                      @endforeach
                    </select>
                    <p style="color: red;" id="account_id_debet_potongan_penjualan_feedback">
                      <strong>Akun wajib dipilih!</strong>
                    </p>
                  </td>
                  <td>
                    <button class="btn btn-success btn-save" data-account-id="account-id-debet-potongan-penjualan" data-account-setting-type="account-setting-type-debet-potongan-penjualan" data-feedback="account_id_debet_potongan_penjualan_feedback">Simpan</button>
                  </td>
                </tr>
                <tr>
                  <td>Debet Diskon Penjualan</td>
                  <td>
                    <input type="hidden" id="account-setting-type-debet-diskon-penjualan" value="Debet Diskon Penjualan">
                    <select id="account-id-debet-diskon-penjualan" class="form-control">
                      <option value="">-- Pilih Akun --</option>
                      @foreach($accounts as $row)
                        @if(isset($account_settings['Debet Diskon Penjualan']))
                          @if($account_settings['Debet Diskon Penjualan']->account_id == $row->account_id)
                            <option value="{{ $row->account_id }}" selected>{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @else
                            <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @endif
                        @else
                          <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                        @endif
                      @endforeach
                    </select>
                    <p style="color: red;" id="account_id_debet_diskon_penjualan_feedback">
                      <strong>Akun wajib dipilih!</strong>
                    </p>
                  </td>
                  <td>
                    <button class="btn btn-success btn-save" data-account-id="account-id-debet-diskon-penjualan" data-account-setting-type="account-setting-type-debet-diskon-penjualan" data-feedback="account_id_debet_diskon_penjualan_feedback">Simpan</button>
                  </td>
                </tr>
                <tr>
                  <td>Kredit Nota Penjualan</td>
                  <td>
                    <input type="hidden" id="account-setting-type-kredit-nota-penjualan" value="Kredit Nota Penjualan">
                    <select id="account-id-kredit-nota-penjualan" class="form-control">
                      <option value="">-- Pilih Akun --</option>
                      @foreach($accounts as $row)
                        @if(isset($account_settings['Kredit Nota Penjualan']))
                          @if($account_settings['Kredit Nota Penjualan']->account_id == $row->account_id)
                            <option value="{{ $row->account_id }}" selected>{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @else
                            <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @endif
                        @else
                          <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                        @endif
                      @endforeach
                    </select>
                    <p style="color: red;" id="account_id_kredit_nota_penjualan_feedback">
                      <strong>Akun wajib dipilih!</strong>
                    </p>
                  </td>
                  <td>
                    <button class="btn btn-success btn-save" data-account-id="account-id-kredit-nota-penjualan" data-account-setting-type="account-setting-type-kredit-nota-penjualan" data-feedback="account_id_kredit_nota_penjualan_feedback">Simpan</button>
                  </td>
                </tr>
                <tr>
                  <td>Debet Bahan Baku Penjualan</td>
                  <td>
                    <input type="hidden" id="account-setting-type-debet-bahan-baku-penjualan" value="Debet Bahan Baku Penjualan">
                    <select id="account-id-debet-bahan-baku-penjualan" class="form-control">
                      <option value="">-- Pilih Akun --</option>
                      @foreach($accounts as $row)
                        @if(isset($account_settings['Debet Bahan Baku Penjualan']))
                          @if($account_settings['Debet Bahan Baku Penjualan']->account_id == $row->account_id)
                            <option value="{{ $row->account_id }}" selected>{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @else
                            <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @endif
                        @else
                          <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                        @endif
                      @endforeach
                    </select>
                    <p style="color: red;" id="account_id_debet_bahan_baku_penjualan_feedback">
                      <strong>Akun wajib dipilih!</strong>
                    </p>
                  </td>
                  <td>
                    <button class="btn btn-success btn-save" data-account-id="account-id-debet-bahan-baku-penjualan" data-account-setting-type="account-setting-type-debet-bahan-baku-penjualan" data-feedback="account_id_debet_bahan_baku_penjualan_feedback">Simpan</button>
                  </td>
                </tr>
                <tr>
                  <td>Kredit Bahan Baku Penjualan</td>
                  <td>
                    <input type="hidden" id="account-setting-type-kredit-bahan-baku-penjualan" value="Kredit Bahan Baku Penjualan">
                    <select id="account-id-kredit-bahan-baku-penjualan" class="form-control">
                      <option value="">-- Pilih Akun --</option>
                      @foreach($accounts as $row)
                        @if(isset($account_settings['Kredit Bahan Baku Penjualan']))
                          @if($account_settings['Kredit Bahan Baku Penjualan']->account_id == $row->account_id)
                            <option value="{{ $row->account_id }}" selected>{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @else
                            <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @endif
                        @else
                          <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                        @endif
                      @endforeach
                    </select>
                    <p style="color: red;" id="account_id_kredit_bahan_baku_penjualan_feedback">
                      <strong>Akun wajib dipilih!</strong>
                    </p>
                  </td>
                  <td>
                    <button class="btn btn-success btn-save" data-account-id="account-id-kredit-bahan-baku-penjualan" data-account-setting-type="account-setting-type-kredit-bahan-baku-penjualan" data-feedback="account_id_kredit_bahan_baku_penjualan_feedback">Simpan</button>
                  </td>
                </tr>
                <tr>
                  <td>Debet Kredit Hutang Dagang</td>
                  <td>
                    <input type="hidden" id="account-setting-type-debet-kredit-hutang-dagang" value="Debet Kredit Hutang Dagang">
                    <select id="account-id-debet-kredit-hutang-dagang" class="form-control">
                      <option value="">-- Pilih Akun --</option>
                      @foreach($accounts as $row)
                        @if(isset($account_settings['Debet Kredit Hutang Dagang']))
                          @if($account_settings['Debet Kredit Hutang Dagang']->account_id == $row->account_id)
                            <option value="{{ $row->account_id }}" selected>{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @else
                            <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                          @endif
                        @else
                          <option value="{{ $row->account_id }}">{{ $row->account_name }} {{ $row->payment_type_name }}</option>
                        @endif
                      @endforeach
                    </select>
                    <p style="color: red;" id="account_id_debet_kredit_hutang_dagang_feedback">
                      <strong>Akun wajib dipilih!</strong>
                    </p>
                  </td>
                  <td>
                    <button class="btn btn-success btn-save" data-account-id="account-id-debet-kredit-hutang-dagang" data-account-setting-type="account-setting-type-debet-kredit-hutang-dagang" data-feedback="account_id_debet_kredit_hutang_dagang_feedback">Simpan</button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th>Tipe</th>
                  <th>Akun</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{ csrf_field() }}
@endsection

@section('script')
  <script type="text/javascript">
    $('#account_id_debet_potongan_penjualan_feedback').attr('hidden', 'hidden');
    $('#account_id_debet_diskon_penjualan_feedback').attr('hidden', 'hidden');
    $('#account_id_kredit_nota_penjualan_feedback').attr('hidden', 'hidden');
    $('#account_id_debet_bahan_baku_penjualan_feedback').attr('hidden', 'hidden');
    $('#account_id_kredit_bahan_baku_penjualan_feedback').attr('hidden', 'hidden');
    $('#account_id_debet_kredit_hutang_dagang_feedback').attr('hidden', 'hidden');

    $('.btn-save').click(function() {
      var field_account_id = $(this).data('account-id');
      var field_account_setting_type = $(this).data('account-setting-type');
      var feedback = $(this).data('feedback');

      var account_id = $('#' + field_account_id).val();
      var account_setting_type = $('#' + field_account_setting_type).val();
      var branch_id = $('#branch_id').val();
      var _token = $('input[name=_token]').val();
      
      if(account_id == "") {
        $('#' + feedback).removeAttr('hidden');
      } else {
        $('#loading').removeAttr('hidden');
        $.ajax({
          url: "{{ route('atur_akun.store') }}",
          method: "POST",  
          data: {
            account_id : account_id,
            account_setting_type : account_setting_type,
            branch_id : branch_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      }
    });
  </script>
@endsection
@extends('layouts.master')

@section('title','Jurnal')

@section('heading','Jurnal')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Jurnal -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <form action="" method="GET">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label">Akun</label>
                            <div class="col-sm-12">
                                @php
                                  $acc = [];
                                  if(isset($_GET['acc'])) {
                                    $acc_temp = $_GET['acc'];
                                    foreach($acc_temp as $row) {
                                      $acc[$row] = "";
                                    }
                                  }
                                @endphp
                                <input type="checkbox" id="check_all" name="acc[]" @if(isset($acc['Pilih Semua']) || count($acc) == 0) checked @endif value="Pilih Semua"> <b>Pilih Semua</b><br> 
                                @foreach($accounts_filter as $index => $value)
                                  <input type="checkbox" class="acc" id="acc" name="acc[]" @if(isset($acc[$index]) || count($acc) == 0) checked @endif value="{{ $index }}"> {{ $index }}<br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                        @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                          @if(isset($branches))
                            <a href="{{ url('jurnal/' . $branches->branch_id) }}" class="btn btn-danger">Hapus Filter</a>
                          @else
                            <a href="{{ url('jurnal/s' . $stores->store_id) }}" class="btn btn-danger">Hapus Filter</a>
                          @endif
                        @else
                          <a href="{{ url('jurnal') }}" class="btn btn-danger">Hapus Filter</a>
                        @endif
                    </div>
                </div>
              </form>
              <br>
            </div>

            <div class="col-md-8">
              <div class="table-responsive">
                @php
                  $total_debet = 0;
                  $total_credit = 0;
                @endphp
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Akun</th>
                      <th style="width: 150px;">Debet</th>
                      <th style="width: 150px;">Kredit</th>
                      @if(!isset($branches))
                        <th>Cabang</th>
                      @endif
                      <th>Kode Transaksi</th>
                    </tr>
                  </thead>
                  <tbody id="table_data">
                    @php
                      $no = 1;
                    @endphp
                    @foreach($journals as $row)
                      @if(isset($_GET['min']))
                        @php
                          $index = "";
                          if($accounts_library[$row->account_id]->payment_type_id == "") {
                            $index = $accounts_library[$row->account_id]->account_name;
                          } else {
                            $index = $accounts_library[$row->account_id]->account_name . ' ' . $accounts_library[$row->account_id]->payment_type_name;
                          }
                        @endphp
                        @if(strtotime($_GET['min'] . " 00:00:00") <= strtotime($row->journal_date) && strtotime($_GET['max'] . " 23:59:59") >= strtotime($row->journal_date) && isset($acc[$index]))
                          @php
                            if($row->journal_debet != "" && $row->journal_debet != 0) {
                              $total_debet += $row->journal_debet;
                            } else if($row->journal_credit != "" && $row->journal_credit != 0) {
                              $total_credit += $row->journal_credit;
                            }
                          @endphp
                          <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ date('d/m/Y H:i:s', strtotime($row->journal_date)) }}</td>
                            <td>{{ $accounts_library[$row->account_id]->account_name }} {{ $accounts_library[$row->account_id]->payment_type_name }}</td>
                            <td>IDR {{ number_format($row->journal_debet, 0, ',', '.') }}</td>
                            <td>IDR {{ number_format($row->journal_credit, 0, ',', '.') }}</td>
                            @if(!isset($branches))
                              <td>{{ $row->branch_name }}</td>
                            @endif
                            <td>
                              @if($row->restock_detail_id != "")
                                Pembelian [{{ $row->restock_invoice_number }}]
                              @elseif($row->transaction_detail_id != "")
                                Penjualan [{{ str_pad($row->transaction_invoice_number, 5, 0, STR_PAD_LEFT) }}]
                              @elseif($row->transfer_detail_id != "")
                                Transfer [{{ $row->transfer_invoice_number }}]
                              @elseif($row->other_transaction_id != "")
                                Biaya Lain [{{ $row->other_transaction_invoice_number }}]
                              @endif
                            </td>
                          </tr>
                        @endif
                      @else
                        @php
                          if($row->journal_debet != "" && $row->journal_debet != 0) {
                            $total_debet += $row->journal_debet;
                          } else if($row->journal_credit != "" && $row->journal_credit != 0) {
                            $total_credit += $row->journal_credit;
                          }
                        @endphp
                        <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{ date('d/m/Y H:i:s', strtotime($row->journal_date)) }}</td>
                          <td>{{ $accounts_library[$row->account_id]->account_name }} {{ $accounts_library[$row->account_id]->payment_type_name }}</td>
                          <td>IDR {{ number_format($row->journal_debet, 0, ',', '.') }}</td>
                          <td>IDR {{ number_format($row->journal_credit, 0, ',', '.') }}</td>
                          @if(!isset($branches))
                            <td>{{ $row->branch_name }}</td>
                          @endif
                          <td>
                            @if($row->restock_detail_id != "")
                              Pembelian [{{ $row->restock_invoice_number }}]
                            @elseif($row->transaction_detail_id != "")
                              Penjualan [{{ str_pad($row->transaction_invoice_number, 5, 0, STR_PAD_LEFT) }}]
                            @elseif($row->transfer_detail_id != "")
                              Transfer [{{ $row->transfer_invoice_number }}]
                            @elseif($row->other_transaction_id != "")
                              Biaya Lain [{{ $row->other_transaction_invoice_number }}]
                            @endif
                          </td>
                        </tr>
                      @endif
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Akun</th>
                      <th>Debet</th>
                      <th>Kredit</th>
                      @if(!isset($branches))
                        <th>Cabang</th>
                      @endif
                      <th>Kode Transaksi</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <h4>&nbsp;</h4>
              <table>
                <tr>
                  <td>Total Debet</td>
                  <td>&emsp;:&nbsp;</td>
                  <td>{{ number_format($total_debet, 0, ',', '.') }}</td>
                </tr>
                <tr>
                  <td>Total Kredit</td>
                  <td>&emsp;:&nbsp;</td>
                  <td>{{ number_format($total_credit, 0, ',', '.') }}</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('#check_all').change(function() {
      if($(this).prop("checked") == true) {
        $('.acc').prop('checked', true);
      } else {
        $('.acc').prop('checked', false);
      }
    });

    $('#dataTable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5 ]
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5 ]
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
      ]
    }); 
  </script>
@endsection
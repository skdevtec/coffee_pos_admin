@extends('layouts.master')

@section('title','Penjualan')

@section('heading','Penjualan')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Detail Penjualan {{ $transactions->transaction_invoice_number }}</h6>
            </div>
          </div>
        </div>
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-12 col-xs-12 col-md-12">
              <center>
                <table>
                  <tr>
                    <th>Tanggal</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ date('d F Y H:i:s', strtotime($transactions->transaction_date)) }}</td>
                  </tr>
                  <tr>
                    <th>Kasir</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ $transactions->user_name }}</td>
                  </tr>
                  <tr>
                    <th>Total</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>IDR {{ number_format($transactions->transaction_total_price, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <th>Diskon Nota</th>
                    <td>&emsp;:&nbsp;</td>
                    <td style="color: red;">IDR {{ number_format($transactions->transaction_discount_absolute, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <th>Diskon Menu</th>
                    <td>&emsp;:&nbsp;</td>
                    <td style="color: red;">IDR {{ number_format($transactions->transaction_total_discount - $transactions->transaction_discount_absolute, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <th>Grand Total</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>IDR {{ number_format($transactions->transaction_total_price - $transactions->transaction_total_discount, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <td colspan="3">&emsp;</td>
                  </tr>
                  <tr>
                    <th>Jenis</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ $transactions->transaction_type_name }}</td>
                  </tr>
                  <tr>
                    <th>Pembayaran</th>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ $transactions->payment_type_name }}</td>
                  </tr>
                </table>
              </center>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($transaction_details as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                      @if($row->menu_name != "")
                        {{ $row->menu_name }}
                      @elseif($row->ingredient_name != "")
                        {{ $row->ingredient_name }}
                      @endif
                    </td>
                    <td>{{ $row->transaction_detail_count }}</td>
                    <td>
                      @if($row->transaction_detail_price == $row->transaction_detail_price_after_discount)
                        IDR {{ number_format($row->transaction_detail_price, 0, ',', '.') }}
                      @else
                        <strike>IDR {{ number_format($row->transaction_detail_price, 0, ',', '.') }}</strike><br> IDR {{ number_format($row->transaction_detail_price_after_discount, 0, ',', '.') }}
                      @endif
                    </td>
                    <td>
                      @if($row->transaction_detail_subtotal == $row->transaction_detail_subtotal_after_discount)
                        IDR {{ number_format($row->transaction_detail_subtotal, 0, ',', '.') }}
                      @else
                        <strike>IDR {{ number_format($row->transaction_detail_subtotal, 0, ',', '.') }}</strike><br> IDR {{ number_format($row->transaction_detail_subtotal_after_discount, 0, ',', '.') }}
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
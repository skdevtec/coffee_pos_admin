@extends('layouts.master')

@section('title','Penjualan')

@section('heading','Penjualan')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Penjualan -
                @if(Auth::user()->user_role == "Super Admin")
                  {{ $stores->store_name }}
                @endif
                @if(isset($branches))
                  {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="" method="GET">
              <div class="row">
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Awal</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="min" name="min" required="" @if(isset($_GET['min'])) value="@php echo $_GET['min']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Tanggal Akhir</label>
                          <div class="col-sm-12">
                              <input type="date" class="form-control" id="max" name="max" required="" @if(isset($_GET['max'])) value="@php echo $_GET['max']; @endphp" @endif autocomplete="off" />
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="form-group row">
                          <label class="col-sm-12 col-form-label">&nbsp;</label>
                          <input type="submit" name="submit" value="Filter" class="btn btn-primary">
                          @if(isset($branches))
                            &nbsp;<a class="btn btn-danger" href="{{ url('/penjualan/' . $branches->branch_id) }}">Hapus Filter</a>
                          @else
                            &nbsp;<a class="btn btn-danger" href="{{ url('/penjualan/s' . $stores->store_id) }}">Hapus Filter</a>
                          @endif
                      </div>
                  </div>
              </div>
          </form>

          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th style="width: 125px;">Total</th>
                  <th style="width: 125px;">Diskon</th>
                  <th style="width: 125px;">Potongan</th>
                  <th style="width: 125px;">Grand Total</th>
                  <th>Jenis</th>
                  <th>Status</th>
                  <th>Kas</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @php
                  $total_price = 0;
                  $total_discount_absolute = 0;
                  $total_charge = 0;
                  $payment_recap = [];
                  $type_total_recap = [];
                  $type_count_recap = [];
                  $transaction_recap['success'] = 0;
                  $transaction_recap['failed'] = 0;
                @endphp
                @foreach($transactions as $row)
                  @if(isset($_GET['min']))
                    @if(strtotime($_GET['min'] . " 00:00:00") <= strtotime($row->transaction_date) && strtotime($_GET['max'] . " 23:59:59") >= strtotime($row->transaction_date))
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ date('d/m/Y', strtotime($row->transaction_date)) }}</td>
                        <td>{{ $row->transaction_invoice_number }}</td>
                        <td>IDR {{ number_format($row->transaction_total_price, 0, ',', '.') }}</td>
                        <td>IDR {{ number_format($row->transaction_total_discount, 0, ',', '.') }}</td>
                        <td>
                          @php
                            $charge = 0;
                            $grand_total = $row->transaction_total_price - $row->transaction_total_discount;
                          @endphp

                          @if($row->transaction_charge_absolute != 0)
                            @php
                              $charge += $row->transaction_charge_absolute;
                              $total_charge += $charge;
                              $grand_total = $grand_total - $charge;
                            @endphp
                          @endif
                          @if($row->transaction_charge_percentage != 0)
                            @php
                              $charge += $grand_total * $row->transaction_charge_percentage / 100;
                              $total_charge += $charge;
                              $grand_total = $grand_total - $charge;
                            @endphp
                          @endif
                          IDR {{ number_format($charge, 0, ',', '.') }}
                        </td>
                        <td>
                          IDR {{ number_format($grand_total, 0, ',', '.') }}
                        </td>
                        <td>
                          {{ $row->transaction_type_name }}
                          @php
                            if(isset($type_count_recap[$row->transaction_type_name])) {
                              $type_total_recap[$row->transaction_type_name] += $grand_total;
                              $type_count_recap[$row->transaction_type_name]++;
                            } else {
                              $type_total_recap[$row->transaction_type_name] = $grand_total;
                              $type_count_recap[$row->transaction_type_name] = 1;
                            }
                          @endphp
                        </td>
                        <td>
                          @if($row->transaction_delete == 0)
                            @php
                              $total_price += $row->transaction_total_price;
                              $total_discount_absolute += $row->transaction_total_discount;

                              if(isset($payment_recap[$row->payment_type_name])) {
                                $payment_recap[$row->payment_type_name] += $grand_total;
                              } else {
                                $payment_recap[$row->payment_type_name] = $grand_total;
                              }
                              $transaction_recap['success']++;
                            @endphp
                            OK
                          @else
                            @php
                              $transaction_recap['failed']++;
                            @endphp
                            Void. <br>
                            Alasan :
                            @if($row->transaction_delete_reason == "")
                              -
                            @else
                              {{ $row->transaction_delete_reason }}
                            @endif
                          @endif
                        </td>
                        <td>{{ $row->payment_type_name }}</td>
                        @if(!isset($branches))
                          <td>{{ $row->branch_name }}</td>
                        @endif
                        <td>
                          <a class="btn btn-primary" href="{{ url('invoice_download/penjualan/' . $row->transaction_id) }}">{{ __('Unduh') }}</a>
                          <a class="btn btn-success" href="{{ route('penjualan.show', 'dt' . $row->transaction_id) }}">{{ __('Lihat') }}</a>
                          @if($row->transaction_delete == 0)
                            <a class="btn btn-danger delete" data-id="{{ $row->transaction_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                          @endif
                        </td>
                      </tr>
                    @endif
                  @else
                    <tr>
                      <td>{{ $loop->iteration }}</td>
                      <td>{{ date('d/m/Y', strtotime($row->transaction_date)) }}</td>
                      <td>{{ $row->transaction_invoice_number }}</td>
                      <td>IDR {{ number_format($row->transaction_total_price, 0, ',', '.') }}</td>
                      <td>IDR {{ number_format($row->transaction_total_discount, 0, ',', '.') }}</td>
                      <td>
                        @php
                          $charge = 0;
                          $grand_total = $row->transaction_total_price - $row->transaction_total_discount;
                        @endphp

                        @if($row->transaction_charge_absolute != 0)
                          @php
                            $charge += $row->transaction_charge_absolute;
                            $total_charge += $charge;
                            $grand_total = $grand_total - $charge;
                          @endphp
                        @endif
                        @if($row->transaction_charge_percentage != 0)
                          @php
                            $charge += $grand_total * $row->transaction_charge_percentage / 100;
                            $total_charge += $charge;
                            $grand_total = $grand_total - $charge;
                          @endphp
                        @endif
                        IDR {{ number_format($charge, 0, ',', '.') }}
                      </td>
                      <td>
                        IDR {{ number_format($grand_total, 0, ',', '.') }}
                      </td>
                      <td>
                        {{ $row->transaction_type_name }}
                        @php
                          if(isset($type_count_recap[$row->transaction_type_name])) {
                            $type_total_recap[$row->transaction_type_name] += $grand_total;
                            $type_count_recap[$row->transaction_type_name]++;
                          } else {
                            $type_total_recap[$row->transaction_type_name] = $grand_total;
                            $type_count_recap[$row->transaction_type_name] = 1;
                          }
                        @endphp
                      </td>
                      <td>
                        @if($row->transaction_delete == 0)
                          @php
                            $total_price += $row->transaction_total_price;
                            $total_discount_absolute += $row->transaction_total_discount;

                            if(isset($payment_recap[$row->payment_type_name])) {
                              $payment_recap[$row->payment_type_name] += $grand_total;
                            } else {
                              $payment_recap[$row->payment_type_name] = $grand_total;
                            }
                            $transaction_recap['success']++;
                          @endphp
                          OK
                        @else
                          @php
                            $transaction_recap['failed']++;
                          @endphp
                          Void. <br>
                          Alasan :
                          @if($row->transaction_delete_reason == "")
                            -
                          @else
                            {{ $row->transaction_delete_reason }}
                          @endif
                        @endif
                      </td>
                      <td>{{ $row->payment_type_name }}</td>
                      @if(!isset($branches))
                        <td>{{ $row->branch_name }}</td>
                      @endif
                      <td>
                        <a class="btn btn-primary" href="{{ url('invoice_download/penjualan/' . $row->transaction_id) }}">{{ __('Unduh') }}</a>
                        <a class="btn btn-success" href="{{ route('penjualan.show', 'dt' . $row->transaction_id) }}">{{ __('Lihat') }}</a>
                        @if($row->transaction_delete == 0)
                          <a class="btn btn-danger delete" data-id="{{ $row->transaction_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                        @endif
                      </td>
                    </tr>
                  @endif
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Nomor Nota</th>
                  <th>Total</th>
                  <th>Diskon</th>
                  <th>Potongan</th>
                  <th>Grand Total</th>
                  <th>Jenis</th>
                  <th>Status</th>
                  <th>Kas</th>
                  @if(!isset($branches))
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Rekap Penjualan</h6>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="block">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-xs-4">
                <table>
                  <tr>
                    <td>Jumlah Penjualan</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ number_format($transaction_recap['success'] + $transaction_recap['failed'], 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <td>Jumlah Penjualan Berhasil</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ number_format($transaction_recap['success'], 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <td>Jumlah Penjualan Gagal</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>{{ number_format($transaction_recap['failed'], 0, ',', '.') }}</td>
                  </tr>
                </table>
              </div>
              <div class="col-lg-4 col-md-4 col-xs-4">
                <table>
                  <tr>
                    <td>Total Penjualan</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>IDR {{ number_format($total_price, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <td>Total Diskon</td>
                    <td>&emsp;:&nbsp;</td>
                    <td style="color: red;">IDR {{ number_format($total_discount_absolute, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <td>Total Potongan</td>
                    <td>&emsp;:&nbsp;</td>
                    <td style="color: red;">IDR {{ number_format($total_charge, 0, ',', '.') }}</td>
                  </tr>
                  <tr>
                    <td>Grand Total</td>
                    <td>&emsp;:&nbsp;</td>
                    <td>IDR {{ number_format($total_price - $total_discount_absolute - $total_charge, 0, ',', '.') }}</td>
                  </tr>
                </table>
              </div>
              <div class="col-lg-4 col-md-4 col-xs-4">
                @if(count($payment_recap) > 0)
                  <table>
                    @foreach($payment_recap as $payment => $value)
                      <tr>
                        <td>Total Kas {{ $payment }}</td>
                        <td>&emsp;:&nbsp;</td>
                        <td>IDR {{ number_format($value, 0, ',', '.') }}</td>
                      </tr>
                    @endforeach
                  </table>
                @endif
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-xs-4">
                @if(count($type_count_recap) > 0)
                  <table>
                    @foreach($type_count_recap as $type => $value)
                      <tr>
                        <td>Jumlah Penjualan {{ $type }}</td>
                        <td>&emsp;:&nbsp;</td>
                        <td>{{ number_format($value, 0, ',', '.') }}</td>
                      </tr>
                    @endforeach
                  </table>
                @endif
              </div>
              <div class="col-lg-4 col-md-4 col-xs-4">
                @if(count($type_total_recap) > 0)
                  <table>
                    @foreach($type_total_recap as $type => $value)
                      <tr>
                        <td>Total Penjualan {{ $type }}</td>
                        <td>&emsp;:&nbsp;</td>
                        <td>IDR {{ number_format($value, 0, ',', '.') }}</td>
                      </tr>
                    @endforeach
                  </table>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          Apakah anda ingin menghapus data penjualan ini?
          @csrf
          <input type="text" class="form-control" name="transaction_delete_reason" id="transaction_delete_reason" placeholder="Alasan Penghapusan">
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal" onclick="clear_text();">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('#dataTable').DataTable({
      dom: 'Bfrtip',
      buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            }
        },
        {
            extend: 'pdfHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
      ]
    });

    function clear_text() {
      $('#transaction_delete_reason').val('');
    }

    $(document).delegate('.delete', 'click', function() {
      var transaction_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $(document).delegate('#button_delete', 'click', function() {
        var _token = $('input[name=_token]').val();
        var transaction_delete_reason = $('#transaction_delete_reason').val();
        
        var url = "{{ route('penjualan.update', 'transaction_id') }}";
        url = url.replace('transaction_id', transaction_id);

        $.ajax({
          url: url,
          method: "PUT",  
          data: {
            transaction_id : transaction_id, 
            transaction_delete_reason : transaction_delete_reason, 
            _token : _token
          },
          success: function(result) {
            $('#transaction_delete_reason').val('');
            location.reload();
          },
          error: function(xhr) {
            console.log(xhr);
          }
        });
      });
    }); 
  </script>
@endsection
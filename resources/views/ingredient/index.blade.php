@extends('layouts.master')

@section('title','Bahan Baku')

@section('heading','Bahan Baku')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Bahan Baku
                @if(Auth::user()->user_role == "Admin")
                  - {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
            @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
              <div class="col-lg-6 col-xs-6 col-md-6">
                <select class="form-control" id="filter_branch">
                  <option value="">-- Pilih Cabang --</option>
                  @foreach($branches as $row)
                    @if(Auth::user()->user_role == "Super Admin")
                      <option value="{{ $row->store_name }} - {{ $row->branch_name }}">{{ $row->store_name }} - {{ $row->branch_name }}</option>
                    @else
                      <option value="{{ $row->branch_name }}">{{ $row->branch_name }}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th style="width: 150px;">Harga Beli</th>
                  <th>Stok</th>
                  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($ingredients as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->ingredient_name }}</td>
                    <td>IDR {{ number_format($row->ingredient_buy_price, 0, ',', '.') }}</td>
                    <td>{{ number_format($row->ingredient_stock, 0, ',', '.') }} {{ $row->unit_name }}</td>
                    @if(Auth::user()->user_role == "Super Admin")
                      <td>{{ $row->store_name }} - {{ $row->branch_name }}</td>
                    @elseif(Auth::user()->user_role == "Owner")
                      <td>{{ $row->branch_name }}</td>
                    @endif
                    <td>
                      <a class="btn btn-primary edit" data-id="{{ $row->ingredient_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                      @if(Auth::user()->user_role == "Super Admin")
                        <a class="btn btn-danger delete" data-id="{{ $row->ingredient_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Harga Beli</th>
                  <th>Stok</th>
                  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                    <th>Cabang</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Bahan Baku
            @if(Auth::user()->user_role == "Admin")
              - {{ $branches->branch_name }}
            @endif
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="ingredient_name" class="col-sm-3 col-form-label text-md-right">
              {{ __('Nama') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="ingredient_name" name="ingredient_name" value="">
              <p style="color: red;" id="ingredient_name_feedback">
                <strong>Nama bahan baku wajib diisi!</strong>
              </p>
            </div>
          </div>

          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="store_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Brand') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="store_id" name="store_id">
                  <option value="">---- Pilih Brand ----</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="store_id_feedback">
                  <strong>Brand wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @else
            <input type="text" id="store_id" value="{{ Auth::user()->store_id }}" hidden>
          @endif

          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="branch_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Cabang') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="branch_id" name="branch_id">
                  <option value="">---- Pilih Brand ----</option>
                </select>
                <p style="color: red;" id="branch_id_feedback">
                  <strong>Cabang wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Owner")
            <div class="form-group row">
              <label for="branch_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Cabang') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="branch_id" name="branch_id">
                  <option value="">---- Pilih Cabang ----</option>
                  <option value="all">Semua</option>
                  @foreach($branches as $row)
                    <option value="{{ $row->branch_id }}">{{ $row->branch_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="branch_id_feedback">
                  <strong>Cabang wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Admin")
            <input type="text" id="branch_id" value="{{ Auth::user()->branch_id }}" hidden>
          @endif

          <div class="form-group row">
            <label for="unit_id" class="col-sm-3 col-form-label text-md-right">
              {{ __('Satuan') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <select class="form-control" id="unit_id" name="unit_id">
                @if(Auth::user()->user_role == "Super Admin")
                  <option value="">---- Pilih Brand ----</option>
                @else
                  <option value="">---- Pilih Satuan ----</option>
                  @foreach($units as $row)
                    <option value="{{ $row->unit_id }}">{{ $row->unit_name }}</option>
                  @endforeach
                @endif
              </select>
              <p style="color: red;" id="unit_id_feedback">
                <strong>Satuan wajib dipilih!</strong>
              </p>
            </div>
          </div>
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="ingredient_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data bahan baku ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#ingredient_name_feedback').attr('hidden', 'hidden');
    $('#unit_id_feedback').attr('hidden', 'hidden');
    $('#store_id_feedback').attr('hidden', 'hidden');
    $('#branch_id_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    var table = $('#dataTable').DataTable();
    $('#filter_branch').change(function() {
      table.column(4).search($('#filter_branch').val());
      table.draw();
    });

    $('#store_id').change(function() {      
      var url = "{{ url('/get_branch/store_id') }}";
      url = url.replace('store_id', $('#store_id').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#branch_id').html(``);
          $('#branch_id').append(`<option value="">---- Pilih Cabang ----</option>`);
          $('#branch_id').append(`<option value="all">Semua</option>`);

          $.each(result.data, function(i,index){
            $('#branch_id').append(`
                <option value="` + result.data[i]['branch_id'] + `">` + result.data[i]['branch_name'] + `</option>
              `);
          });
        }
      });

      url = "{{ url('/get_unit/sstore_id') }}";
      url = url.replace('store_id', $('#store_id').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#unit_id').html(``);
          $('#unit_id').append(`<option value="">---- Pilih Satuan ----</option>`);

          $.each(result.data, function(i,index){
            $('#unit_id').append(`
                <option value="` + result.data[i]['unit_id'] + `">` + result.data[i]['unit_name'] + `</option>
              `);
          });
        }
      });
    });

    $('#submit').click(function() {
      $('#ingredient_name_feedback').attr('hidden', 'hidden');
      $('#unit_id_feedback').attr('hidden', 'hidden');
      $('#store_id_feedback').attr('hidden', 'hidden');
      $('#branch_id_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var ingredient_name = $('#ingredient_name').val();
        var unit_id = $('#unit_id').val();
        var store_id = $('#store_id').val();
        var branch_id = $('#branch_id').val();
        var _token = $('input[name=_token]').val();
        
        if(ingredient_name == "") {
          $('#ingredient_name_feedback').removeAttr('hidden');
        } else if(unit_id == "") {
          $('#unit_id_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else if(branch_id == "") {
          $('#branch_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: "{{ route('bahan_baku.store') }}",
            method: "POST",  
            data: {
              ingredient_name : ingredient_name, 
              unit_id : unit_id, 
              store_id : store_id, 
              branch_id : branch_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var ingredient_id = $('#ingredient_id').val();
        var ingredient_name = $('#ingredient_name').val();
        var unit_id = $('#unit_id').val();
        var store_id = $('#store_id').val();
        var branch_id = $('#branch_id').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('bahan_baku.update', 'ingredient_id') }}";
        url = url.replace('ingredient_id', ingredient_id);

        if(ingredient_name == "") {
          $('#ingredient_name_feedback').removeAttr('hidden');
        } else if(unit_id == "") {
          $('#unit_id_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else if(branch_id == "") {
          $('#branch_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          $.ajax({
            url: url,
            method: "PUT",  
            data: {
              ingredient_id : ingredient_id, 
              ingredient_name : ingredient_name, 
              unit_id : unit_id, 
              store_id : store_id, 
              branch_id : branch_id, 
              _token : _token
            },
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');

      var ingredient_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('bahan_baku.show', 'ingredient_id') }}";
      url = url.replace('ingredient_id', ingredient_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          ingredient_id : ingredient_id, 
          _token : _token
        },
        success: function(result) {
          $('#ingredient_id').val(ingredient_id);
          $('#ingredient_name').val(result.data.ingredient_name);
          $('#store_id').val(result.data.store_id);
          $('#store_id').change();
          setTimeout(function() {
            $('#branch_id').val(result.data.branch_id);
            $('#unit_id').val(result.data.unit_id);
          }, 1000);
        }
      });
    });

    var ingredient_id = "";
    $(document).delegate('.delete', 'click', function() {
      ingredient_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('bahan_baku.destroy', 'ingredient_id') }}";
        url = url.replace('ingredient_id', ingredient_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            ingredient_id : ingredient_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');

      $('#ingredient_id').val('');
      $('#ingredient_name').val('');
      $('#store_id').val('');
      $('#branch_id').val('');
      $('#unit_id').val('');
      if("{{ Auth::user()->user_role }}" != "Super Admin" && "{{ Auth::user()->user_role }}" != "Owner") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
        $('#branch_id').val("{{ Auth::user()->branch_id }}");
      } else if("{{ Auth::user()->user_role }}" == "Owner") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
      }
    });

    $('#button_add').click();
    if("{{ Auth::user()->user_role }}" != "Super Admin" && "{{ Auth::user()->user_role }}" != "Owner") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
      $('#branch_id').val("{{ Auth::user()->branch_id }}")
    } else if("{{ Auth::user()->user_role }}" == "Owner") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
    }
  </script>
@endsection
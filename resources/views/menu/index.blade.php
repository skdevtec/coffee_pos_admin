@extends('layouts.master')

@section('title','Menu')

@section('heading','Menu')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <div class="row">
            <div class="col-lg-6 col-xs-6 col-md-6">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Menu
                @if(Auth::user()->user_role == "Admin")
                  - {{ $branches->branch_name }}
                @endif
              </h6>
            </div>
            @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
              <div class="col-lg-6 col-xs-6 col-md-6">
                <select class="form-control" id="filter_branch">
                  <option value="">-- Pilih Cabang --</option>
                  @foreach($branches as $row)
                    @if(Auth::user()->user_role == "Super Admin")
                      <option value="{{ $row->store_name }} - {{ $row->branch_name }}">{{ $row->store_name }} - {{ $row->branch_name }}</option>
                    @else
                      <option value="{{ $row->branch_name }}">{{ $row->branch_name }}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th style="width: 150px;">Harga Jual</th>
                  <th>Harga Poin</th>
                  <th>Diskon</th>
                  <th>Kategori</th>
                  <th>Gambar</th>
                  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                    <th>Cabang</th>
                  @endif
                  <th>Resep</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($menus as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->menu_code }}</td>
                    <td>{{ $row->menu_name }}</td>
                    <td>
                      @if($row->menu_discount == 0)
                        IDR {{ number_format($row->menu_sell_price, 0, ',', '.') }}
                      @else
                        <strike>IDR {{ number_format($row->menu_sell_price, 0, ',', '.') }}</strike><br>
                        IDR {{ number_format($row->menu_sell_price * (100 - $row->menu_discount) / 100, 0, ',', '.') }}
                      @endif
                    </td>
                    <td>{{ number_format($row->menu_point_price, 0, ',', '.') }}</td>
                    <td>{{ $row->menu_discount }}%</td>
                    <td>{{ $row->category_name }}</td>
                    <td><img src="{{ asset('images/menu/' . $row->menu_photo) }}" style="width: 100px;"></td>
                    @if(Auth::user()->user_role == "Super Admin")
                      <td>{{ $row->store_name }} - {{ $row->branch_name }}</td>
                    @elseif(Auth::user()->user_role == "Owner")
                      <td>{{ $row->branch_name }}</td>
                    @endif
                    <td>
                      @if(isset($menu_ingredients[$row->menu_id]))
                        <table>
                          @foreach($menu_ingredients[$row->menu_id] as $mi)
                            <tr>
                              <td>{{ $mi->ingredient_name }}</td>
                              <td>{{ $mi->menu_ingredient_count }}{{ $mi->unit_name }}</td>
                            </tr>
                          @endforeach
                        </table>
                      @endif
                      <a class="btn btn-success ingredient" data-id="{{ $row->menu_id }}" data-name="{{ $row->menu_name }}" data-branch="{{ $row->branch_id }}" href="javascript:void(0)" @if(isset($menu_ingredients[$row->menu_id])) style="float: right;" @endif>{{ __('Atur') }}</a>
                    </td>
                    <td>
                      <a class="btn btn-primary edit" data-id="{{ $row->menu_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                      <a class="btn btn-danger delete" data-id="{{ $row->menu_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Harga Jual</th>
                  <th>Harga Poin</th>
                  <th>Diskon</th>
                  <th>Kategori</th>
                  <th>Gambar</th>
                  @if(Auth::user()->user_role == "Super Admin" || Auth::user()->user_role == "Owner")
                    <th>Cabang</th>
                  @endif
                  <th>Resep</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Menu
            @if(Auth::user()->user_role == "Admin")
              - {{ $branches->branch_name }}
            @endif
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="menu_code" class="col-sm-3 col-form-label text-md-right">
              {{ __('Kode') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="menu_code" name="menu_code" value="">
              <p style="color: red;" id="menu_code_feedback">
                <strong>Kode menu wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="menu_name" class="col-sm-3 col-form-label text-md-right">
              {{ __('Nama') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="menu_name" name="menu_name" value="">
              <p style="color: red;" id="menu_name_feedback">
                <strong>Nama menu wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="menu_sell_price" class="col-sm-3 col-form-label text-md-right">
              {{ __('Harga Jual') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="number" class="form-control" id="menu_sell_price" name="menu_sell_price" value="">
              <p style="color: red;" id="menu_sell_price_feedback">
                <strong>Harga jual menu wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="menu_point_price" class="col-sm-3 col-form-label text-md-right">
              {{ __('Harga Poin') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="number" class="form-control" id="menu_point_price" name="menu_point_price" value="">
              <p style="color: red;" id="menu_point_price_feedback">
                <strong>Harga poin menu wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="menu_discount" class="col-sm-3 col-form-label text-md-right">
              {{ __('Diskon (%)') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="number" class="form-control" id="menu_discount" name="menu_discount" max="100" value="">
              <p style="color: red;" id="menu_discount_feedback">
                <strong>Diskon menu wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="menu_photo" class="col-sm-3 col-form-label text-md-right">
              {{ __('Gambar') }} <abbr id="required_menu_photo" style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <div id="preview_image_div">
                <img id="preview_image" style="width: 100%;" src="">
              </div>
              <input type="file" id="menu_photo" name="menu_photo" onchange="fileValidation()">
              <p style="color: red;" id="menu_photo_feedback">
                <strong>Gambar menu wajib diisi!</strong>
              </p>
              <p style="color: red;" id="menu_photo_feedback_extention">
                <strong>Ekstensi berkas yang didukung adalah jpg atau png!</strong>
              </p>
              <p style="color: red;" id="menu_photo_feedback_size">
                <strong>Maksimal ukuran berkas adalah 1MB!</strong>
              </p>
            </div>
          </div>
          
          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="store_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Brand') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="store_id" name="store_id">
                  <option value="">---- Pilih Brand ----</option>
                  @foreach($stores as $row)
                    <option value="{{ $row->store_id }}">{{ $row->store_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="store_id_feedback">
                  <strong>Brand wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @else
            <input type="text" id="store_id" value="{{ Auth::user()->store_id }}" hidden>
          @endif

          @if(Auth::user()->user_role == "Super Admin")
            <div class="form-group row">
              <label for="branch_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Cabang') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="branch_id" name="branch_id">
                  <option value="">---- Pilih Brand ----</option>
                </select>
                <p style="color: red;" id="branch_id_feedback">
                  <strong>Cabang wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Owner")
            <div class="form-group row">
              <label for="branch_id" class="col-sm-3 col-form-label text-md-right">
                {{ __('Cabang') }} <abbr style="color: red;">*</abbr>
              </label>
              <div class="col-sm-7">
                <select class="form-control" id="branch_id" name="branch_id">
                  <option value="">---- Pilih Cabang ----</option>
                  <option value="all">Semua</option>
                  @foreach($branches as $row)
                    <option value="{{ $row->branch_id }}">{{ $row->branch_name }}</option>
                  @endforeach
                </select>
                <p style="color: red;" id="branch_id_feedback">
                  <strong>Cabang wajib dipilih!</strong>
                </p>
              </div>
            </div>
          @elseif(Auth::user()->user_role == "Admin")
            <input type="text" id="branch_id" value="{{ Auth::user()->branch_id }}" hidden>
          @endif

          <div class="form-group row">
            <label for="category_id" class="col-sm-3 col-form-label text-md-right">
              {{ __('Kategori') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <select class="form-control" id="category_id" name="category_id">
                @if(Auth::user()->user_role == "Super Admin")
                  <option value="">---- Pilih Brand ----</option>
                @elseif(Auth::user()->user_role == "Owner" || Auth::user()->user_role == "Admin")
                  <option value="">---- Pilih Kategori ----</option>
                  @foreach($categories as $row)
                    <option value="{{ $row->category_id }}">{{ $row->category_name }}</option>
                  @endforeach
                @endif
              </select>
              <p style="color: red;" id="category_id_feedback">
                <strong>Kategori wajib dipilih!</strong>
              </p>
            </div>
          </div>
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="menu_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            Atur Resep Menu
            @if(Auth::user()->user_role == "Admin")
              - {{ $branches->branch_name }}
            @endif
            <div class="spinner-border text-primary pull-right" id="loading_recipe" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="menu_code" class="col-sm-4 col-form-label text-md-right">
              {{ __('Nama Menu') }}
            </label>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="menu_name_recipe" name="menu_name_recipe" readonly="" value="">
            </div>
          </div>
          <div id="body_recipe">
            <div class="form-group row">
              <div class="col-sm-2">
              </div>
              <div class="col-sm-5">
                <select class="form-control" id="ingredient_id0" name="ingredient_id">
                  <option>-- Pilih Bahan Baku --</option>
                </select>
                <p style="color: red;" id="ingredient_id0_feedback">
                  <strong>Bahan baku wajib dipilih!</strong>
                </p>
              </div>
              <div class="col-sm-3">
                <input type="number" class="form-control" id="menu_ingredient_count0" name="menu_ingredient_count" value="0">
                <p style="color: red;" id="menu_ingredient_count0_feedback">
                  <strong>Jumlah wajib diisi!</strong>
                </p>
              </div>
              <div class="col-sm-1">
                <button class="btn btn-success" id="add_recipe">+</button>
              </div>
            </div>
          </div>
          <div class="form-group">
            <input type="text" id="menu_id_recipe" value="" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit_recipe">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data menu ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#menu_photo_feedback_extention').attr('hidden', 'hidden');
    $('#menu_photo_feedback_size').attr('hidden', 'hidden');
    function fileValidation() {
      $('#menu_photo_feedback_extention').attr('hidden', 'hidden');
      $('#menu_photo_feedback_size').attr('hidden', 'hidden');

      var input, file;
      input = document.getElementById('menu_photo');
      file = input.files[0];
  
      var fileInput = document.getElementById('menu_photo');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.jpg|\.png)$/i;
      if(!allowedExtensions.exec(filePath)){
        $('#menu_photo_feedback_extention').removeAttr('hidden');
  
        fileInput.value = "";
        return false;
      } else if(file.size > 1048576){
        $('#menu_photo_feedback_size').removeAttr('hidden');
  
        fileInput.value = "";
        return false;
      }
    }

    $('#preview_image_div').attr('hidden', 'hidden');
    $('#menu_code_feedback').attr('hidden', 'hidden');
    $('#menu_name_feedback').attr('hidden', 'hidden');
    $('#menu_sell_price_feedback').attr('hidden', 'hidden');
    $('#menu_point_price_feedback').attr('hidden', 'hidden');
    $('#menu_discount_feedback').attr('hidden', 'hidden');
    $('#menu_photo_feedback').attr('hidden', 'hidden');
    $('#category_id_feedback').attr('hidden', 'hidden');
    $('#store_id_feedback').attr('hidden', 'hidden');
    $('#branch_id_feedback').attr('hidden', 'hidden');

    $('#ingredient_id0_feedback').attr('hidden', 'hidden');
    $('#menu_ingredient_count0_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');
    $('#submit_recipe').attr('disabled', 'disabled');
    $('#add_recipe').attr('disabled', 'disabled');

    var table = $('#dataTable').DataTable();
    $('#filter_branch').change(function() {
      table.column(8).search($('#filter_branch').val());
      table.draw();
    });

    $('#store_id').change(function() {      
      var url = "{{ url('/get_branch/store_id') }}";
      url = url.replace('store_id', $('#store_id').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#branch_id').html(``);
          $('#branch_id').append(`<option value="">---- Pilih Cabang ----</option>`);
          $('#branch_id').append(`<option value="all">Semua</option>`);

          $.each(result.data, function(i,index){
            $('#branch_id').append(`
                <option value="` + result.data[i]['branch_id'] + `">` + result.data[i]['branch_name'] + `</option>
              `);
          });
        }
      });

      url = "{{ url('/get_category/sstore_id') }}";
      url = url.replace('store_id', $('#store_id').val());

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $('#category_id').html(``);
          $('#category_id').append(`<option value="">---- Pilih Kategori ----</option>`);

          $.each(result.data, function(i,index){
            $('#category_id').append(`
                <option value="` + result.data[i]['category_id'] + `">` + result.data[i]['category_name'] + `</option>
              `);
          });
        }
      });
    });

    var array_ingredient = new Array();
    var array_recipe_count = new Array();
    array_recipe_count.push(0);
    var branch_id = 0;
    var count = 0;
    $('#add_recipe').click(function() {   
      count++;
      array_recipe_count.push(count);
      $('#body_recipe').append(`
        <div class="form-group row" id="list_ingredient` + count + `">
          <div class="col-sm-2">
          </div>
          <div class="col-sm-5">
            <select class="form-control" id="ingredient_id` + count + `" name="ingredient_id">
              <option value="">-- Pilih Bahan Baku --</option>
            </select>
            <p style="color: red;" id="ingredient_id` + count + `_feedback">
              <strong>Bahan baku wajib dipilih!</strong>
            </p>
          </div>
          <div class="col-sm-3">
            <input type="number" class="form-control" id="menu_ingredient_count` + count + `" name="menu_ingredient_count" value="0">
            <p style="color: red;" id="menu_ingredient_count` + count + `_feedback">
              <strong>Jumlah wajib diisi!</strong>
            </p>
          </div>
          <div class="col-sm-1">
            <button class="btn btn-danger" onclick="remove_ingredient('` + count + `');">-</button>
          </div>
        </div>
      `);

      $('#ingredient_id' + count + '_feedback').attr('hidden', 'hidden');
      $('#menu_ingredient_count' + count + '_feedback').attr('hidden', 'hidden');
      
      $('#ingredient_id' + count).html(``);
      $('#ingredient_id' + count).append(`<option value="">-- Pilih Bahan Baku --</option>`);

      $.each(array_ingredient, function(i,index){
        $('#ingredient_id' + count).append(`
            <option value="` + array_ingredient[i]['ingredient_id'] + `">` + array_ingredient[i]['ingredient_name'] + ` - ` + array_ingredient[i]['unit_name'] + `</option>
          `);
      });
    });

    function remove_ingredient(id) {
      var array_recipe_count_temp = new Array();
      for(var i = 0; i < array_recipe_count.length; i++) {
        if(array_recipe_count[i] != id) {
          array_recipe_count_temp.push(array_recipe_count[i]);
        }
      }
      array_recipe_count = array_recipe_count_temp;

      $('#list_ingredient' + id).remove();
    }

    $('#submit_recipe').click(function() {
      for(i = 0; i <= count; i++) {
        $('#ingredient_id' + i + '_feedback').attr('hidden', 'hidden');
        $('#menu_ingredient_count' + i + '_feedback').attr('hidden', 'hidden');
      }

      var status = true;
      for(i = 0; i <= count; i++) {
        var ingredient_id = $('#ingredient_id' + i).val();
        var menu_ingredient_count = $('#menu_ingredient_count' + i).val();

        if(ingredient_id !== undefined) {
          if(ingredient_id == "") {
            $('#ingredient_id' + i + '_feedback').removeAttr('hidden');
            status = false;
          }
          if(menu_ingredient_count == 0) {
            $('#menu_ingredient_count' + i + '_feedback').removeAttr('hidden');
            status = false;
          }
        }
      }

      if(status === true) {
        $('#loading_recipe').removeAttr('hidden');
        var menu_id = $('#menu_id_recipe').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('bahan_baku_menu.destroy', 'menu_id') }}";
        url = url.replace('menu_id', menu_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            menu_id : menu_id, 
            _token : _token
          },
          success: function(result) {
            
          }
        });

        for(i = 0; i <= count; i++) {
          var ingredient_id = $('#ingredient_id' + i).val();
          var menu_ingredient_count = $('#menu_ingredient_count' + i).val();

          $.ajax({
            url: "{{ route('bahan_baku_menu.store') }}",
            method: "POST",  
            data: {
              ingredient_id : ingredient_id, 
              menu_id : menu_id, 
              menu_ingredient_count : menu_ingredient_count, 
              _token : _token
            },
            success: function(result) {

            },
            error: function(xhr) {
              console.log(xhr);
            }
          });
        }

        setInterval(
          function() { location.reload(); }, 
          3000
        );
      }
    });

    $('.ingredient').click(function() {
      for(i = 1; i <= count; i++) {
        $('#list_ingredient' + i).remove();
      }
      count = 0;
      $('#ingredient_id0').val('');
      $('#menu_ingredient_count0').val(0);

      var menu_id = $(this).data('id');
      var menu_name = $(this).data('name');
      branch_id = $(this).data('branch');

      var url = "{{ url('/get_ingredient/branch_id') }}";
      url = url.replace('branch_id', branch_id);

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          array_ingredient = result.data;

          $('#ingredient_id0').html(``);
          $('#ingredient_id0').append(`<option value="">-- Pilih Bahan Baku --</option>`);

          $.each(array_ingredient, function(i,index){
            $('#ingredient_id0').append(`
                <option value="` + array_ingredient[i]['ingredient_id'] + `">` + array_ingredient[i]['ingredient_name'] + ` - ` + array_ingredient[i]['unit_name'] + `</option>
              `);
          });
        },
        error: function(xhr) {
          console.log(xhr);
        }
      });

      // load ingredient yang telah tersimpan
      var url = "{{ url('/bahan_baku_menu/menu_id') }}";
      url = url.replace('menu_id', menu_id);

      $.ajax({
        url: url,
        method: "GET", 
        data: {

        },
        success: function(result) {
          $.each(result.data, function(i,index){
            if(i != 0) {
              $('#add_recipe').click(); 
            }
          });

          $.each(result.data, function(i,index){
            $('#ingredient_id' + i).val(result.data[i]['ingredient_id']);
            $('#menu_ingredient_count' + i).val(result.data[i]['menu_ingredient_count']);
          });
        },
        error: function(xhr) {
          console.log(xhr);
        }
      });

      $('#menu_id_recipe').val(menu_id);
      $('#menu_name_recipe').val(menu_name);

      $('#submit_recipe').removeAttr('disabled');
      $('#add_recipe').removeAttr('disabled');
    });

    $('#submit').click(function() {
      $('#menu_code_feedback').attr('hidden', 'hidden');
      $('#menu_name_feedback').attr('hidden', 'hidden');
      $('#menu_sell_price_feedback').attr('hidden', 'hidden');
      $('#menu_point_price_feedback').attr('hidden', 'hidden');
      $('#menu_discount_feedback').attr('hidden', 'hidden');
      $('#menu_photo_feedback').attr('hidden', 'hidden');
      $('#category_id_feedback').attr('hidden', 'hidden');
      $('#store_id_feedback').attr('hidden', 'hidden');
      $('#branch_id_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var menu_code = $('#menu_code').val();
        var menu_name = $('#menu_name').val();
        var menu_sell_price = $('#menu_sell_price').val();
        var menu_point_price = $('#menu_point_price').val();
        var menu_discount = $('#menu_discount').val();
        var menu_photo = $('#menu_photo').get(0).files.length;
        var category_id = $('#category_id').val();
        var store_id = $('#store_id').val();
        var branch_id = $('#branch_id').val();
        var _token = $('input[name=_token]').val();
        
        if(menu_code == "") {
          $('#menu_code_feedback').removeAttr('hidden');
        } else if(menu_name == "") {
          $('#menu_name_feedback').removeAttr('hidden');
        } else if(menu_sell_price == "") {
          $('#menu_sell_price_feedback').removeAttr('hidden');
        } else if(menu_point_price == "") {
          $('#menu_point_price_feedback').removeAttr('hidden');
        } else if(menu_discount == "") {
          $('#menu_discount_feedback').removeAttr('hidden');
        } else if(menu_photo == 0) {
          $('#menu_photo_feedback').removeAttr('hidden');
        } else if(category_id == "") {
          $('#category_id_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else if(branch_id == "") {
          $('#branch_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          var data = new FormData();
          data.append('menu_code', menu_code);
          data.append('menu_name', menu_name);
          data.append('menu_sell_price', menu_sell_price);
          data.append('menu_point_price', menu_point_price);
          data.append('menu_discount', menu_discount);
          data.append('menu_photo', $('#menu_photo').prop('files')[0]);
          data.append('category_id', category_id);
          data.append('store_id', store_id);
          data.append('branch_id', branch_id);
          data.append('_token', _token);

          $.ajax({
            url: "{{ route('menu.store') }}",
            method: "POST",  
            processData: false,
            contentType: false,
            dataType : 'json',
            enctype: "multipart/form-data",
            data: data,
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var menu_id = $('#menu_id').val();
        var menu_code = $('#menu_code').val();
        var menu_name = $('#menu_name').val();
        var menu_sell_price = $('#menu_sell_price').val();
        var menu_point_price = $('#menu_point_price').val();
        var menu_discount = $('#menu_discount').val();
        var menu_photo = $('#menu_photo').get(0).files.length;
        var category_id = $('#category_id').val();
        var store_id = $('#store_id').val();
        var branch_id = $('#branch_id').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('menu.update', 'menu_id') }}";
        url = url.replace('menu_id', menu_id);

        if(menu_code == "") {
          $('#menu_code_feedback').removeAttr('hidden');
        } else if(menu_name == "") {
          $('#menu_name_feedback').removeAttr('hidden');
        } else if(menu_sell_price == "") {
          $('#menu_sell_price_feedback').removeAttr('hidden');
        } else if(menu_point_price == "") {
          $('#menu_point_price_feedback').removeAttr('hidden');
        } else if(menu_discount == "") {
          $('#menu_discount_feedback').removeAttr('hidden');
        } else if(category_id == "") {
          $('#category_id_feedback').removeAttr('hidden');
        } else if(store_id == "") {
          $('#store_id_feedback').removeAttr('hidden');
        } else if(branch_id == "") {
          $('#branch_id_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          var data = new FormData();
          data.append('menu_id', menu_id);
          data.append('menu_code', menu_code);
          data.append('menu_name', menu_name);
          data.append('menu_sell_price', menu_sell_price);
          data.append('menu_point_price', menu_point_price);
          data.append('menu_discount', menu_discount);
          if(menu_photo != 0) {
            data.append('menu_photo', $('#menu_photo').prop('files')[0]);
          }
          data.append('category_id', category_id);
          data.append('store_id', store_id);
          data.append('branch_id', branch_id);
          data.append('_token', _token);
          data.append('_method', 'PUT');

          $.ajax({
            url: url,
            method: "POST",  
            processData: false,
            contentType: false,
            dataType : 'json',
            enctype: "multipart/form-data",
            data: data,
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');
      $('#preview_image_div').removeAttr('hidden');
      $('#required_menu_photo').attr('hidden', 'hidden');

      var menu_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('menu.show', 'menu_id') }}";
      url = url.replace('menu_id', menu_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          menu_id : menu_id, 
          _token : _token
        },
        success: function(result) {
          $('#menu_id').val(menu_id);
          $('#menu_code').val(result.data.menu_code);
          $('#menu_name').val(result.data.menu_name);
          $('#menu_sell_price').val(result.data.menu_sell_price);
          $('#menu_point_price').val(result.data.menu_point_price);
          $('#menu_discount').val(result.data.menu_discount);
          var url_menu_photo = "{{ asset('images/menu/menu_photo') }}";
          url_menu_photo = url_menu_photo.replace('menu_photo', result.data.menu_photo);
          $('#preview_image').attr('src', url_menu_photo);

          $('#store_id').val(result.data.store_id);
          $('#store_id').change();
          setTimeout(function() {
            $('#branch_id').val(result.data.branch_id);
            $('#category_id').val(result.data.category_id);
          }, 2000);
        }
      });
    });

    var menu_id = "";
    $(document).delegate('.delete', 'click', function() {
      menu_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('menu.destroy', 'menu_id') }}";
        url = url.replace('menu_id', menu_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            menu_id : menu_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');
      $('#preview_image_div').attr('hidden', 'hidden');
      $('#required_menu_photo').removeAttr('hidden');

      $('#menu_id').val('');
      $('#menu_code').val('');
      $('#menu_name').val('');
      $('#menu_sell_price').val('');
      $('#menu_point_price').val('');
      $('#menu_discount').val('');
      $('#menu_photo').val('');
      $('#category_id').val('');
      $('#store_id').val('');
      $('#branch_id').val('');
      if("{{ Auth::user()->user_role }}" != "Super Admin" && "{{ Auth::user()->user_role }}" != "Owner") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
        $('#branch_id').val("{{ Auth::user()->branch_id }}");
      } else if("{{ Auth::user()->user_role }}" == "Owner") {
        $('#store_id').val("{{ Auth::user()->store_id }}");
      }
    });

    $('#button_add').click();
    if("{{ Auth::user()->user_role }}" != "Super Admin" && "{{ Auth::user()->user_role }}" != "Owner") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
      $('#branch_id').val("{{ Auth::user()->branch_id }}")
    } else if("{{ Auth::user()->user_role }}" == "Owner") {
      $('#store_id').val("{{ Auth::user()->store_id }}");
    }
  </script>
@endsection
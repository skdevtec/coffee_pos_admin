@extends('layouts.master')

@section('title','Brand')

@section('heading','Brand')

@section('content')
  @if(Session::has('error'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('error') }}</p>
  @endif

  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif

  <div class="row">
    <div class="col-lg-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Daftar Brand</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Logo</th>
                  <th>Maksimal<br>Cabang</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody id="table_data">
                @foreach($stores as $row)
                  <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->store_name }}</td>
                    <td><img src="{{ asset('images/store/' . $row->store_photo) }}" style="width: 200px;"></td>
                    <td>{{ $row->store_branch_max }}</td>
                    <td>
                      <a class="btn btn-primary edit" data-id="{{ $row->store_id }}" href="javascript:void(0)">{{ __('Ubah') }}</a>
                      <a class="btn btn-danger delete" data-id="{{ $row->store_id }}" href="javascript:void(0)">{{ __('Hapus') }}</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Logo</th>
                  <th>Maksimal<br>Cabang</th>
                  <th>Aksi</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary" style="width: 100%;">
            <span id="header_action">Tambah</span> Brand
            <button id="button_add" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah</button>
            <div class="spinner-border text-primary pull-right" id="loading" hidden="hidden"></div>
          </h6>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="store_name" class="col-sm-3 col-form-label text-md-right">
              {{ __('Nama') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="store_name" name="store_name" value="">
              <p style="color: red;" id="store_name_feedback">
                <strong>Nama brand wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="store_photo" class="col-sm-3 col-form-label text-md-right">
              {{ __('Logo') }} <abbr id="required_store_photo" style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <div id="preview_image_div">
                <img id="preview_image" style="width: 100%;" src="">
              </div>
              <input type="file" id="store_photo" name="store_photo" onchange="fileValidation()">
              <p style="color: red;" id="store_photo_feedback">
                <strong>Logo brand wajib diisi!</strong>
              </p>
              <p style="color: red;" id="store_photo_feedback_extention">
                <strong>Ekstensi berkas yang didukung adalah jpg atau png!</strong>
              </p>
              <p style="color: red;" id="store_photo_feedback_size">
                <strong>Maksimal ukuran berkas adalah 1MB!</strong>
              </p>
            </div>
          </div>
          <div class="form-group row">
            <label for="store_branch_max" class="col-sm-3 col-form-label text-md-right">
              {{ __('Maksimal Cabang') }} <abbr style="color: red;">*</abbr>
            </label>
            <div class="col-sm-7">
              <input type="int" class="form-control" id="store_branch_max" name="store_branch_max" value="">
              <p style="color: red;" id="store_branch_max_feedback">
                <strong>Maksimal Cabang brand wajib diisi!</strong>
              </p>
            </div>
          </div>
          <div class="form-group">
            {{ csrf_field() }}
            <input type="text" id="store_id" value="" hidden>
            <input type="text" id="type" value="create" hidden>
            <center>
              <button type="button" class="btn btn-primary" id="submit">Simpan</button>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>  
        <div class="modal-body">
          Apakah anda ingin menghapus data brand ini?
        </div>
        <div class="modal-footer">
          <button id="button_delete" class="btn btn-secondary">{{ __('Ya, Hapus') }}</button>
          <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
   <script type="text/javascript">
    $('#store_photo_feedback_extention').attr('hidden', 'hidden');
    $('#store_photo_feedback_size').attr('hidden', 'hidden');
    function fileValidation() {
      $('#store_photo_feedback_extention').attr('hidden', 'hidden');
      $('#store_photo_feedback_size').attr('hidden', 'hidden');

      var input, file;
      input = document.getElementById('store_photo');
      file = input.files[0];
  
      var fileInput = document.getElementById('store_photo');
      var filePath = fileInput.value;
      var allowedExtensions = /(\.jpg|\.png)$/i;
      if(!allowedExtensions.exec(filePath)){
        $('#store_photo_feedback_extention').removeAttr('hidden');
  
        fileInput.value = "";
        return false;
      } else if(file.size > 1048576){
        $('#store_photo_feedback_size').removeAttr('hidden');
  
        fileInput.value = "";
        return false;
      }
    }

    $('#preview_image_div').attr('hidden', 'hidden');
    $('#store_name_feedback').attr('hidden', 'hidden');
    $('#store_photo_feedback').attr('hidden', 'hidden');
    $('#store_branch_max_feedback').attr('hidden', 'hidden');

    $('#button_add').attr('hidden', 'hidden');

    $('#submit').click(function() {
      $('#store_name_feedback').attr('hidden', 'hidden');
      $('#store_photo_feedback').attr('hidden', 'hidden');
      $('#store_branch_max_feedback').attr('hidden', 'hidden');

      var type = $('#type').val();
      if(type == "create") {
        var store_name = $('#store_name').val();
        var store_photo = $('#store_photo').get(0).files.length;
        var store_branch_max = $('#store_branch_max').val();
        var _token = $('input[name=_token]').val();

        if(store_name == "") {
          $('#store_name_feedback').removeAttr('hidden');
        } else if(store_photo == 0) {
          $('#store_photo_feedback').removeAttr('hidden');
        } else if(store_branch_max == 0) {
          $('#store_branch_max_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          var data = new FormData();
          data.append('store_name', store_name);
          data.append('store_photo', $('#store_photo').prop('files')[0]);
          data.append('store_branch_max', store_branch_max);
          data.append('_token', _token);

          $.ajax({
            url: "{{ route('brand.store') }}",
            method: "POST",  
            processData: false,
            contentType: false,
            dataType : 'json',
            enctype: "multipart/form-data",
            data: data,
            success: function(result) {
              location.reload();
            }
          });
        }
      } else if(type == "edit") {
        var store_id = $('#store_id').val();
        var store_name = $('#store_name').val();
        var store_photo = $('#store_photo').get(0).files.length;
        var store_branch_max = $('#store_branch_max').val();
        var _token = $('input[name=_token]').val();

        var url = "{{ route('brand.update', 'store_id') }}";
        url = url.replace('store_id', store_id);

        if(store_name == "") {
          $('#store_name_feedback').removeAttr('hidden');
        } else if(store_branch_max == 0) {
          $('#store_branch_max_feedback').removeAttr('hidden');
        } else {
          $('#loading').removeAttr('hidden');
          var data = new FormData();
          data.append('store_id', store_id);
          data.append('store_name', store_name);
          if(store_photo != 0) {
            data.append('store_photo', $('#store_photo').prop('files')[0]);
          }
          data.append('store_branch_max', store_branch_max);
          data.append('_token', _token);
          data.append('_method', 'PUT');

          $.ajax({
            url: url,
            method: "POST",  
            processData: false,
            contentType: false,
            dataType : 'json',
            enctype: "multipart/form-data",
            data: data,
            success: function(result) {
              location.reload();
            }
          });
        }
      }
    });

    $(document).delegate('.edit', 'click', function() {
      $('#header_action').html('Ubah');
      $('#type').val('edit');
      $('#button_add').removeAttr('hidden');
      $('#preview_image_div').removeAttr('hidden');
      $('#required_store_photo').attr('hidden', 'hidden');

      var store_id = $(this).data('id');
      var _token = $('input[name=_token]').val();

      var url = "{{ route('brand.show', 'store_id') }}";
      url = url.replace('store_id', store_id);

      $.ajax({
        url: url,
        method: "GET",  
        data: {
          store_id : store_id, 
          _token : _token
        },
        success: function(result) {
          $('#store_id').val(store_id);
          $('#store_name').val(result.data.store_name);
          var url_store_photo = "{{ asset('images/store/store_photo') }}";
          url_store_photo = url_store_photo.replace('store_photo', result.data.store_photo);
          $('#preview_image').attr('src', url_store_photo);
          $('#store_branch_max').val(result.data.store_branch_max);
        }
      });
    });

    var store_id = "";
    $(document).delegate('.delete', 'click', function() {
      store_id = $(this).data('id');

      $('#modal_delete').modal("show");
      $('#button_delete').click(function() {
        var _token = $('input[name=_token]').val();
        
        var url = "{{ route('brand.destroy', 'store_id') }}";
        url = url.replace('store_id', store_id);

        $.ajax({
          url: url,
          method: "DELETE",  
          data: {
            store_id : store_id, 
            _token : _token
          },
          success: function(result) {
            location.reload();
          }
        });
      });
    });

    $('#button_add').click(function() {
      $('#header_action').html('Tambah');
      $('#type').val('create');
      $('#button_add').attr('hidden', 'hidden');
      $('#preview_image_div').attr('hidden', 'hidden');
      $('#required_store_photo').removeAttr('hidden');

      $('#store_id').val('');
      $('#store_name').val('');
      $('#store_photo').val('');
      $('#store_branch_max').val('');
    });

    $('#button_add').click();
  </script>
@endsection
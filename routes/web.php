<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('admin', 'AdminController');
Route::resource('atur_akun', 'AccountSettingController');
Route::resource('akun', 'AccountController');
Route::resource('bahan_baku', 'IngredientController');
Route::resource('bahan_baku_menu', 'MenuIngredientController');
Route::resource('biaya_lain', 'OtherTransactionController');
Route::resource('cabang', 'BranchController');
Route::resource('jenis_akun', 'AccountTypeController');
Route::resource('jenis_pembayaran', 'PaymentTypeController');
Route::resource('jurnal', 'JournalController');
Route::resource('kasir', 'CashierController');
Route::resource('kategori', 'CategoryController');
Route::resource('member', 'MemberController');
Route::resource('menu', 'MenuController');
Route::resource('owner', 'OwnerController');
Route::resource('satuan', 'UnitController');
Route::resource('transfer', 'TransferController');
Route::resource('pembelian', 'RestockController');
Route::resource('penjualan', 'TransactionController');
Route::resource('brand', 'StoreController');
Route::resource('jenis_penjualan', 'TransactionTypeController');
Route::resource('konversi_poin', 'SettingPointController');
Route::resource('riwayat_poin', 'HistoryPointController');

Route::get('/get_branch/{store_id}', 'BranchController@get_branch');
Route::get('/get_unit/{branch_id}', 'UnitController@get_unit');
Route::get('/get_category/{branch_id}', 'CategoryController@get_category');
Route::get('/get_ingredient/{branch_id}', 'IngredientController@get_ingredient');
Route::get('/get_account_type/{branch_id}', 'AccountTypeController@get_account_type');
Route::get('/get_account/{branch_id}', 'AccountController@get_account');
Route::get('/get_payment_type/{branch_id}', 'PaymentTypeController@get_payment_type');

Route::get('/laporan/neraca', 'ReportController@neraca_pre')->name('neraca_pre');
Route::get('/laporan/laba_rugi', 'ReportController@laba_rugi_pre')->name('laba_rugi_pre');
Route::get('/laporan/arus_kas', 'ReportController@arus_kas_pre')->name('arus_kas_pre');
Route::get('/laporan/neraca/{store_id}/{branch_id}', 'ReportController@neraca')->name('neraca');
Route::get('/laporan/laba_rugi/{store_id}/{branch_id}', 'ReportController@laba_rugi')->name('laba_rugi');
Route::get('/laporan/arus_kas/{store_id}/{branch_id}', 'ReportController@arus_kas')->name('arus_kas');

Route::get('/invoice_download/penjualan/{transaction_id}', 'PDFController@invoice_transaction');
Route::get('/invoice_download/pembelian/{restock_id}', 'PDFController@invoice_restock');
Route::get('/invoice_download/transfer/{transfer_id}', 'PDFController@invoice_transfer');
Route::get('/invoice_download/biaya_lain/{other_transaction_id}', 'PDFController@invoice_other_transaction');
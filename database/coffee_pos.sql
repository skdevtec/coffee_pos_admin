-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Nov 2020 pada 10.40
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coffee_pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `account_balance` double DEFAULT NULL,
  `account_delete` int(11) DEFAULT NULL,
  `payment_type_id` int(11) DEFAULT NULL,
  `account_type_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `account`
--

INSERT INTO `account` (`account_id`, `account_name`, `account_balance`, `account_delete`, `payment_type_id`, `account_type_id`, `branch_id`, `store_id`) VALUES
(1, 'Kas', 2203000, 0, 1, 1, 1, 1),
(2, 'Kas', 2203000, 0, 2, 1, 2, 1),
(3, 'Kas', 2500000, 0, 3, 1, 1, 1),
(4, 'Kas', 2500000, 0, 4, 1, 2, 1),
(5, 'Kas', 2500000, 0, 7, 1, 1, 1),
(6, 'Kas', 2500000, 0, 8, 1, 2, 1),
(7, 'Kas', 2350000, 0, 5, 1, 1, 1),
(8, 'Kas', 2500000, 0, 6, 1, 2, 1),
(9, 'Persediaan', 447000, 0, NULL, 1, 1, 1),
(10, 'Persediaan', 297000, 0, NULL, 1, 2, 1),
(11, 'Penjualan', 0, 0, NULL, 4, 1, 1),
(12, 'Penjualan', 0, 0, NULL, 4, 2, 1),
(13, 'Biaya Listrik', 0, 0, NULL, 5, 1, 1),
(14, 'Biaya Listrik', 0, 0, NULL, 5, 2, 1),
(15, 'Biaya Air', 0, 0, NULL, 5, 1, 1),
(16, 'Biaya Air', 0, 0, NULL, 5, 2, 1),
(17, 'Biaya Gaji Karyawan', 0, 0, NULL, 5, 1, 1),
(18, 'Biaya Gaji Karyawan', 0, 0, NULL, 5, 2, 1),
(19, 'Biaya Diskon Penjualan', 0, 0, NULL, 5, 1, 1),
(20, 'Biaya Diskon Penjualan', 0, 0, NULL, 5, 2, 1),
(21, 'Biaya Potongan Penjualan', 0, 0, NULL, 5, 1, 1),
(22, 'Biaya Potongan Penjualan', 0, 0, NULL, 5, 2, 1),
(23, 'Biaya Bahan Baku', 0, 0, NULL, 5, 1, 1),
(24, 'Biaya Bahan Baku', 0, 0, NULL, 5, 2, 1),
(25, 'Kas', 0, 0, 9, 1, 1, 1),
(26, 'Kas', 0, 0, 10, 1, 2, 1),
(27, 'Kas Besar', 1000000, 0, NULL, 1, NULL, 1),
(28, 'Kas Kecil', 500000, 0, NULL, 1, NULL, 1),
(29, 'Kas Sedang', 750000, 0, NULL, 1, NULL, 1),
(30, 'Hutang Dagang', 0, 0, NULL, 2, 1, 1),
(31, 'Hutang Dagang', 0, 0, NULL, 2, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `account_setting`
--

CREATE TABLE `account_setting` (
  `account_setting_id` int(11) NOT NULL,
  `account_setting_type` varchar(100) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `account_setting`
--

INSERT INTO `account_setting` (`account_setting_id`, `account_setting_type`, `account_id`, `branch_id`) VALUES
(1, 'Debet Diskon Penjualan', 19, 1),
(2, 'Kredit Nota Penjualan', 11, 1),
(3, 'Kredit Bahan Baku Penjualan', 9, 1),
(4, 'Debet Bahan Baku Penjualan', 23, 1),
(5, 'Kredit Bahan Baku Penjualan', 10, 2),
(6, 'Debet Bahan Baku Penjualan', 24, 2),
(7, 'Kredit Nota Penjualan', 12, 2),
(8, 'Debet Diskon Penjualan', 20, 2),
(9, 'Debet Potongan Penjualan', 21, 1),
(10, 'Debet Potongan Penjualan', 22, 2),
(11, 'Debet Kredit Hutang Dagang', 30, 1),
(12, 'Debet Kredit Hutang Dagang', 31, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `account_type`
--

CREATE TABLE `account_type` (
  `account_type_id` int(11) NOT NULL,
  `account_type_name` varchar(100) DEFAULT NULL,
  `account_type_type` enum('Aktiva','Pasiva') DEFAULT NULL,
  `account_type_delete` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `account_type`
--

INSERT INTO `account_type` (`account_type_id`, `account_type_name`, `account_type_type`, `account_type_delete`, `store_id`) VALUES
(1, 'Kas', 'Aktiva', 0, 1),
(2, 'Hutang', 'Pasiva', 0, 1),
(3, 'Modal', 'Pasiva', 0, 1),
(4, 'Pendapatan', NULL, 0, 1),
(5, 'Beban', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(100) DEFAULT NULL,
  `branch_address` varchar(200) DEFAULT NULL,
  `branch_delete` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `branch_address`, `branch_delete`, `store_id`) VALUES
(1, 'Technology', 'Jl Dharmahusada Utara 22', 0, 1),
(2, 'Development', 'Jl Dharmahusada Utara 44', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `category_delete` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_delete`, `store_id`) VALUES
(1, 'Makanan', 0, 1),
(2, 'Minuman', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_point`
--

CREATE TABLE `history_point` (
  `history_point_id` int(11) NOT NULL,
  `history_point_value` varchar(45) DEFAULT NULL,
  `history_point_status` enum('In','Out') DEFAULT NULL,
  `setting_point_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ingredient`
--

CREATE TABLE `ingredient` (
  `ingredient_id` int(11) NOT NULL,
  `ingredient_name` varchar(100) DEFAULT NULL,
  `ingredient_buy_price` double DEFAULT NULL,
  `ingredient_stock` double DEFAULT NULL,
  `ingredient_delete` int(11) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ingredient`
--

INSERT INTO `ingredient` (`ingredient_id`, `ingredient_name`, `ingredient_buy_price`, `ingredient_stock`, `ingredient_delete`, `unit_id`, `branch_id`) VALUES
(1, 'Kopi Robusta', 125, 2000, 0, 2, 1),
(2, 'Kopi Robusta', 100, 1000, 0, 2, 2),
(3, 'Kopi Sumatera', 150, 1000, 0, 2, 1),
(4, 'Kopi Sumatera', 150, 1000, 0, 2, 2),
(5, 'Susu Kental', 20, 1000, 0, 1, 1),
(6, 'Susu Kental', 20, 1000, 0, 1, 2),
(7, 'Susu UHT', 18, 1000, 0, 1, 1),
(8, 'Susu UHT', 18, 1000, 0, 1, 2),
(9, 'Gula', 9, 1000, 0, 2, 1),
(10, 'Gula', 9, 1000, 0, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `journal`
--

CREATE TABLE `journal` (
  `journal_id` int(11) NOT NULL,
  `journal_date` datetime DEFAULT NULL,
  `journal_amount` double DEFAULT NULL,
  `journal_debet` double DEFAULT NULL,
  `journal_credit` double DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `restock_detail_id` int(11) DEFAULT NULL,
  `transfer_detail_id` int(11) DEFAULT NULL,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `other_transaction_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `journal`
--

INSERT INTO `journal` (`journal_id`, `journal_date`, `journal_amount`, `journal_debet`, `journal_credit`, `account_id`, `restock_detail_id`, `transfer_detail_id`, `transaction_detail_id`, `other_transaction_id`, `branch_id`, `store_id`) VALUES
(1, '2020-11-03 10:51:54', NULL, 100000, 0, 9, 1, NULL, NULL, NULL, 1, 1),
(2, '2020-11-03 10:51:54', NULL, 0, 100000, 1, 1, NULL, NULL, NULL, 1, 1),
(3, '2020-11-03 10:51:54', NULL, 150000, 0, 9, 2, NULL, NULL, NULL, 1, 1),
(4, '2020-11-03 10:51:54', NULL, 0, 150000, 1, 2, NULL, NULL, NULL, 1, 1),
(5, '2020-11-03 10:51:54', NULL, 20000, 0, 9, 3, NULL, NULL, NULL, 1, 1),
(6, '2020-11-03 10:51:54', NULL, 0, 20000, 1, 3, NULL, NULL, NULL, 1, 1),
(7, '2020-11-03 10:51:54', NULL, 18000, 0, 9, 4, NULL, NULL, NULL, 1, 1),
(8, '2020-11-03 10:51:54', NULL, 0, 18000, 1, 4, NULL, NULL, NULL, 1, 1),
(9, '2020-11-03 10:51:54', NULL, 9000, 0, 9, 5, NULL, NULL, NULL, 1, 1),
(10, '2020-11-03 10:51:54', NULL, 0, 9000, 1, 5, NULL, NULL, NULL, 1, 1),
(11, '2020-11-03 10:51:54', NULL, 100000, 0, 10, 1, NULL, NULL, NULL, 2, 1),
(12, '2020-11-03 10:51:54', NULL, 0, 100000, 2, 1, NULL, NULL, NULL, 2, 1),
(13, '2020-11-03 10:51:54', NULL, 150000, 0, 10, 2, NULL, NULL, NULL, 2, 1),
(14, '2020-11-03 10:51:54', NULL, 0, 150000, 2, 2, NULL, NULL, NULL, 2, 1),
(15, '2020-11-03 10:51:54', NULL, 20000, 0, 10, 3, NULL, NULL, NULL, 2, 1),
(16, '2020-11-03 10:51:54', NULL, 0, 20000, 2, 3, NULL, NULL, NULL, 2, 1),
(17, '2020-11-03 10:51:54', NULL, 18000, 0, 10, 4, NULL, NULL, NULL, 2, 1),
(18, '2020-11-03 10:51:54', NULL, 0, 18000, 2, 4, NULL, NULL, NULL, 2, 1),
(19, '2020-11-03 10:51:54', NULL, 9000, 0, 10, 5, NULL, NULL, NULL, 2, 1),
(20, '2020-11-03 10:51:54', NULL, 0, 9000, 2, 5, NULL, NULL, NULL, 2, 1),
(21, '2020-11-18 14:12:53', NULL, 150000, 0, 9, 11, NULL, NULL, NULL, 1, 1),
(22, '2020-11-18 14:12:53', NULL, 0, 150000, 30, 11, NULL, NULL, NULL, 1, 1),
(23, '2020-11-18 07:13:10', NULL, 150000, 0, 30, 11, NULL, NULL, NULL, 1, 1),
(24, '2020-11-18 07:13:10', NULL, 0, 150000, 7, 11, NULL, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_code` varchar(100) DEFAULT NULL,
  `menu_name` varchar(100) DEFAULT NULL,
  `menu_sell_price` double DEFAULT NULL,
  `menu_point_price` double DEFAULT NULL,
  `menu_photo` varchar(100) DEFAULT NULL,
  `menu_discount` int(11) DEFAULT NULL,
  `menu_delete` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_code`, `menu_name`, `menu_sell_price`, `menu_point_price`, `menu_photo`, `menu_discount`, `menu_delete`, `branch_id`, `category_id`) VALUES
(1, 'SK01', 'Coffee Latte Robusta', 20000, 50, 'SK01_1.png', 0, 0, 1, 2),
(2, 'SK01', 'Coffee Latte Robusta', 20000, 0, 'SK01_2.png', 20, 0, 2, 2),
(3, 'SK02', 'Coffee Latte Sumatera', 25000, 0, 'SK02_1.png', 20, 0, 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_ingredient`
--

CREATE TABLE `menu_ingredient` (
  `menu_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `menu_ingredient_count` double DEFAULT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `menu_ingredient`
--

INSERT INTO `menu_ingredient` (`menu_id`, `ingredient_id`, `menu_ingredient_count`, `unit_id`) VALUES
(1, 1, 20, 2),
(1, 7, 50, 1),
(1, 9, 10, 2),
(2, 2, 20, 2),
(2, 8, 50, 1),
(2, 10, 10, 2),
(3, 3, 20, 2),
(3, 5, 10, 1),
(3, 7, 15, 1),
(3, 9, 5, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_ingredient_transaction`
--

CREATE TABLE `menu_ingredient_transaction` (
  `menu_ingredient_transaction_id` int(11) NOT NULL,
  `menu_ingredient_transaction_count` double DEFAULT NULL,
  `menu_ingredient_transaction_buy_price` double DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) NOT NULL,
  `transaction_detail_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `other_transaction`
--

CREATE TABLE `other_transaction` (
  `other_transaction_id` int(11) NOT NULL,
  `other_transaction_invoice_number` varchar(45) DEFAULT NULL,
  `other_transaction_date` datetime DEFAULT NULL,
  `other_transaction_description` varchar(45) DEFAULT NULL,
  `other_transaction_amount` double DEFAULT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `payment_type`
--

CREATE TABLE `payment_type` (
  `payment_type_id` int(11) NOT NULL,
  `payment_type_name` varchar(100) DEFAULT NULL,
  `payment_type_type` enum('Cash','Cashless') DEFAULT NULL,
  `payment_type_delete` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `payment_type`
--

INSERT INTO `payment_type` (`payment_type_id`, `payment_type_name`, `payment_type_type`, `payment_type_delete`, `branch_id`) VALUES
(1, 'Tunai', 'Cash', 0, 1),
(2, 'Tunai', 'Cash', 0, 2),
(3, 'OVO', 'Cashless', 0, 1),
(4, 'OVO', 'Cashless', 0, 2),
(5, 'DANA', 'Cashless', 0, 1),
(6, 'DANA', 'Cashless', 0, 2),
(7, 'GOPAY', 'Cashless', 0, 1),
(8, 'GOPAY', 'Cashless', 0, 2),
(9, 'QRIS', 'Cashless', 0, 1),
(10, 'QRIS', 'Cashless', 0, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `restock`
--

CREATE TABLE `restock` (
  `restock_id` int(11) NOT NULL,
  `restock_invoice_number` varchar(45) DEFAULT NULL,
  `restock_date` datetime DEFAULT NULL,
  `restock_message` varchar(200) DEFAULT NULL,
  `restock_total_price` double DEFAULT NULL,
  `restock_payment_status` enum('Paid','Unpaid') DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `account_id_debet` int(11) NOT NULL,
  `account_id_credit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `restock`
--

INSERT INTO `restock` (`restock_id`, `restock_invoice_number`, `restock_date`, `restock_message`, `restock_total_price`, `restock_payment_status`, `user_id`, `branch_id`, `account_id_debet`, `account_id_credit`) VALUES
(1, '202011030001', '2020-11-03 10:51:54', 'Persediaan Awal', 297000, 'Paid', 1, 1, 9, 1),
(2, '202011030001', '2020-11-03 10:52:42', 'Persediaan Awal', 297000, 'Paid', 1, 2, 10, 2),
(3, '202011180001', '2020-11-18 14:12:53', NULL, 150000, 'Paid', 1, 1, 9, 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `restock_detail`
--

CREATE TABLE `restock_detail` (
  `restock_detail_id` int(11) NOT NULL,
  `restock_detail_price` double DEFAULT NULL,
  `restock_detail_count` double DEFAULT NULL,
  `restock_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `restock_detail`
--

INSERT INTO `restock_detail` (`restock_detail_id`, `restock_detail_price`, `restock_detail_count`, `restock_id`, `ingredient_id`, `unit_id`) VALUES
(1, 100000, 1000, 1, 1, 2),
(2, 150000, 1000, 1, 3, 2),
(3, 20000, 1000, 1, 5, 1),
(4, 18000, 1000, 1, 7, 1),
(5, 9000, 1000, 1, 9, 2),
(6, 100000, 1000, 2, 2, 2),
(7, 150000, 1000, 2, 4, 2),
(8, 20000, 1000, 2, 6, 1),
(9, 18000, 1000, 2, 8, 1),
(10, 9000, 1000, 2, 10, 2),
(11, 150000, 1000, 3, 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting_point`
--

CREATE TABLE `setting_point` (
  `setting_point_id` int(11) NOT NULL,
  `setting_point_multiple` double DEFAULT NULL,
  `setting_point_value` double DEFAULT NULL,
  `setting_point_delete` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `setting_point`
--

INSERT INTO `setting_point` (`setting_point_id`, `setting_point_multiple`, `setting_point_value`, `setting_point_delete`, `store_id`) VALUES
(1, 10000, 10, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `store`
--

CREATE TABLE `store` (
  `store_id` int(11) NOT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `store_photo` varchar(100) DEFAULT NULL,
  `store_branch_max` int(11) DEFAULT NULL,
  `store_delete` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `store`
--

INSERT INTO `store` (`store_id`, `store_name`, `store_photo`, `store_branch_max`, `store_delete`) VALUES
(1, 'SK', 'SK.png', 5, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction`
--

CREATE TABLE `transaction` (
  `transaction_id` int(11) NOT NULL,
  `transaction_invoice_number` varchar(45) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `transaction_status` enum('Pending','Paid','Credit','Paid Credit') DEFAULT NULL,
  `transaction_member_phone` varchar(100) DEFAULT NULL,
  `transaction_discount_absolute` double DEFAULT NULL,
  `transaction_total_price` double DEFAULT NULL,
  `transaction_total_discount` double DEFAULT NULL,
  `transaction_total_paid` double DEFAULT NULL,
  `transaction_total_change` double DEFAULT NULL,
  `transaction_charge_absolute` double DEFAULT NULL,
  `transaction_charge_percentage` double DEFAULT NULL,
  `transaction_delete` int(11) DEFAULT NULL,
  `transaction_delete_reason` varchar(100) DEFAULT NULL,
  `transaction_type_id` int(11) DEFAULT NULL,
  `payment_type_id` int(11) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_detail`
--

CREATE TABLE `transaction_detail` (
  `transaction_detail_id` int(11) NOT NULL,
  `transaction_detail_count` int(11) DEFAULT NULL,
  `transaction_detail_capital_price` double DEFAULT NULL,
  `transaction_detail_price` double DEFAULT NULL,
  `transaction_detail_price_after_discount` double DEFAULT NULL,
  `transaction_detail_discount` int(11) DEFAULT NULL,
  `transaction_detail_subtotal` double DEFAULT NULL,
  `transaction_detail_subtotal_after_discount` double DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `ingredient_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_type`
--

CREATE TABLE `transaction_type` (
  `transaction_type_id` int(11) NOT NULL,
  `transaction_type_name` varchar(100) DEFAULT NULL,
  `transaction_type_charge_absolute` double DEFAULT NULL,
  `transaction_type_charge_percentage` double DEFAULT NULL,
  `transaction_type_delete` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `transaction_type`
--

INSERT INTO `transaction_type` (`transaction_type_id`, `transaction_type_name`, `transaction_type_charge_absolute`, `transaction_type_charge_percentage`, `transaction_type_delete`, `store_id`) VALUES
(1, 'Dine In', 0, 0, 0, 1),
(2, 'Take Away', 0, 0, 0, 1),
(3, 'Grab', 0, 20, 0, 1),
(4, 'Gojek', 0, 20, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transfer`
--

CREATE TABLE `transfer` (
  `transfer_id` int(11) NOT NULL,
  `transfer_invoice_number` varchar(45) DEFAULT NULL,
  `transfer_date` datetime DEFAULT NULL,
  `transfer_message` varchar(100) DEFAULT NULL,
  `transfer_total_amount` double DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transfer_detail`
--

CREATE TABLE `transfer_detail` (
  `transfer_detail_id` int(11) NOT NULL,
  `transfer_detail_amount` double DEFAULT NULL,
  `account_id_from` int(11) NOT NULL,
  `account_id_to` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `unit`
--

CREATE TABLE `unit` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(100) DEFAULT NULL,
  `unit_delete` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `unit`
--

INSERT INTO `unit` (`unit_id`, `unit_name`, `unit_delete`, `store_id`) VALUES
(1, 'ml', 0, 1),
(2, 'g', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_phone` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `user_role` varchar(100) DEFAULT NULL,
  `user_point` double DEFAULT NULL,
  `user_delete` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_phone`, `email`, `password`, `user_role`, `user_point`, `user_delete`, `branch_id`, `store_id`) VALUES
(1, 'Super Admin', '085252212221', 'super.admin@gmail.com', '$2y$10$QDHuWvwcjoLSpmfclIKTFeXTUD08Q5fbT6gZENvGuvtKKMvMnECS2', 'Super Admin', 0, 0, NULL, NULL),
(2, 'Owner', '+6285252212221', 'owner@gmail.com', '$2y$10$OE3rsOP5CBBQwDRa5aKKJuJjQTlDu5BSnGCyFCyzKhpGZeegbZyIe', 'Owner', 0, 0, NULL, 1),
(3, 'Admin', '+6285252212221', 'admin@gmail.com', '$2y$10$4dHjygLnGI4PwJIYTYGVjOirJ15cSR23wqh2mRzKMcQk3q2lshQlS', 'Admin', 0, 0, 1, 1),
(4, 'Kasir', '+6285252212221', 'kasir@gmail.com', '$2y$10$n5osUQlIV8Ezfs.FHqOgoOABNHrlLOvhSd4JXsMr3TVCeXksEvMxy', 'Cashier', 0, 0, 1, 1),
(5, 'Member', '+6285252212221', 'member@gmail.com', '$2y$10$/hvjdeIm8tUrG/SZdZJyvu7vIoZLtrQoInH4rdA1oPaDtCuCFzuSe', 'Member', 0, 0, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`),
  ADD KEY `fk_account_payment_type1_idx` (`payment_type_id`),
  ADD KEY `fk_account_account_type1_idx` (`account_type_id`),
  ADD KEY `fk_account_branch1_idx` (`branch_id`),
  ADD KEY `fk_account_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `account_setting`
--
ALTER TABLE `account_setting`
  ADD PRIMARY KEY (`account_setting_id`),
  ADD KEY `fk_account_setting_account1_idx` (`account_id`),
  ADD KEY `fk_account_setting_branch1_idx` (`branch_id`);

--
-- Indeks untuk tabel `account_type`
--
ALTER TABLE `account_type`
  ADD PRIMARY KEY (`account_type_id`),
  ADD KEY `fk_account_type_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `fk_branch_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `fk_category_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `history_point`
--
ALTER TABLE `history_point`
  ADD PRIMARY KEY (`history_point_id`),
  ADD KEY `fk_history_point_transaction1_idx` (`transaction_id`),
  ADD KEY `fk_history_point_setting_point1_idx` (`setting_point_id`),
  ADD KEY `fk_history_point_users1_idx` (`user_id`);

--
-- Indeks untuk tabel `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`ingredient_id`),
  ADD KEY `fk_ingredient_unit_idx` (`unit_id`),
  ADD KEY `fk_ingredient_branch1_idx` (`branch_id`);

--
-- Indeks untuk tabel `journal`
--
ALTER TABLE `journal`
  ADD PRIMARY KEY (`journal_id`),
  ADD KEY `fk_log_account_account1_idx` (`account_id`),
  ADD KEY `fk_log_account_other_transaction1_idx` (`other_transaction_id`),
  ADD KEY `fk_journal_branch1_idx` (`branch_id`),
  ADD KEY `fk_journal_store1_idx` (`store_id`),
  ADD KEY `fk_journal_transaction_detail1_idx` (`transaction_detail_id`),
  ADD KEY `fk_journal_restock_detail1_idx` (`restock_detail_id`),
  ADD KEY `fk_journal_transfer_detail1_idx` (`transfer_detail_id`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `fk_menu_branch1_idx` (`branch_id`),
  ADD KEY `fk_menu_category1_idx` (`category_id`);

--
-- Indeks untuk tabel `menu_ingredient`
--
ALTER TABLE `menu_ingredient`
  ADD PRIMARY KEY (`menu_id`,`ingredient_id`),
  ADD KEY `fk_menu_has_ingredient_ingredient1_idx` (`ingredient_id`),
  ADD KEY `fk_menu_has_ingredient_menu1_idx` (`menu_id`),
  ADD KEY `fk_menu_ingredient_unit1_idx` (`unit_id`);

--
-- Indeks untuk tabel `menu_ingredient_transaction`
--
ALTER TABLE `menu_ingredient_transaction`
  ADD PRIMARY KEY (`menu_ingredient_transaction_id`),
  ADD KEY `fk_menu_has_ingredient_ingredient2_idx` (`ingredient_id`),
  ADD KEY `fk_menu_has_ingredient_menu2_idx` (`menu_id`),
  ADD KEY `fk_menu_has_ingredient_transaction_detail1_idx` (`transaction_detail_id`),
  ADD KEY `fk_menu_ingredient_transaction_unit1_idx` (`unit_id`);

--
-- Indeks untuk tabel `other_transaction`
--
ALTER TABLE `other_transaction`
  ADD PRIMARY KEY (`other_transaction_id`),
  ADD KEY `fk_other_transaction_branch1_idx` (`branch_id`);

--
-- Indeks untuk tabel `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`payment_type_id`),
  ADD KEY `fk_payment_type_branch1_idx` (`branch_id`);

--
-- Indeks untuk tabel `restock`
--
ALTER TABLE `restock`
  ADD PRIMARY KEY (`restock_id`),
  ADD KEY `fk_restock_users1_idx` (`user_id`),
  ADD KEY `fk_restock_branch1_idx` (`branch_id`),
  ADD KEY `fk_restock_account1_idx` (`account_id_debet`),
  ADD KEY `fk_restock_account2_idx` (`account_id_credit`);

--
-- Indeks untuk tabel `restock_detail`
--
ALTER TABLE `restock_detail`
  ADD PRIMARY KEY (`restock_detail_id`),
  ADD KEY `fk_restock_detail_ingredient1_idx` (`ingredient_id`),
  ADD KEY `fk_restock_detail_unit1_idx` (`unit_id`),
  ADD KEY `fk_restock_detail_restock1_idx` (`restock_id`);

--
-- Indeks untuk tabel `setting_point`
--
ALTER TABLE `setting_point`
  ADD PRIMARY KEY (`setting_point_id`),
  ADD KEY `fk_setting_point_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indeks untuk tabel `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `fk_transaction_users1_idx` (`employee_id`),
  ADD KEY `fk_transaction_payment_type1_idx` (`payment_type_id`),
  ADD KEY `fk_transaction_branch1_idx` (`branch_id`),
  ADD KEY `fk_transaction_transaction_type1_idx` (`transaction_type_id`);

--
-- Indeks untuk tabel `transaction_detail`
--
ALTER TABLE `transaction_detail`
  ADD PRIMARY KEY (`transaction_detail_id`),
  ADD KEY `fk_transaction_detail_menu1_idx` (`menu_id`),
  ADD KEY `fk_transaction_detail_transaction1_idx` (`transaction_id`),
  ADD KEY `fk_transaction_detail_ingredient1_idx` (`ingredient_id`);

--
-- Indeks untuk tabel `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`transaction_type_id`),
  ADD KEY `fk_transaction_type_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`transfer_id`),
  ADD KEY `fk_transfer_users1_idx` (`user_id`),
  ADD KEY `fk_transfer_branch1_idx` (`branch_id`);

--
-- Indeks untuk tabel `transfer_detail`
--
ALTER TABLE `transfer_detail`
  ADD PRIMARY KEY (`transfer_detail_id`),
  ADD KEY `fk_transfer_detail_account1_idx` (`account_id_from`),
  ADD KEY `fk_transfer_detail_account2_idx` (`account_id_to`),
  ADD KEY `fk_transfer_detail_transfer1_idx` (`transfer_id`);

--
-- Indeks untuk tabel `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`unit_id`),
  ADD KEY `fk_unit_store1_idx` (`store_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `fk_users_branch1_idx` (`branch_id`),
  ADD KEY `fk_users_store1_idx` (`store_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `account_setting`
--
ALTER TABLE `account_setting`
  MODIFY `account_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `account_type`
--
ALTER TABLE `account_type`
  MODIFY `account_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `history_point`
--
ALTER TABLE `history_point`
  MODIFY `history_point_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `ingredient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `journal`
--
ALTER TABLE `journal`
  MODIFY `journal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `menu_ingredient_transaction`
--
ALTER TABLE `menu_ingredient_transaction`
  MODIFY `menu_ingredient_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `other_transaction`
--
ALTER TABLE `other_transaction`
  MODIFY `other_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `payment_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `restock`
--
ALTER TABLE `restock`
  MODIFY `restock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `restock_detail`
--
ALTER TABLE `restock_detail`
  MODIFY `restock_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `setting_point`
--
ALTER TABLE `setting_point`
  MODIFY `setting_point_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `store`
--
ALTER TABLE `store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transaction_detail`
--
ALTER TABLE `transaction_detail`
  MODIFY `transaction_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `transaction_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transfer`
--
ALTER TABLE `transfer`
  MODIFY `transfer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `transfer_detail`
--
ALTER TABLE `transfer_detail`
  MODIFY `transfer_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `unit`
--
ALTER TABLE `unit`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `fk_account_account_type1` FOREIGN KEY (`account_type_id`) REFERENCES `account_type` (`account_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_account_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_account_payment_type1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_type` (`payment_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_account_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `account_setting`
--
ALTER TABLE `account_setting`
  ADD CONSTRAINT `fk_account_setting_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_account_setting_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `account_type`
--
ALTER TABLE `account_type`
  ADD CONSTRAINT `fk_account_type_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `branch`
--
ALTER TABLE `branch`
  ADD CONSTRAINT `fk_branch_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `fk_category_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `history_point`
--
ALTER TABLE `history_point`
  ADD CONSTRAINT `fk_history_point_setting_point1` FOREIGN KEY (`setting_point_id`) REFERENCES `setting_point` (`setting_point_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_point_transaction1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_point_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `ingredient`
--
ALTER TABLE `ingredient`
  ADD CONSTRAINT `fk_ingredient_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ingredient_unit` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `journal`
--
ALTER TABLE `journal`
  ADD CONSTRAINT `fk_journal_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_journal_restock_detail1` FOREIGN KEY (`restock_detail_id`) REFERENCES `restock_detail` (`restock_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_journal_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_journal_transaction_detail1` FOREIGN KEY (`transaction_detail_id`) REFERENCES `transaction_detail` (`transaction_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_journal_transfer_detail1` FOREIGN KEY (`transfer_detail_id`) REFERENCES `transfer_detail` (`transfer_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_account_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_log_account_other_transaction1` FOREIGN KEY (`other_transaction_id`) REFERENCES `other_transaction` (`other_transaction_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fk_menu_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_menu_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `menu_ingredient`
--
ALTER TABLE `menu_ingredient`
  ADD CONSTRAINT `fk_menu_has_ingredient_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`ingredient_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menu_has_ingredient_menu1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menu_ingredient_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `menu_ingredient_transaction`
--
ALTER TABLE `menu_ingredient_transaction`
  ADD CONSTRAINT `fk_menu_has_ingredient_ingredient2` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`ingredient_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menu_has_ingredient_menu2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menu_has_ingredient_transaction_detail1` FOREIGN KEY (`transaction_detail_id`) REFERENCES `transaction_detail` (`transaction_detail_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_menu_ingredient_transaction_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`unit_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `other_transaction`
--
ALTER TABLE `other_transaction`
  ADD CONSTRAINT `fk_other_transaction_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `payment_type`
--
ALTER TABLE `payment_type`
  ADD CONSTRAINT `fk_payment_type_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `restock`
--
ALTER TABLE `restock`
  ADD CONSTRAINT `fk_restock_account1` FOREIGN KEY (`account_id_debet`) REFERENCES `account` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restock_account2` FOREIGN KEY (`account_id_credit`) REFERENCES `account` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restock_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restock_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `restock_detail`
--
ALTER TABLE `restock_detail`
  ADD CONSTRAINT `fk_restock_detail_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`ingredient_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restock_detail_restock1` FOREIGN KEY (`restock_id`) REFERENCES `restock` (`restock_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_restock_detail_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`unit_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `setting_point`
--
ALTER TABLE `setting_point`
  ADD CONSTRAINT `fk_setting_point_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `fk_transaction_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transaction_payment_type1` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_type` (`payment_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transaction_transaction_type1` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`transaction_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transaction_users1` FOREIGN KEY (`employee_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaction_detail`
--
ALTER TABLE `transaction_detail`
  ADD CONSTRAINT `fk_transaction_detail_ingredient1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`ingredient_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transaction_detail_menu1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transaction_detail_transaction1` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD CONSTRAINT `fk_transaction_type_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `transfer`
--
ALTER TABLE `transfer`
  ADD CONSTRAINT `fk_transfer_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transfer_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `transfer_detail`
--
ALTER TABLE `transfer_detail`
  ADD CONSTRAINT `fk_transfer_detail_account1` FOREIGN KEY (`account_id_from`) REFERENCES `account` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transfer_detail_account2` FOREIGN KEY (`account_id_to`) REFERENCES `account` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transfer_detail_transfer1` FOREIGN KEY (`transfer_id`) REFERENCES `transfer` (`transfer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `unit`
--
ALTER TABLE `unit`
  ADD CONSTRAINT `fk_unit_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_store1` FOREIGN KEY (`store_id`) REFERENCES `store` (`store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
